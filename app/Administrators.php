<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Administrators extends Model
{
    protected $table="administrators";

    protected $fillable = [
        'lname','fname','email','id_user','images'
    ];

    /**
     * @param $id_user
     * @return string
     */
    public function get_logo($id_user)
    {
        $pathToFile = "";

        $UsersAdministrators = UsersAdministrators::join('administrators', 'administrators.id', '=', 'users_administrators.id_administrator')
            ->select('users_administrators.*', 'administrators.images')
            ->where('users_administrators.id_user', '=', $id_user)->get();

        if ($UsersAdministrators->count()>0) {
            foreach ($UsersAdministrators as $UsersAdministrator) {
                $pathToFile = "admin/".$UsersAdministrator->id_administrator.".jpg";
                if ($UsersAdministrator->images <> "") {
                    \Image::make($UsersAdministrator->images, 41, 41)->save($pathToFile);
                }
            }
        } else {

            $UsersOwners = UsersOwners::join('administrators', 'administrators.id', '=', 'users_owners.id_administrators')
                ->select('users_owners.*', 'administrators.images')
                ->where('users_owners.id_user', '=', $id_user)->get();

            if ($UsersOwners->count() > 0) {
                foreach ($UsersOwners as $UsersOwner) {
                    $pathToFile = "admin/".$UsersOwner->id_administrators.".jpg";
                    if ($UsersOwner->images <> "") {
                        \Image::make($UsersOwner->images, 41, 41)->save($pathToFile);
                    }
                }
            }
        }

        if ($UsersAdministrators->count()<=0 && $UsersOwners->count()<=0) {
            $pathToFile = "logoG.png";
        }
        return $pathToFile;
    }
}

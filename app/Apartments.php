<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Apartments extends Model
{
    protected $table="apartments";

    protected $fillable = [
        'description','identifications','meters','floor','notes','parking','id_user'
    ];

    public function get_apartments_builds_owners_polls($id_builds)
    {
        $ApartmentsOwners = BuildsApartments::join('apartments','apartments.id','builds_apartments.id_apartments')
            ->join('apartments_owners','apartments_owners.id_apartments','=','apartments.id')
            ->select('builds_apartments.id','apartments.description')
            ->where('builds_apartments.id_builds',$id_builds)
            ->pluck('description','id');

        return $ApartmentsOwners;
    }

    /**
     * @param $id_builds
     * @return mixed
     */
    public function get_apartments_builds_owners($id_builds)
    {
        $ApartmentsOwners = BuildsApartments::join('apartments','apartments.id','builds_apartments.id_apartments')
            ->select('builds_apartments.id','apartments.description')
            ->whereIN('builds_apartments.id_builds',$id_builds)
            ->pluck('description','id');

        return $ApartmentsOwners;
    }

    /**
     * @return mixed
     */
    public function get_apartments_builds()
    {
        $UsersOwners = new UsersOwners;
        $iduser = \Auth::id();
        $builds = $UsersOwners->get_builds_owners_administrator($iduser);

        $apartments = Apartments::join('builds_apartments','apartments.id', '=', 'builds_apartments.id_apartments')
            ->select(
                'apartments.id',
                'apartments.description'
            )
            ->join('builds','builds_apartments.id_builds','=','builds.id')
            ->whereIn('builds.id',$builds->toArray())
            ->orderBy('builds.id')->orderBy('apartments.id')->pluck('description','id');
        return  $apartments;
    }


    /**
     * @param $id_administrator
     * @return mixed
     */
    public function get_apartments_administrator_builds($id_administrator)
    {
        $UsersOwners = new UsersOwners;
        $iduser = \Auth::id();

        $builds = $UsersOwners->get_builds_owners_administrator($iduser);

        $apartments = Apartments::join('builds_apartments','apartments.id', '=', 'builds_apartments.id_apartments')
            ->select(
                'apartments.*',
                'builds.description as description_builds',
                'builds.id as id_builds'
            )
            ->join('builds','builds_apartments.id_builds','=','builds.id')
            ->whereIn('builds.id',$builds->toArray())
            ->orderBy('builds.id')->orderBy('apartments.id')->get();
        return  $apartments;
    }

    /**
     * @param $id_administrator
     * @return mixed
     */
    public function get_apartments_administrator_builds_without($id_administrator)
    {
        $UsersOwners = new UsersOwners;
        $iduser = \Auth::id();

        $builds = $UsersOwners->get_builds_owners_administrator($iduser);

        $apartmentsowners = \DB::table('apartments_owners')
            ->select('apartments_owners.id_apartments')
            ->join('apartments','apartments_owners.id_apartments','=','apartments.id')
            ->join('builds_apartments','builds_apartments.id_apartments','=','apartments.id')
            ->join('users_owners','apartments_owners.id_owners','=','users_owners.id_owners')
            ->whereIn('builds_apartments.id_builds',$builds);

        $apartments = Apartments::join('builds_apartments','apartments.id', '=', 'builds_apartments.id_apartments')
            ->select(
                'apartments.*',
                'builds.description as description_builds',
                'builds.id as id_builds'
            )
            ->join('builds', function ($join) {
                    $join->on('builds_apartments.id_builds', '=', 'builds.id');
            })
            ->join('administrators_builds', function ($join) use($id_administrator,$builds)  {
                $join->on('administrators_builds.id_builds', '=', 'builds.id')
                    ->whereIN('builds.id',$builds);
            })
            ->whereNotIn('apartments.id',$apartmentsowners)
            ->orderBy('builds.id')->orderBy('apartments.id')->get();

        return  $apartments;
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApartmentsOwners extends Model
{
    protected $table="apartments_owners";

    protected $fillable = [
        'id_apartments','id_owners'
    ];
}

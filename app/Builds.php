<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Builds extends Model
{
    protected $table="builds";

    protected $fillable = [
        'identifications','description','id_user','address','amount_maintenance_meters'
    ];

    /**
     * @param $id_administrator
     * @return mixed
     */
    public function get_builds_apartments_user_administrator($id_administrator)
    {
        $UsersOwners = new UsersOwners;
        $iduser = \Auth::id();

        $builds = $UsersOwners->get_builds_owners_administrator($iduser);

        $builds = Builds::join('administrators_builds','builds.id', '=', 'administrators_builds.id_builds')
            ->select(
                'builds.description',
                'builds.id'
            )
            ->whereIN('administrators_builds.id_builds',$builds->toArray())
            ->orderBy('builds.id')->get();
        return  $builds;
    }

    /**
     * @param $id_administrator
     * @return mixed
     */
    public function get_builds_user_administrator($id_administrator)
    {
        $UsersOwners = new UsersOwners;
        $iduser = \Auth::id();

        $builds = $UsersOwners->get_builds_owners_administrator($iduser);

        $builds = Builds::whereIN('builds.id',$builds)
            ->select(
                'builds.*'
            )
            ->orderBy('builds.id')->get();

        return  $builds;
    }



}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BuildsAdministrators extends Model
{
    protected $table="administrators_builds";

    protected $fillable = [
        'id_administrator','id_builds'
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BuildsApartments extends Model
{
    protected $table="builds_apartments";

    protected $fillable = [
        'id_apartments','id_builds'
    ];
}

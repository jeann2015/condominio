<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Documents extends Model
{
    protected $table="documents";

    protected $fillable = [
        'description','id_builds','type_file','files_general','size'
    ];

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    protected $table="expense";

    protected $fillable = [
        'id_user','id_administrator','id_builds','amount','id_statment'
    ];
}

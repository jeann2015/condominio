<?php

namespace App\Http\Controllers;

use App\Builds;
use App\BuildsAdministrators;
use App\GruposUsers;
use App\UsersOwners;
use App\ViewsAdministrators;
use Bogardo\Mailgun\Facades\Mailgun;
use Illuminate\Http\Request;

use App\Administrators;
use App\Modules;
use App\Audits;
use App\User;
use App\UsersAdministrators;
use App\Access;
use App\Grupos;

class AdministratorsController extends Controller
{
    /**
     * AdministratorsController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $module = new Modules;
        $Users = new User;

        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);
        $administrators = Administrators::join('users_administrators','users_administrators.id_administrator','=','administrators.id')
            ->join('grupos_usuarios','grupos_usuarios.id_user','=','users_administrators.id_user')
            ->join('grupos','grupos.id','=','grupos_usuarios.id_grupos')
            ->select('administrators.*','grupos.description as description_grupos')
            ->where('administrators.id_user',$iduser)->get();

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);



        if ($request->session()->exists('status')) {
            $notifys = $request->session()->get('status');
            return view('administrators.index', compact('notifys', 'administrators', 'user_access', 'module_principals', 'module_menus'));
        }
        else{
            return view('administrators.index', compact('notifys', 'administrators', 'user_access', 'module_principals', 'module_menus'));
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(Request $request)
    {
        $module = new Modules;
        $user = new User;

        $UsersOwners = new UsersOwners;
        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        $users = $user->get_type_user($iduser);

        if($users==0) {
            $builds = $UsersOwners->get_builds_owners_administrator($iduser);

            if ($builds->count() > 0) {
                $build = new Builds;
                $usersadministrators = new UsersAdministrators;
                $id_administrators = $usersadministrators->get_administrator($iduser);
                foreach ($id_administrators as $id_administrator) {
                    $buildsAdmin = $build->get_builds_apartments_user_administrator($id_administrator->id_administrator)->pluck('description','id');
                }
                $Grupos = Grupos::whereIN('id',[4,5])->pluck('description','id');

                return view('administrators.addbuilds', compact('Grupos','buildsAdmin','user_access', 'module_principals', 'module_menus'));

            } else {
                $request->session()->flash('status', 'No Builds');
                return redirect('administrators');
            }
        }else{
            return view('administrators.add', compact('user_access', 'module_principals', 'module_menus'));
        }

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function news(Request $request)
    {
        $audits = new Audits;
        $user = new User;
        $iduser = \Auth::id();

        $name=$request->fname." ".$request->lname;
        $users = $user->get_type_user($iduser);

        if($users==1) {

            $this->validate($request, [
                'email' => 'required|unique:users,email',
                'fname' => 'required',
                'lname' => 'required',
                'images' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
            ]);

            $link = hash('md5', uniqid('142857',true));
            $pathToFile = $link.'.jpg';

            if($request->file('images')){
                $images = \Image::make($request->file('images')->getRealPath())
                    ->save($pathToFile);
            }else{
                $images = "";
            }

            $administrators = Administrators::create([
                'fname'=>$request->fname,
                'lname'=>$request->lname,
                'email'=>$request->email,
                'id_user'=>$iduser,
                'images'=>$images
            ]);

            $grupos = 2;

            $user_pass = $user->news_user_administrators_owners($name, $request->email, $grupos, $administrators->id);

            $data['email']=$request->email;
            $data['pass']=$user_pass;
            $email = $request->email;
            $name = $request->fname." ".$request->lname;

            Mailgun::send('emails.maila', $data, function ($message) use ($name, $email) {
                $message->to($email, $name)->subject('Bienvenido!');
            });



        }else{

            $this->validate($request, [
                'email' => 'required|unique:users',
                'id_builds' => 'required:numeric|exists:builds,id',
                'fname' => 'required',
                'lname' => 'required',
                'images' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'id_grupos' => 'required:numeric|exists:grupos,id',
            ]);

            $link = hash('md5', uniqid('142857',true));
            $pathToFile = $link.'.jpg';

            if($request->file('images')){
                $images = \Image::make($request->file('images')->getRealPath())
                    ->save($pathToFile);
            }else{
                $images = "";
            }

            $administrators = Administrators::create([
                'fname'=>$request->fname,
                'lname'=>$request->lname,
                'email'=>$request->email,
                'id_user'=>$iduser,
                'images'=>$images
            ]);
            foreach ($request->id_builds as $id_build) {

                BuildsAdministrators::create([
                    'id_builds'=>$id_build,
                    'id_administrator'=>$administrators->id
                ]);

            }
            $grupos = $request->id_grupos;

            $user_pass = $user->news_user_administrators_owners($name, $request->email, $grupos, $administrators->id);

            $data['email']=$request->email;
            $data['pass']=$user_pass;
            $email = $request->email;
            $name = $request->fname." ".$request->lname;

            Mailgun::send('emails.maila', $data, function ($message) use ($name, $email) {
                $message->to($email, $name)->subject('Bienvenido!');
            });

        }

        $audits->save_audits('Add new Administrator:'.$administrators->id." - ".$request->fname." ".$request->lname);
        return redirect('administrators');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request)
    {


        $module = new Modules;
        $user = new User;
        $iduser = \Auth::id();
        $users = $user->get_type_user($iduser);

        if($users==1) {


            $administrators = Administrators::find($request->id);
            if ($administrators->images <> "") {

                $link = hash('md5', uniqid('142857', true));
                $pathToFile = "" . $link . ".jpg";

                \Image::make($administrators->images, 100, 100)->save($pathToFile);
                $imagen[] = $pathToFile;
            } else {
                $imagen[] = "";
            }

            $module_principals = $module->get_modules_principal_user($iduser);
            $module_menus = $module->get_modules_menu_user($iduser);
            return view('administrators.mod', compact('imagen', 'administrators', 'module_menus', 'module_principals'));

        }else{



            $administrators = Administrators::find($request->id);
            $BuildsAdministrators = BuildsAdministrators::where('id_administrator',$request->id)->pluck('id_builds');
            if ($administrators->images <> "") {

                $link = hash('md5', uniqid('142857', true));
                $pathToFile = "" . $link . ".jpg";

                \Image::make($administrators->images, 100, 100)->save($pathToFile);
                $imagen[] = $pathToFile;
            } else {
                $imagen[] = "";
            }

            $module_principals = $module->get_modules_principal_user($iduser);
            $module_menus = $module->get_modules_menu_user($iduser);

            $build = new Builds;
            $usersadministrators = new UsersAdministrators;
            $id_administrators = $usersadministrators->get_administrator($iduser);
            foreach ($id_administrators as $id_administrator) {
                $buildsAdmin = $build->get_builds_apartments_user_administrator($id_administrator->id_administrator)->pluck('description','id');
            }
            $Grupos = Grupos::whereIN('id',[4,5])->pluck('description','id');


            return view('administrators.modbuilds', compact('BuildsAdministrators','Grupos','buildsAdmin','imagen', 'administrators', 'module_menus', 'module_principals'));
        }

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request)
    {
        $audits = new Audits;

        $user = new User;
        $iduser = \Auth::id();
        $users = $user->get_type_user($iduser);

        if($users==1) {

            $this->validate($request, [
                'fname' => 'required',
                'lname' => 'required',
                'images' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
            ]);

            $link = hash('md5', uniqid('142857', true));
            $pathToFile = $link . '.jpg';

            if ($request->file('images')) {
                $images = \Image::make($request->file('images')->getRealPath())
                    ->save($pathToFile);
            } else {
                $images = "";
            }

            $administrators = Administrators::find($request->id);
            $administrators->fname = $request->fname;
            $administrators->lname = $request->lname;
            $administrators->id_user = $iduser;
            if ($images <> "") {
                $administrators->images = $images;
            }
            $administrators->save();

            $audits->save_audits('Modify Administrators:' . $administrators->id . " - " . $request->fname . " " . $request->lname);
            return redirect('administrators');

        }else{

            $this->validate($request, [
                'email' => 'required|exists:users,email',
                'id_builds' => 'required:numeric|exists:builds,id',
                'fname' => 'required',
                'lname' => 'required',
                'images' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'id_grupos' => 'required:numeric|exists:grupos,id',
            ]);

            $link = hash('md5', uniqid('142857', true));
            $pathToFile = $link . '.jpg';

            if ($request->file('images')) {
                $images = \Image::make($request->file('images')->getRealPath())
                    ->save($pathToFile);
            } else {
                $images = "";
            }

            $administrators = Administrators::find($request->id);
            $administrators->fname = $request->fname;
            $administrators->lname = $request->lname;
            $administrators->id_user = $iduser;
            if ($images <> "") {
                $administrators->images = $images;
            }
            $administrators->save();

            BuildsAdministrators::where('id_administrator','=',$request->id)->delete();

            foreach ($request->id_builds as $id_build) {
                BuildsAdministrators::create([
                    'id_builds'=>$id_build,
                    'id_administrator'=>$administrators->id
                ]);
            }

            $audits->save_audits('Modify Administrators:' . $request->id . " - " . $request->fname . " " . $request->lname);
            return redirect('administrators');

        }

    }

    public function delete(Request $request)
    {


        $module = new Modules;
        $user = new User;
        $iduser = \Auth::id();
        $users = $user->get_type_user($iduser);

        if($users==1) {
            $administrators = Administrators::find($request->id);
            if ($administrators->images <> "") {

                $link = hash('md5', uniqid('142857', true));
                $pathToFile = "" . $link . ".jpg";

                \Image::make($administrators->images, 100, 100)->save($pathToFile);
                $imagen[] = $pathToFile;
            } else {
                $imagen[] = "";
            }

            $module_principals = $module->get_modules_principal_user($iduser);
            $module_menus = $module->get_modules_menu_user($iduser);
            return view('administrators.del', compact('imagen', 'administrators', 'module_menus', 'module_principals'));

        }else{

            $administrators = Administrators::find($request->id);
            $BuildsAdministrators = BuildsAdministrators::where('id_administrator',$request->id)->pluck('id_builds');
            if ($administrators->images <> "") {

                $link = hash('md5', uniqid('142857', true));
                $pathToFile = "" . $link . ".jpg";

                \Image::make($administrators->images, 100, 100)->save($pathToFile);
                $imagen[] = $pathToFile;
            } else {
                $imagen[] = "";
            }

            $module_principals = $module->get_modules_principal_user($iduser);
            $module_menus = $module->get_modules_menu_user($iduser);

            $build = new Builds;
            $usersadministrators = new UsersAdministrators;
            $id_administrators = $usersadministrators->get_administrator($iduser);
            foreach ($id_administrators as $id_administrator) {
                $buildsAdmin = $build->get_builds_apartments_user_administrator($id_administrator->id_administrator)->pluck('description','id');
            }
            $Grupos = Grupos::whereIN('id',[4,5])->pluck('description','id');


            return view('administrators.delbuilds', compact('BuildsAdministrators','Grupos','buildsAdmin','imagen', 'administrators', 'module_menus', 'module_principals'));
        }

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request)
    {
        $this->validate($request, [
            'id' => 'required:numeric|exists:administrators,id'
        ]);

        $audits = new Audits;
        $users_administrators = UsersAdministrators::where('id_administrator','=',$request->id);
        $access = Access::where('id_user','=',$users_administrators->pluck('id_user'));

        $GruposUsers = GruposUsers::whereIN('id_user',$users_administrators->pluck('id_user')->all());
        $GruposUsers->delete();

        User::whereIN('id',$users_administrators->pluck('id_user')->all())->delete();

        $users_administrators->delete();
        $access->delete();

        BuildsAdministrators::where('id_administrator','=',$request->id)->delete();
        ViewsAdministrators::where('id_administrator',$request->id)->delete();
        $administrators = Administrators::find($request->id);
        $administrators->delete();

        $audits->save_audits('Deleted Administrator:'.$administrators->id." - ".$request->fname." ".$request->lname);
        return redirect('administrators');
    }
}

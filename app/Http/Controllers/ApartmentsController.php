<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Apartments;
use App\Modules;
use App\Audits;
use App\BuildsApartments;
use App\Builds;
use App\UsersAdministrators;
use App\Http\Requests\NewsApartments;
use App\BuildsAdministrators;


class ApartmentsController extends Controller
{
    /**
     * OwnersController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $module = new Modules;
        $apartment = new Apartments;
        $usersadministrators = new UsersAdministrators;

        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);
        $id_administrators = $usersadministrators->get_administrator($iduser);

        $apartments=array();
        foreach ($id_administrators as $id_administrator)
        {
            $apartments = $apartment->get_apartments_administrator_builds($id_administrator->id_administrator);
        }

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        return view('apartments.index', compact('apartments', 'user_access','module_principals','module_menus'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(Request $request)
    {
        $module = new Modules;
        $build = new Builds;
        $usersadministrators = new UsersAdministrators;
        $builds = array();

        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);
        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        $id_administrators = $usersadministrators->get_administrator($iduser);

        foreach ($id_administrators as $id_administrator)
        {
            $buildsAll = $build->get_builds_apartments_user_administrator($id_administrator->id_administrator)->pluck('description','id')->put('','Seleccione un Edificio/Residencia');
            $builds = array_sort_recursive($buildsAll->toArray());
        }

        return view('apartments.add', compact( 'user_access','builds','module_principals','module_menus'));
    }

    /**
     * @param NewsApartments $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function news(NewsApartments $request)
    {
        $audits = new Audits;
        $iduser = \Auth::id();

        if($request->notes==""){
            $notas = "S/N";
        }else{
            $notas = $request->notes;
        }

        $apartments = Apartments::create([
            'description'=>$request->description,
            'identifications'=>$request->identification,
            'meters'=>$request->meters,
            'floor'=>$request->floor,
            'parking'=>$request->parking,
            'id_user'=>$iduser,
            'notes'=>$notas
        ]);

        BuildsApartments::create([
            'id_apartments'=>$apartments->id,
            'id_builds'=>$request->id_builds
        ]);


        $audits->save_audits('Add new Apartments:'.$apartments->id." - ".$request->descriptions);
        return redirect('apartments');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request)
    {
        $module = new Modules;
        $build = new Builds;
        $usersadministrators = new UsersAdministrators;
        $iduser = \Auth::id();
        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        $id_administrators = $usersadministrators->get_administrator($iduser);
        foreach ($id_administrators as $id_administrator)
        {
            $buildsAll = $build->get_builds_apartments_user_administrator($id_administrator->id_administrator)->pluck('description','id')->put('','Seleccione');
            $builds = array_sort_recursive($buildsAll->toArray());
        }
        $apartments = Apartments::find($request->id);
        $buildsapartments = BuildsApartments::where('id_apartments','=',$request->id)->pluck('id_builds');
        return view('apartments.mod', compact('apartments','builds','buildsapartments','module_menus','module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request)
    {
        $audits = new Audits;
        $iduser = \Auth::id();

        $apartments = Apartments::find($request->id);
        $apartments->description = $request->description;
        $apartments->identifications = $request->identification;
        $apartments->meters = $request->meters;
        $apartments->floor = $request->floor;
        $apartments->parking = $request->parking;
        $apartments->notes = $request->notes;
        $apartments->id_user = $iduser;
        $apartments->save();

        BuildsApartments::where('id_apartments',$apartments->id)
            ->update(['id_builds'=>$request->id_builds]);

        $audits->save_audits('Modify Apartments:'.$request->id." - ".$request->description);
        return redirect('apartments');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function delete(Request $request)
    {
        $module = new Modules;
        $build = new Builds;
        $usersadministrators = new UsersAdministrators;
        $iduser = \Auth::id();
        $apartments = Apartments::find($request->id);
        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);
        $id_administrators = $usersadministrators->get_administrator($iduser);
        foreach ($id_administrators as $id_administrator)
        {
            $buildsAll = $build->get_builds_apartments_user_administrator($id_administrator->id_administrator)->pluck('description','id')->put('','Seleccione');
            $builds = array_sort_recursive($buildsAll->toArray());
        }
        $apartments = Apartments::find($request->id);
        $buildsapartments = BuildsApartments::where('id_apartments','=',$request->id)->pluck('id_builds');
        return view('apartments.del', compact('apartments','builds','buildsapartments','module_menus','module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request)
    {
        $audits = new Audits;

        $buildsapartments = BuildsApartments::where('id_apartments','=',$request->id);
        $buildsapartments->delete();

        $apartments = Apartments::find($request->id);
        $apartments->delete();

        $audits->save_audits('Deleted Apartments:'.$apartments->id." - ".$request->description);
        return redirect('apartments');
    }
}

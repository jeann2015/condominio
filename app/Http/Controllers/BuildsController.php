<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Builds;
use App\BuildsAdministrators;
use App\Modules;
use App\Audits;
use App\UsersAdministrators;

class BuildsController extends Controller
{
    /**
     * BuildsController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $module = new Modules;
        $build = new Builds;
        $usersadministrators = new UsersAdministrators;
        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);
        $id_administrators = $usersadministrators->get_administrator($iduser);
        $builds=array();
        foreach ($id_administrators as $id_administrator) {
            $builds = $build->get_builds_user_administrator($id_administrator->id_administrator);
        }
        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);
        return view('builds.index', compact('builds', 'user_access', 'module_principals', 'module_menus'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(Request $request)
    {
        $module = new Modules;
        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);
        $builds = Builds::all();
        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);
        return view('builds.add', compact('builds', 'user_access', 'module_principals', 'module_menus'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function news(Request $request)
    {
        $this->validate($request, [
            'description'=>'required|min:3',
            'identifications'=>'required',
            'address'=>'required',
            'amount_maintenance_meters'=>'required:numeric'
        ]);

        $audits = new Audits;
        $usersadministrators = new UsersAdministrators;
        $iduser = \Auth::id();
        $id_administrators = $usersadministrators->get_administrator($iduser);

        if ($id_administrators->count()>0) {
            $builds = Builds::create([
                'identifications'=>$request->identifications,
                'description'=>$request->description,
                'address'=>$request->address,
                'amount_maintenance_meters'=>$request->amount_maintenance_meters,
                'id_user'=>$iduser
            ]);
            foreach ($id_administrators as $id_administrator) {
                BuildsAdministrators::create([
                    'id_builds' => $builds->id,
                    'id_administrator' => $id_administrator->id_administrator
                ]);
            }
        }
        $audits->save_audits('Add new Builds:'.$builds->id." - ".$request->description);
        return redirect('builds');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request)
    {

        $module = new Modules;
        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);
        $builds = Builds::find($request->id);
        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);
        return view('builds.mod', compact('builds', 'user_access', 'module_principals', 'module_menus'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'id'=>'required:numeric|exists:builds,id',
            'description'=>'required|min:3',
            'identifications'=>'required',
            'address'=>'required',
            'amount_maintenance_meters'=>'required:numeric'
        ]);
        $audits = new Audits;
        $usersadministrators = new UsersAdministrators;
        $iduser = \Auth::id();
        $builds = Builds::find($request->id);
        $builds->identifications=$request->identifications;
        $builds->description=$request->description;
        $builds->address=$request->address;
        $builds->amount_maintenance_meters=$request->amount_maintenance_meters;
        $builds->id_user=$iduser;
        $builds->save();
        $id_administrators = $usersadministrators->get_administrator($iduser);
        foreach ($id_administrators as $id_administrator) {
            BuildsAdministrators::where('id_builds', '=', $request->id)
                ->update(['id_administrator'=>$id_administrator->id_administrator]);
        }
        $audits->save_audits('Modify Builds:'.$builds->id." - ".$request->description);
        return redirect('builds');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function delete(Request $request)
    {

        $module = new Modules;
        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);
        $builds = Builds::find($request->id);
        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);
        return view('builds.del', compact('builds', 'user_access', 'module_principals', 'module_menus'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request)
    {
        $this->validate($request, [
            'id'=>'required:numeric|exists:builds,id'
        ]);
        $audits = new Audits;
        $buildsadministrators = BuildsAdministrators::where('id_builds', '=', $request->id);
        $buildsadministrators->delete();
        $builds = Builds::find($request->id);
        $builds->delete();
        $audits->save_audits('Delete Builds:'.$builds->id." - ".$request->description);
        return redirect('builds');
    }
}

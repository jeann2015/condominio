<?php

namespace App\Http\Controllers;

use App\Audits;
use App\Builds;
use App\Documents;
use App\Modules;
use App\UsersAdministrators;
use Illuminate\Http\Request;
use App\UsersOwners;


class DocumentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $module = new Modules;
        $build = new Builds;
        $UsersOwners =  new UsersOwners;
        $usersadministrators = new UsersAdministrators;
        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);
        $id_administrators = $usersadministrators->get_administrator($iduser);
        $builds=array();

        $idAdministrator="";
        foreach ($id_administrators as $id_administrator) {
            $builds = $build->get_builds_user_administrator($id_administrator->id_administrator);
            $idAdministrator = $id_administrator->id_administrator;
        }

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);
        $builds_user = $UsersOwners->get_builds_owners_administrator($iduser);
        $documents = Documents::join('builds','builds.id','=','documents.id_builds')
        ->select('documents.*','builds.description as description_builds')
        ->wherein('builds.id',$builds_user->toArray())
        ->get();

        return view('documents.index', compact('idAdministrator','documents','builds', 'user_access', 'module_principals', 'module_menus'));
    }

    public function add(Request $request)
    {
        $module = new Modules;
        $build = new Builds;
        $url = $request->path();
        $usersadministrators = new UsersAdministrators;
        $iduser = \Auth::id();
        $user_access = $module->accesos($iduser, $url);
        $id_administrators = $usersadministrators->get_administrator($iduser);
        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        $builds=array();
        foreach ($id_administrators as $id_administrator) {
            $buildsAll = $build->get_builds_apartments_user_administrator($id_administrator->id_administrator)->pluck('description','id')->put('','Seleccione un Edificio/Residencia');
            $builds = array_sort_recursive($buildsAll->toArray());
        }
        return view('documents.add', compact('builds','user_access', 'builds', 'module_principals', 'module_menus'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function news(Request $request)
    {
        $this->validate($request, [
            'id_builds' => 'required:numeric|exists:builds,id',
            'description' => 'required|min:3',
            'images' => 'required|mimes:pdf|max:200000',
        ]);

        $audits = new Audits;
        $f = $request->file('images');

        $documents = Documents::create([
            'id_builds'=>$request->id_builds,
            'description'=>$request->description,
            'files_general'=>base64_encode(file_get_contents($f->getRealPath())),
            'type_file'=>$f->getClientMimeType(),
            'size'=>$f->getClientSize()
        ]);

        $audits->save_audits('Add Documents:' . $documents->id . " " . $request->description);
        return redirect('documents');

    }

    public function edit(Request $request)
    {
        $module = new Modules;
        $build = new Builds;
        $url = $request->path();
        $usersadministrators = new UsersAdministrators;
        $iduser = \Auth::id();
        $user_access = $module->accesos($iduser, $url);
        $id_administrators = $usersadministrators->get_administrator($iduser);
        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        $builds=array();
        foreach ($id_administrators as $id_administrator) {
            $buildsAll = $build->get_builds_apartments_user_administrator($id_administrator->id_administrator)->pluck('description','id')->put('','Seleccione un Edificio/Residencia');
            $builds = array_sort_recursive($buildsAll->toArray());
        }
        $documents = Documents::find($request->id);
        return view('documents.mod', compact('documents','builds','user_access', 'builds', 'module_principals', 'module_menus'));
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id_builds' => 'required:numeric|exists:builds,id',
            'description' => 'required|min:3',
            'images' => 'required|mimes:pdf,xlsx|max:50000',
        ]);

        $audits = new Audits;
        $f = $request->file('images');

        Documents::where('id',$request->id)
            ->update(
                    [
                    'files_general'=>base64_encode(file_get_contents($f->getRealPath())),
                    'size'=>$f->getClientSize(),
                    'description'=>$request->description,
                    'id_builds'=>$request->id_builds,
                    'type_file'=>$f->getClientMimeType(),
                    ]);


        $audits->save_audits('Modify Documents:' . $request->id . " " . $request->description);
        return redirect('documents');
    }

    public function delete(Request $request)
    {
        $module = new Modules;
        $build = new Builds;
        $url = $request->path();
        $usersadministrators = new UsersAdministrators;
        $iduser = \Auth::id();
        $user_access = $module->accesos($iduser, $url);
        $id_administrators = $usersadministrators->get_administrator($iduser);
        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        $builds=array();
        foreach ($id_administrators as $id_administrator) {
            $buildsAll = $build->get_builds_apartments_user_administrator($id_administrator->id_administrator)->pluck('description','id')->put('','Seleccione un Edificio/Residencia');
            $builds = array_sort_recursive($buildsAll->toArray());
        }
        $documents = Documents::find($request->id);
        return view('documents.del', compact('documents','builds','user_access', 'builds', 'module_principals', 'module_menus'));
    }


    public function destroy(Request $request)
    {
        $this->validate($request, [
            'id_builds' => 'required:numeric|exists:builds,id',
            'description' => 'required|min:3',
            'id' => 'required|exists:documents,id',
        ]);

        $audits = new Audits;
        Documents::find($request->id)->delete();
        $audits->save_audits('Delete Documents:' . $request->id . " " . $request->description);
        return redirect('documents');
    }
}

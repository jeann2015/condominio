<?php

namespace App\Http\Controllers;

use App\UsersOwners;
use Illuminate\Http\Request;

use App\Expense;
use App\Modules;
use App\Audits;
use App\UsersAdministrators;
use App\Builds;
use App\Statement;

class ExpenseController extends Controller
{
    /**
     * ExpenseController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $module = new Modules;
        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);
        $UsersOwners = new UsersOwners;
        $iduser = \Auth::id();
        $builds = $UsersOwners->get_builds_owners_administrator($iduser);

        $id_administrators_owners =  UsersAdministrators::where('id_user', '=', $iduser)->get();
        $administratorId="";
        foreach ($id_administrators_owners as $id_administrators_owner) {
            $administratorId = $id_administrators_owner->id_administrator;
        }
        $expenses=[];

        if($administratorId<>"") {
            $expenses = Expense::join('statement', 'statement.id', '=', 'expense.id_statment')
                ->join('builds', 'builds.id', '=', 'expense.id_builds')
                ->select('expense.*', 'builds.description as description_builds', 'statement.type')
                ->whereIN('builds.id',$builds->toArray())->get();
        }
        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

//        dd($expenses);

        return view('expenses.index', compact('expenses','user_access','module_principals','module_menus'));
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(Request $request)
    {
        $module = new Modules;
        $build = new Builds;

        $usersadministrators = new UsersAdministrators;
        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);
        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        $id_administrators = $usersadministrators->get_administrator($iduser);

        foreach ($id_administrators as $id_administrator)
        {
            $buildsAll = $build->get_builds_apartments_user_administrator($id_administrator->id_administrator)->pluck('description','id')->put('','Seleccione un Edificio/Residencia');
            $builds = array_sort_recursive($buildsAll->toArray());
            $statement = Statement::where('id_administrator','=',$id_administrator->id_administrator)->pluck('description','id')->put('','Seleccione');
            $statements = array_sort_recursive($statement->toArray());
        }

        return view('expenses.add', compact('statements','builds','user_access','module_principals','module_menus'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function news(Request $request)
    {
        $audits = new Audits;
        $iduser = \Auth::id();

        $this->validate($request, [
            'id_builds' => 'required',
            'id_statment' => 'required',
            'amount'=>'required:numeric'
        ]);

        $id_administrators_owners =  UsersAdministrators::where('id_user', '=', $iduser)->get();
        foreach ($id_administrators_owners as $id_administrators_owner) {

            $expenses = Expense::create([
                'id_builds' => $request->id_builds,
                'id_statment' => $request->id_statment,
                'amount' =>$request->amount,
                'id_user'=>$iduser,
                'id_administrator'=>$id_administrators_owner->id_administrator
            ]);
        }

        $audits->save_audits('Add new Expense:'.$expenses->id);
        return redirect('expenses');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request)
    {
        $module = new Modules;
        $iduser = \Auth::id();
        $build = new Builds;

        $id_administrators_owners =  UsersAdministrators::where('id_user', '=', $iduser)->get();
        foreach ($id_administrators_owners as $id_administrators_owner) {
            $administrator_id = $id_administrators_owner->id_administrator;

            $buildsAll = $build->get_builds_apartments_user_administrator($administrator_id)->pluck('description','id')->put('','Seleccione un Edificio/Residencia');
            $builds = array_sort_recursive($buildsAll->toArray());
            $statement = Statement::where('id_administrator','=',$administrator_id)->pluck('description','id')->put('','Seleccione');
            $statements = array_sort_recursive($statement->toArray());
            $expenses = Expense::where('id','=',$request->id)->where('id_administrator','=',$administrator_id)->get();
        }

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        return view('expenses.mod', compact('statements','builds','expenses','module_menus','module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request)
    {
        $audits = new Audits;
        $iduser = \Auth::id();

        $this->validate($request, [
            'id_builds' => 'required',
            'id_statment' => 'required',
            'amount'=>'required:numeric',
            'id'=>'required:numeric'
        ]);

        $id_administrators_owners =  UsersAdministrators::where('id_user', '=', $iduser)->get();
        foreach ($id_administrators_owners as $id_administrators_owner) {

            $expenses = Expense::where('id','=',$request->id)
                ->update([
                'id_builds' => $request->id_builds,
                'id_statment' => $request->id_statment,
                'amount' =>$request->amount,
                'id_user'=>$iduser,
                'id_administrator'=>$id_administrators_owner->id_administrator
            ]);
        }

        $audits->save_audits('Modify Expense:'.$request->id);

        return redirect('expenses');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function delete(Request $request)
    {
        $module = new Modules;
        $iduser = \Auth::id();
        $build = new Builds;

        $id_administrators_owners =  UsersAdministrators::where('id_user', '=', $iduser)->get();
        foreach ($id_administrators_owners as $id_administrators_owner) {
            $administrator_id = $id_administrators_owner->id_administrator;

            $buildsAll = $build->get_builds_apartments_user_administrator($administrator_id)->pluck('description','id')->put('','Seleccione un Edificio/Residencia');
            $builds = array_sort_recursive($buildsAll->toArray());
            $statement = Statement::where('id_administrator','=',$administrator_id)->pluck('description','id')->put('','Seleccione');
            $statements = array_sort_recursive($statement->toArray());
            $expenses = Expense::where('id','=',$request->id)->where('id_administrator','=',$administrator_id)->get();
        }

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        return view('expenses.del', compact('statements','builds','expenses','module_menus','module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request)
    {
        $audits = new Audits;
        $this->validate($request, [
            'id'=>'required:numeric'
        ]);
        $expenses = Expense::find($request->id);
        $expenses->delete();
        $audits->save_audits('Delete Expense:'.$expenses->id);
        return redirect('expenses');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function pdf(Request $request)
    {

        $expenses = Expense::join('statement','statement.id','=','expense.id_statment')
            ->join('builds', function ($join)  {
                $join->on('builds.id','=','expense.id_builds');
            })
            ->select('expense.*','builds.description as description_builds','statement.type','statement.description as description_statement')
            ->where('expense.id','=',$request->id)
            ->get();

        $pdf = \PDF::loadView('expenses.pdf', compact('expenses'));
        return $pdf->setPaper('letter', 'landscape')->download('expense.pdf');
    }

}

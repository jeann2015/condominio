<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modules;
use App\User;
use App\Administrators;
use App\ViewsOwners;

class HomeController extends Controller
{
    /**
     * HomeController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $iduser = \Auth::id();
        $m = new Modules();
        $a = new Administrators();
        $ViewsOwner = new ViewsOwners();
        $views_news = $ViewsOwner->search_news_owners_administrators($iduser);
        $module_principals = $m->get_modules_principal_user($iduser);
        $module_menus = $m->get_modules_menu_user($iduser);
        $logo=$a->get_logo($iduser);
        $request->session()->put('logo', $logo);
        return view('home', compact('module_principals', 'module_menus','views_news'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function changes()
    {
        $iduser = \Auth::id();
        $m = new Modules();
        $module_principals = $m->get_modules_principal_user($iduser);
        $module_menus = $m->get_modules_menu_user($iduser);
        return view('changes.index', compact('module_principals', 'module_menus'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request)
    {
        $iduser = \Auth::id();
        $user = User::find($iduser);
        $user->password = bcrypt($request->password);
        $user->save();
        flash('Clave Modificada!');
        return redirect('changes');
    }
}

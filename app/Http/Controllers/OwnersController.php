<?php

namespace App\Http\Controllers;

use App\Apartments;
use Bogardo\Mailgun\Facades\Mailgun;
use Illuminate\Http\Request;

use App\Owners;
use App\Modules;
use App\Audits;
use App\User;
use App\UsersOwners;
use App\UsersAdministrators;
use App\Builds;
use App\ApartmentsOwners;
use PhpParser\ErrorHandler\Collecting;


class OwnersController extends Controller
{
    /**
     * OwnersController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $module = new Modules;
        $owner = new Owners;
        $iduser = \Auth::id();

        $usersadministrators = new UsersAdministrators;
        $id_administrators = $usersadministrators->get_administrator($iduser);


        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);
        $owners=array();
        foreach ($id_administrators as $id_administrator)
        {
            $owners = $owner->get_owners_apartments_administrator_builds($id_administrator->id_administrator);
        }

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        return view('owners.index', compact('owners', 'user_access','module_principals','module_menus'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(Request $request)
    {
        $module = new Modules;
        $build = new Builds;
        $apartment = new Apartments;


        $usersadministrators = new UsersAdministrators;
        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        $id_administrators = $usersadministrators->get_administrator($iduser);

        $apartments=array();
        $builds=array();
        foreach ($id_administrators as $id_administrator)
        {
            $apartments = $apartment->get_apartments_administrator_builds_without($id_administrator->id_administrator);
            $buildsAll = $build->get_builds_user_administrator($id_administrator->id_administrator)->pluck('description','id')->put('','Seleccione un Edificio/Residencia');
            $builds = array_sort_recursive($buildsAll->toArray());
        }
        return view('owners.add', compact( 'user_access','apartments','builds','module_principals','module_menus'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function news(Request $request)
    {
        $audits = new Audits;
        $users = new User;
        $iduser = \Auth::id();

        $this->validate($request, [
            'email' => 'required|unique:users',
            'fname' => 'required',
            'lname' => 'required',
            'phonenumber'=>'required',
            'status'=>'required',
            'id_apartments'=>'required'
        ]);

        $owners = Owners::create([
            'fname'=>$request->fname,
            'lname'=>$request->lname,
            'email'=>$request->email,
            'phonenumber'=>$request->phonenumber,
            'id_user'=>$iduser,
            'status'=>$request->status
        ]);

        foreach ($request->id_apartments as $id_apartment) {
            ApartmentsOwners::create([
                'id_owners' => $owners->id,
                'id_apartments' => $id_apartment
            ]);
        }

        $name=$request->fname." ".$request->lname;
        $grupos=3;

        $users_pass = $users->news_user_administrators_owners($name,$request->email,$grupos,$owners->id);

        $data['email']=$request->email;
        $data['pass']=$users_pass;
        $email = $request->email;
        $name = $request->fname." ".$request->lname;

        Mailgun::send('emails.mail', $data, function ($message) use ($name, $email) {
            $message->to($email, $name)->subject('Bienvenido!');
        });


        $audits->save_audits('Add new Owners:'.$owners->id." - ".$request->fname." ".$request->lname);
        return redirect('owners');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request)
    {

        $module = new Modules;
        $build = new Builds;
        $apartment = new Apartments;
        $iduser = \Auth::id();
        $owner = new Owners;

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        $usersadministrators = new UsersAdministrators;
        $id_administrators = $usersadministrators->get_administrator($iduser);
        foreach ($id_administrators as $id_administrator)
        {
            $id_administrators = $usersadministrators->get_administrator($iduser);
        }

        $owners=array();
        foreach ($id_administrators as $id_administrator)
        {
            $owners = $owner->get_owners_apartments_administrator_builds_id_limit($id_administrator->id_administrator,$request->id);
            $apartments = $owner->get_owners_apartments_administrator_builds_id_limit_id($request->id)->pluck('description','id');
            $apartment_builds = $apartment->get_apartments_builds_owners($owners->pluck('id_builds')->toArray());
            $owners_select = $owner->get_owners_apartments_administrator_builds_id($id_administrator->id_administrator,$request->id);
            $buildsAll = $build->get_builds_user_administrator($id_administrator->id_administrator)->pluck('description','id')->put('','Seleccione un Edificio/Residencia');
            $builds = array_sort_recursive($buildsAll->toArray());
        }

        return view('owners.mod', compact('apartment_builds','owners','module_menus','owners_select','apartments','builds','module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request)
    {
        $audits = new Audits;

        $this->validate($request, [
            'fname' => 'required',
            'lname' => 'required',
            'phonenumber'=>'required',
            'status'=>'required',
            'id_apartments'=>'required'
        ]);


        $owners = Owners::findOrFail($request->id);
        $owners->fname = $request->fname;
        $owners->lname = $request->lname;
        $owners->email = $request->email;
        $owners->phonenumber = $request->phonenumber;
        $owners->status = $request->status;
        $owners->save();

        ApartmentsOwners::where('id_owners','=',$request->id)->delete();

        foreach ($request->id_apartments as $id_apartment) {
            ApartmentsOwners::create([
                'id_owners' => $owners->id,
                'id_apartments' => $id_apartment
            ]);
        }

        $audits->save_audits('Modify Owners:'.$owners->id." - ".$request->fname." ".$request->lname);
        return redirect('owners');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function delete(Request $request)
    {
        $module = new Modules;
        $build = new Builds;
        $apartment = new Apartments;
        $iduser = \Auth::id();
        $owner = new Owners;

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        $usersadministrators = new UsersAdministrators;
        $id_administrators = $usersadministrators->get_administrator($iduser);
        foreach ($id_administrators as $id_administrator)
        {
            $id_administrators = $usersadministrators->get_administrator($iduser);
        }

        $owners=array();
        foreach ($id_administrators as $id_administrator)
        {
            $apartments = $owner->get_owners_apartments_administrator_builds_id_limit_id($request->id)->pluck('description','id');
            $apartment_builds = $apartment->get_apartments_builds();
            $owners = $owner->get_owners_apartments_administrator_builds_id_limit($id_administrator->id_administrator,$request->id);
            $owners_select = $owner->get_owners_apartments_administrator_builds_id($id_administrator->id_administrator,$request->id);
            $buildsAll = $build->get_builds_user_administrator($id_administrator->id_administrator)->pluck('description','id')->put('','Seleccione un Edificio/Residencia');
            $builds = array_sort_recursive($buildsAll->toArray());
        }

        return view('owners.del', compact('apartment_builds','owners','owners_select','module_menus','apartments','builds','module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request)
    {
        $audits = new Audits;
        $apartmentsowners = ApartmentsOwners::where('id_owners','=',$request->id);
        $apartmentsowners->delete();

        $usersowners = UsersOwners::where('id_owners','=',$request->id);
        $usersowners->delete();

        $owners = Owners::find($request->id);
        $owners->delete();

        $audits->save_audits('Deleted Owners:'.$owners->id." - ".$request->fname." ".$request->lname);
        return redirect('owners');
    }
}

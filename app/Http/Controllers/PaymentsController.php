<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Payments;
use App\Modules;
use App\Builds;
use App\UsersAdministrators;
use App\Audits;
use App\Status;
use App\PaymentsDetails;
use App\ViewsOwners;

class PaymentsController extends Controller
{
    /**
     * StatusController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $module = new Modules;
        $payment = new Payments;
        $usersadministrators = new UsersAdministrators;
        $ViewsOwners = new ViewsOwners;

        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);

        $payments = $payment->get_payments_administrators_owners($iduser);
        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        $id_administrators = $usersadministrators->get_administrator($iduser);
        if ($id_administrators->count()>0){
            $administrators['id_administrator']=1;
        }
        else{
            $administrators['id_administrator']=0;
        }

        $ViewsOwners->set_news_owners_administrators($iduser,'cuentas');

        return view('payments.index', compact('administrators','payments', 'user_access','module_principals','module_menus'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(Request $request)
    {
        $module = new Modules;
        $build = new Builds;
        $usersadministrators = new UsersAdministrators;

        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        $id_administrators = $usersadministrators->get_administrator($iduser);
        $builds=array();
        foreach ($id_administrators as $id_administrator)
        {
            $buildsAll = $build->get_builds_user_administrator($id_administrator->id_administrator)->pluck('description','id')->put('','Seleccione un Edificio/Residencia');
            $builds = array_sort_recursive($buildsAll->toArray());
        }
        if($request->typePay=='m') {
            return view('payments.manual', compact('user_access', 'builds', 'module_principals', 'module_menus'));
        }
        if($request->typePay=='a') {
            return view('payments.add', compact('user_access', 'builds', 'module_principals', 'module_menus'));
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function news(Request $request)
    {
        $audits = new Audits;
        $iduser = \Auth::id();
        $payment = new Payments;

        if($request->typePay=='m') {

            $this->validate($request, [
                'id_builds' => 'required',
                'id_apartments' => 'required',
                'description' => 'required',
                'amount' => 'required:number',
            ]);

            $payment->get_payments_administrators_owners_builds_apartments($request->id_builds, $request->id_apartments, $iduser,'manual',$request->description,$request->amount);
            $audits->save_audits('Add new Payments:' . $payment->id." Type: Manual");
            return redirect('payments');


        }elseif($request->typePay=='a') {

            $this->validate($request, [
                'id_builds' => 'required',
                'id_apartments' => 'required'
            ]);

            foreach ($request->id_apartments as $id_apartments) {
                $payment->get_payments_administrators_owners_builds_apartments($request->id_builds, $id_apartments, $iduser,'automatic','Estado de Cuenta Mensual',0);
            }

            $audits->save_audits('Add new Payments:' . $payment->id." Type: Automatic");
            return redirect('payments');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request)
    {
        $module = new Modules;
        $build = new Builds;
        $payment = new Payments;
        $usersadministrators = new UsersAdministrators;

        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);
        $status = Status::all()->where('type','=',1)->pluck('description','id');
        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);
        $id_administrators = $usersadministrators->get_administrator($iduser);

        $builds=array();

        foreach ($id_administrators as $id_administrator)
        {
            $buildsAll = $build->get_builds_user_administrator($id_administrator->id_administrator)->pluck('description','id')->put('','Seleccione un Edificio/Residencia');
            $builds = array_sort_recursive($buildsAll->toArray());
        }

        $payments = $payment->get_payments_id($request->id,$iduser);

        if ($id_administrators->count()>0){
            $administrators['id_administrator']=1;
        }
        else{
            $administrators['id_administrator']=0;
        }

        foreach ($payments as $payment) {
            if ($payment->images <> "") {
                $link = hash('md5', uniqid('142857', true));
                $pathToFile = $link . '.jpg';
                \Image::make($payment->images, 100, 100)->save($pathToFile);
            } else {
                $pathToFile = "";
            }
        }

        return view('payments.mod', compact( 'pathToFile','administrators','payments','status','user_access','builds','module_principals','module_menus'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'description' => 'required',
            'bank' => 'required',
            'number_transaction'=>'required'
        ]);

        $audits = new Audits;
        $iduser = \Auth::id();
        $usersadministrators = new UsersAdministrators;
        $id_administrators = $usersadministrators->get_administrator($iduser);

        if ($id_administrators->count()>0){

            $payments = Payments::find($request->id);
            $payments->id_status=$request->id_status;
            $payments->save();
        }
        else{

            $payments = Payments::find($request->id);
            $payments->id_status = 3;
            $payments->save();

            $PaymentsDetails = PaymentsDetails::where('id_payments','=',$request->id)->get();

            if($PaymentsDetails->count()==0) {

                $link = hash('md5', uniqid('142857',true));
                $pathToFile = $link.'.jpg';
                $images = \Image::make($request->file('images')->getRealPath())
                    ->save($pathToFile);


                PaymentsDetails::create([
                    'id_user' => $iduser,
                    'id_payments' => $request->id,
                    'description' => $request->description,
                    'bank' => $request->bank,
                    'number_transaction' => $request->number_transaction,
                    'images'=>$images
                ]);

                $audits->save_audits('Update Payments:' . $payments->id . " " . $request->description);
            }else{

                $link = hash('md5', uniqid('142857',true));
                $pathToFile = $link.'.jpg';
                $images = \Image::make($request->file('images')->getRealPath())
                    ->save($pathToFile);


                foreach ($PaymentsDetails as $PaymentsDetail) {
                    $PaymentsDetails_search = PaymentsDetails::find($PaymentsDetail->id);
                    $PaymentsDetails_search->description = $request->description;
                    $PaymentsDetails_search->bank = $request->bank;
                    $PaymentsDetails_search->number_transaction = $request->number_transaction;
                    $PaymentsDetails_search->id_user = $iduser;
                    $PaymentsDetails_search->images = $images;
                    $PaymentsDetails_search->save();
                }
                $audits->save_audits('Update Payments:' . $payments->id . " " . $request->description);
            }
        }

        return redirect('payments');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function pdf(Request $request)
    {

        $module = new Modules;
        $build = new Builds;
        $payment = new Payments;
        $usersadministrators = new UsersAdministrators;

        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);
        $status = Status::all()->where('type','=',1)->pluck('description','id');
        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);
        $id_administrators = $usersadministrators->get_administrator($iduser);

        $builds=array();

        foreach ($id_administrators as $id_administrator)
        {
            $buildsAll = $build->get_builds_user_administrator($id_administrator->id_administrator)->pluck('description','id')->put('','Seleccione un Edificio/Residencia');
            $builds = array_sort_recursive($buildsAll->toArray());
        }

        $payments = $payment->get_payments_id($request->id,$iduser);

        if ($id_administrators->count()>0){
            $administrators['id_administrator']=1;
        }
        else{
            $administrators['id_administrator']=0;
        }

        foreach ($payments as $payment) {
            if ($payment->images <> "") {
                $link = hash('md5', uniqid('142857', true));
                $pathToFile = $link . '.jpg';
                \Image::make($payment->images, 100, 100)->save($pathToFile);
            } else {
                $pathToFile = "";
            }
        }


//        return view('payments.mod', compact( 'pathToFile','administrators','payments','status','user_access','builds','module_principals','module_menus'));

        $pdf = \PDF::loadView('payments.pdf', compact('pathToFile','status','administrators','payments','status','user_access'));
        return $pdf->setPaper('letter', 'portrait')->download('payments.pdf');
    }
}

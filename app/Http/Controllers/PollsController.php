<?php

namespace App\Http\Controllers;

use App\Apartments;
use App\ApartmentsOwners;
use App\Audits;
use App\PollsAnwers;
use App\PollsOwnersApartments;
use App\Status;
use App\ViewsOwners;
use Illuminate\Http\Request;

use App\Modules;
use App\Polls;
use App\Builds;
use App\UsersAdministrators;
use Illuminate\Support\Facades\Auth;

class PollsController extends Controller
{
    /**
     * StatusController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $module = new Modules;
        $ViewsOwners = new ViewsOwners;
        $PollsAnwers = new PollsAnwers;

        $usersadministrators = new UsersAdministrators;
        $poll = new Polls;
        $iduser = Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);
        $ViewsOwners->set_news_owners_administrators($iduser, 'encuesta');
        $polls = $poll->get_polls_administrators_owners($iduser);

        $PollsAnwer = $PollsAnwers->get_polls_anwers($iduser);

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        $id_administrators = $usersadministrators->get_administrator($iduser);
        $idAdministrator = "";
        if ($id_administrators->count()>0) {
            foreach ($id_administrators as $id_administrator) {
                $idAdministrator = $id_administrator->id_administrator;
            }
        }


        $status = Status::where('type', '=', 4)->get();
        return view('polls.index', compact('PollsAnwer', 'status', 'idAdministrator', 'polls', 'user_access', 'module_principals', 'module_menus'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(Request $request)
    {
        $module = new Modules;
        $build = new Builds;
        $usersadministrators = new UsersAdministrators;

        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        $id_administrators = $usersadministrators->get_administrator($iduser);
        $builds=array();
        foreach ($id_administrators as $id_administrator) {
            $buildsAll = $build->get_builds_user_administrator($id_administrator->id_administrator)->pluck('description', 'id')->put('', 'Seleccione un Edificio/Residencia');
            $builds = array_sort_recursive($buildsAll->toArray());
        }

        return view('polls.add', compact('user_access', 'builds', 'module_principals', 'module_menus'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function news(Request $request)
    {
        $this->validate($request, [
            'id_builds' => 'required:numeric|exists:builds,id',
            'id_apartments' => 'required:numeric|exists:apartments,id',
            'description'=>'required|min:3',
            'desde'=>'required:date',
            'hasta'=>'required:date'
        ]);

        $audits = new Audits;
        $iduser = \Auth::id();
        $usersadministrators = new UsersAdministrators;

        $id_administrators = $usersadministrators->get_administrator($iduser);
        $idAdministrator = "";

        foreach ($id_administrators as $id_administrator) {
            $idAdministrator = $id_administrator->id_administrator;
        }

        $polls = Polls::create([
            'description' => $request->description,
            'id_user' => $iduser,
            'id_status'=>'7',
            'id_administrator'=>$idAdministrator,
            'id_builds'=>$request->id_builds,
            'from'=>$request->desde,
            'to'=>$request->hasta
        ]);

        foreach ($request->id_apartments as $id_apartment) {
            $idOwners = ApartmentsOwners::where('id_apartments', '=', $id_apartment)->get();
            $IDWner="";
            foreach ($idOwners as $idOwner) {
                $IDWner=$idOwner->id_owners;
            }

            PollsOwnersApartments::create([
                'id_polls'=>$polls->id,
                'id_apartments'=>$id_apartment,
                'id_owners'=>$IDWner,
            ]);
        }

        $audits->save_audits('Add new Poll:'.$polls->id." - ".$request->descriptions);
        return redirect('polls');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request)
    {
        $module = new Modules;
        $apartment = new Apartments;
        $build = new Builds;
        $usersadministrators = new UsersAdministrators;

        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        $id_administrators = $usersadministrators->get_administrator($iduser);
        $builds=array();
        $polls = Polls::find($request->id);

        foreach ($id_administrators as $id_administrator) {
            $buildsAll = $build->get_builds_user_administrator($id_administrator->id_administrator)->pluck('description', 'id')->put('', 'Seleccione un Edificio/Residencia');
            $apartments = $apartment->get_apartments_builds_owners_polls($polls->id_builds);
            $builds = array_sort_recursive($buildsAll->toArray());
        }


        $PollsOwnersApartments = PollsOwnersApartments::where('id_polls', $request->id)->get();
        $status = Status::where('type', '=', 3)->get();
        return view('polls.mod', compact('PollsOwnersApartments', 'status', 'apartments', 'polls', 'user_access', 'builds', 'module_principals', 'module_menus'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'id_builds' => 'required:numeric|exists:builds,id',
            'id_apartments' => 'required:numeric:exists:apartments,id',
            'description'=>'required|min:3',
            'desde'=>'required:date',
            'hasta'=>'required:date',
            'id'=>'required:numeric|exists:polls,id',
            'id_status'=>'required:numeric|exists:status,id'
        ]);

        $audits = new Audits;
        $iduser = \Auth::id();
        $usersadministrators = new UsersAdministrators;

        $id_administrators = $usersadministrators->get_administrator($iduser);
        $idAdministrator = "";

        foreach ($id_administrators as $id_administrator) {
            $idAdministrator = $id_administrator->id_administrator;
        }

        Polls::where('id', $request->id)->update([
            'description' => $request->description,
            'id_user' => $iduser,
            'id_status'=>$request->id_status,
            'id_administrator'=>$idAdministrator,
            'id_builds'=>$request->id_builds,
            'from'=>$request->desde,
            'to'=>$request->hasta
        ]);

        PollsOwnersApartments::where('id_polls', $request->id)->delete();


        foreach ($request->id_apartments as $id_apartment) {
            $idOwners = ApartmentsOwners::where('id_apartments', '=', $id_apartment)->get();
            $IDWner="";
            foreach ($idOwners as $idOwner) {
                $IDWner=$idOwner->id_owners;
            }

            PollsOwnersApartments::create([
                'id_polls'=>$request->id,
                'id_apartments'=>$id_apartment,
                'id_owners'=>$IDWner,
            ]);
        }

        $audits->save_audits('Add Update Poll:'.$request->id." - ".$request->descriptions);
        return redirect('polls');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function delete(Request $request)
    {
        $module = new Modules;
        $apartment = new Apartments;
        $build = new Builds;
        $usersadministrators = new UsersAdministrators;

        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        $id_administrators = $usersadministrators->get_administrator($iduser);
        $builds=array();
        $polls = Polls::find($request->id);

        foreach ($id_administrators as $id_administrator) {
            $buildsAll = $build->get_builds_user_administrator($id_administrator->id_administrator)->pluck('description', 'id')->put('', 'Seleccione un Edificio/Residencia');
            $apartments = $apartment->get_apartments_builds_owners_polls($polls->id_builds);
            $builds = array_sort_recursive($buildsAll->toArray());
        }


        $PollsOwnersApartments = PollsOwnersApartments::where('id_polls', $request->id)->get();
        $status = Status::where('type', '=', 3)->get();
        return view('polls.del', compact('PollsOwnersApartments', 'status', 'apartments', 'polls', 'user_access', 'builds', 'module_principals', 'module_menus'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request)
    {
        $this->validate($request, [
            'id_builds' => 'required:numeric|exists:builds,id',
            'id_apartments' => 'required:numeric:exists:apartments,id',
            'description'=>'required|min:3',
            'desde'=>'required:date',
            'hasta'=>'required:date',
            'id'=>'required:numeric|exists:polls,id'
        ]);

        $audits = new Audits;
        PollsOwnersApartments::where('id_polls', $request->id)->delete();
        PollsAnwers::where('id_polls', $request->id)->delete();
        $polls = Polls::find($request->id);
        $polls->delete();
        $audits->save_audits('Add Delete Poll:'.$request->id." - ".$request->descriptions);
        return redirect('polls');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function anwers(Request $request)
    {
        $this->validate($request, [
            'description'=>'required|min:3',
            'id'=>'required:numeric|exists:polls,id',
            'id_status'=>'required:numeric|exists:status,id'
        ]);

        $audits = new Audits;
        $iduser = \Auth::id();

        PollsAnwers::create([
            'id_polls'=>$request->id,
            'id_user'=>$iduser,
            'id_status'=>$request->id_status,
        ]);

        $audits->save_audits('Add new Anwers:'.$request->id." - ".$request->descriptions);
        return redirect('polls');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(Request $request)
    {
        $module = new Modules;
        $apartment = new Apartments;
        $build = new Builds;
        $poll = new Polls;
        $polls = Polls::find($request->id);

        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        $builds = $build->get_builds_user_administrator($polls->id_administrator);
        $apartments = $apartment->get_apartments_administrator_builds($polls->id_administrator)->pluck('description', 'id');

        $PollsOwnersApartments = PollsOwnersApartments::where('id_polls', $request->id)->get();
        $status = Status::where('type', '=', 3)->get();
        $resultado = $poll->resultados($request->id);

        return view('polls.view', compact('resultado', 'PollsOwnersApartments', 'status', 'apartments', 'polls', 'user_access', 'builds', 'module_principals', 'module_menus'));
    }
}

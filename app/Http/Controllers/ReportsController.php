<?php

namespace App\Http\Controllers;

use App\Builds;
use App\Expense;
use App\Payments;
use App\Status;
use App\UsersAdministrators;
use App\UsersOwners;
use Illuminate\Http\Request;
use App\Modules;


class ReportsController extends Controller
{
    /**
     * ReportsController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     */
    public function reportsaccount(Request $request)
    {
        $module = new Modules;
        $build = new Builds;
        $usersadministrators = new UsersAdministrators;

        $iduser = \Auth::id();

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        $id_administrators = $usersadministrators->get_administrator($iduser);

        $status = Status::all()->where('type','=',1)->pluck('description','id');

        $builds=[];

        if ($id_administrators->count()>0) {
            foreach ($id_administrators as $id_administrator) {
                $idAdministrator = $id_administrator->id_administrator;
                $idAdministratorOwner=$id_administrator->id_administrator;
                $buildsAll = $build->get_builds_apartments_user_administrator($id_administrator->id_administrator)->pluck('description', 'id')->put('', 'Seleccione un Edificio/Residencia');
                $builds = array_sort_recursive($buildsAll->toArray());
            }
        } else {
            $UsersOwners = UsersOwners::where('id_user', '=', $iduser)->get();
            foreach ($UsersOwners as $UsersOwner) {
                $idAdministrator = $UsersOwner->id_administrators;
                $buildsAll = $build->get_builds_apartments_user_administrator($idAdministrator)->pluck('description', 'id')->put('', 'Seleccione un Edificio/Residencia');
                $builds = array_sort_recursive($buildsAll->toArray());
                $idAdministratorOwner ="";
            }
        }
        $values['desde']="";
        $values['hasta']="";

        return view('reports.account', compact('status','values', 'idAdministratorOwner', 'idAdministrator', 'builds', 'module_principals', 'module_menus'));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function account(Request $request)
    {
        $payment = new Payments;
        $iduser = \Auth::id();

        $this->validate($request, [
            'id_builds' => 'required',
            'id_apartments' => 'required',
            'desde' => 'required:date',
            'hasta' => 'required:date',
            'id_status' => 'required',
        ]);

        $payments = $payment->get_payments_administrators_owners_report($iduser,$request->id_builds,
            $request->id_apartments,$request->desde,$request->hasta,$request->id_status);

        $pdf = \PDF::loadView('pdf.account', compact('payments'));
        return $pdf->setPaper('letter', 'landscape')->download('account.pdf');

    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function reportsstates(Request $request)
    {
        $module = new Modules;
        $build = new Builds;
        $usersadministrators = new UsersAdministrators;

        $iduser = \Auth::id();

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        $id_administrators = $usersadministrators->get_administrator($iduser);

        $builds=[];

        if ($id_administrators->count()>0) {
            foreach ($id_administrators as $id_administrator) {
                $idAdministrator = $id_administrator->id_administrator;
                $idAdministratorOwner=$id_administrator->id_administrator;
                $buildsAll = $build->get_builds_apartments_user_administrator($id_administrator->id_administrator)->pluck('description', 'id')->put('', 'Seleccione un Edificio/Residencia');
                $builds = array_sort_recursive($buildsAll->toArray());
            }
        } else {
            $UsersOwners = UsersOwners::where('id_user', '=', $iduser)->get();
            foreach ($UsersOwners as $UsersOwner) {
                $idAdministrator = $UsersOwner->id_administrators;
                $buildsAll = $build->get_builds_apartments_user_administrator($idAdministrator)->pluck('description', 'id')->put('', 'Seleccione un Edificio/Residencia');
                $builds = array_sort_recursive($buildsAll->toArray());
                $idAdministratorOwner ="";
            }
        }
        $values['desde']="";
        $values['hasta']="";

        return view('reports.states', compact('values', 'idAdministratorOwner', 'idAdministrator', 'builds', 'module_principals', 'module_menus'));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function states(Request $request)
    {
        $iduser = \Auth::id();

        $this->validate($request, [
            'id_builds' => 'required',
            'desde' => 'required:date',
            'hasta' => 'required:date',
        ]);

        $administratorId="";

        $id_administrators_owners =  UsersAdministrators::where('id_user', '=', $iduser)->get();
        if($id_administrators_owners->count()>0) {
            foreach ($id_administrators_owners as $id_administrators_owner) {
                $administratorId = $id_administrators_owner->id_administrator;
            }
        }
        else{
            $id_administrators_owners =  UsersOwners::where('id_user','=',$iduser)->get();
            foreach ($id_administrators_owners as $id_administrators_owner) {
                $administratorId=$id_administrators_owner->id_administrators;
            }

        }

        $id_builds=$request->id_builds;

        $expenses = Expense::join('statement','statement.id','=','expense.id_statment')
            ->join('builds', function ($join) use ($id_builds) {
                $join->on('builds.id','=','expense.id_builds')
                    ->where('expense.id_builds','=',$id_builds);
            })
            ->select('expense.*','builds.description as description_builds','statement.type','statement.description as description_statement')
            ->whereBetween('expense.created_at',[$request->desde." 00:00:00",$request->hasta." 23:59:59"])
            ->where('expense.id_administrator','=',$administratorId)
            ->get();



        $pdf = \PDF::loadView('pdf.statess', compact('expenses'));
        return $pdf->setPaper('letter', 'landscape')->download('states.pdf');

    }


}

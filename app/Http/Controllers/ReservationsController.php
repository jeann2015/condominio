<?php

namespace App\Http\Controllers;

use App\ApartmentsOwners;
use App\Audits;
use App\UsersAdministrators;
use App\UsersOwners;
use Illuminate\Http\Request;

use App\Reservations;
use App\Modules;
use App\Builds;
use Illuminate\Support\Facades\Auth;
use Carbon;

class ReservationsController extends Controller
{
    /**
     * StatusController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $module = new Modules;
        $build = new Builds;
        $usersadministrators = new UsersAdministrators;

        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        $id_administrators = $usersadministrators->get_administrator($iduser);

        if($id_administrators->count()>0) {
            foreach ($id_administrators as $id_administrator) {
                $idAdministrator = $iduser;
                $idAdministratorOwner=$id_administrator->id_administrator;
                $buildsAll = $build->get_builds_apartments_user_administrator($id_administrator->id_administrator)->pluck('description', 'id')->put('', 'Seleccione un Edificio/Residencia');
                $builds = array_sort_recursive($buildsAll->toArray());
            }
        }else{
            $UsersOwners = UsersOwners::where('id_user','=',$iduser)->get();
            foreach ($UsersOwners as $UsersOwner){
                $idAdministrator = $iduser;
                $buildsAll = $build->get_builds_apartments_user_administrator($idAdministrator)->pluck('description', 'id')->put('', 'Seleccione un Edificio/Residencia');
                $builds = array_sort_recursive($buildsAll->toArray());
                $idAdministratorOwner ="";
            }

        }

        return view('reservations.index', compact( 'idAdministratorOwner','idAdministrator','builds','user_access','module_principals','module_menus'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function news(Request $request)
    {
        $audits = new Audits;
        $idUser = Auth::id();
        $usersadministrators = new UsersAdministrators;

        $this->validate($request, [
            'description' => 'required',
            'date_from' => 'required|date',
            'time_from' => 'required',
            'date_to' => 'required|date',
            'time_to' => 'required',
            'id_builds' => 'required',
            'id_apartments' => 'required'
        ]);

        $idAdministrators = $usersadministrators->get_administrator($idUser);

        foreach ($idAdministrators as $idAdministrator) {

            $timeFrom = Carbon\Carbon::parse($request->time_from)->format('h:i:s');
            $timeTo = Carbon\Carbon::parse($request->time_to)->format('h:i:s');
            $ApartmentsOwners = ApartmentsOwners::where('id_apartments','=',$request->id_apartments)->limit(1)->get();
            foreach ($ApartmentsOwners as $ApartmentsOwner) {
                $IdOwners = $ApartmentsOwner->id_owners;
                $Reservations = Reservations::create([
                    'description' => $request->description,
                    'date_from' => $request->date_from,
                    'time_from' => $timeFrom,
                    'date_to' => $request->date_to,
                    'time_to' => $timeTo,
                    'id_builds' => $request->id_builds,
                    'id_apartments' => $request->id_apartments,
                    'id_administrator' => $idAdministrator->id_administrator,
                    'id_user' => $idUser,
                    'id_owners' => $IdOwners
                ]);
            }
        }

        $audits->save_audits('Add new Reservations:'.$Reservations->id." - ".$request->description);
        return redirect('reservations');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request)
    {
        $module = new Modules;
        $build = new Builds;
        $usersadministrators = new UsersAdministrators;

        $iduser = \Auth::id();
        $reservations = Reservations::join('builds','builds.id','=','reservations.id_builds')
            ->join('apartments','apartments.id','=','reservations.id_apartments')
            ->select('reservations.*','builds.description as description_builds','apartments.description as description_apartments')
            ->where('reservations.id','=',$request->id)
            ->get();

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        $id_administrators = $usersadministrators->get_administrator($iduser);

        if($id_administrators->count()>0) {
            foreach ($id_administrators as $id_administrator) {
                $idAdministrator = $id_administrator->id_administrator;
                $idAdministratorOwner=$id_administrator->id_administrator;
                $buildsAll = $build->get_builds_apartments_user_administrator($id_administrator->id_administrator)->pluck('description', 'id')->put('', 'Seleccione un Edificio/Residencia');
                $builds = array_sort_recursive($buildsAll->toArray());

            }
        }else{
            $UsersOwners = UsersOwners::where('id_user','=',$iduser)->get();
            foreach ($UsersOwners as $UsersOwner){
                $idAdministrator = $UsersOwner->id_administrators;
                $buildsAll = $build->get_builds_apartments_user_administrator($idAdministrator)->pluck('description', 'id')->put('', 'Seleccione un Edificio/Residencia');
                $builds = array_sort_recursive($buildsAll->toArray());
                $idAdministratorOwner ="";
            }

        }

        return view('reservations.mod', compact('idAdministratorOwner','idAdministrator','reservations','builds','module_menus','module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request)
    {
        $audits = new Audits;
        $idUser = Auth::id();
        $usersadministrators = new UsersAdministrators;

        $this->validate($request, [
            'description' => 'required',
            'date_from' => 'required|date',
            'time_from' => 'required',
            'date_to' => 'required|date',
            'time_to' => 'required'
        ]);

        $idAdministrators = $usersadministrators->get_administrator($idUser);

        foreach ($idAdministrators as $idAdministrator) {

            $timeFrom = Carbon\Carbon::parse($request->time_from)->format('h:i:s');
            $timeTo = Carbon\Carbon::parse($request->time_to)->format('h:i:s');

                $Reservations = Reservations::where('id',$request->id)
                    ->where('id_administrator','=',$idAdministrator->id_administrator)
                    ->update([
                    'description' => $request->description,
                    'date_from' => $request->date_from,
                    'time_from' => $timeFrom,
                    'date_to' => $request->date_to,
                    'time_to' => $timeTo,
                    'id_user' => $idUser
                ]);

        }

        $audits->save_audits('Update Reservations:'.$request->id." - ".$request->description);
        return redirect('reservations');
    }



    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request)
    {
        $audits = new Audits;
        $reservations = Reservations::find($request->idD);
        $reservations->delete();

        $audits->save_audits('Deleted Reservations:'.$reservations->id." - ".$request->description);
        return redirect('reservations');
    }
}

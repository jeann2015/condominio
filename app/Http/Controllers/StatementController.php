<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Statement;
use App\Modules;
use App\Audits;
use App\UsersAdministrators;

class StatementController extends Controller
{
    /**
     * StatusController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $module = new Modules;
        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);

        $id_administrators_owners =  UsersAdministrators::where('id_user', '=', $iduser)->get();
        $administrator_id ="";
        foreach ($id_administrators_owners as $id_administrators_owner) {
            $administrator_id = $id_administrators_owner->id_administrator;
        }
        $statement = Statement::where('id_administrator','=',$administrator_id)->get();
        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);
        return view('statements.index', compact('statement', 'user_access','module_principals','module_menus'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(Request $request)
    {
        $module = new Modules;
        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);
        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);
        return view('statements.add', compact('user_access','module_principals','module_menus'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function news(Request $request)
    {
        $audits = new Audits;
        $iduser = \Auth::id();

        $this->validate($request, [
            'description' => 'required',
            'type' => 'required'
        ]);

        $id_administrators_owners =  UsersAdministrators::where('id_user', '=', $iduser)->get();

        foreach ($id_administrators_owners as $id_administrators_owner) {
            $statements = Statement::create([
                'description' => $request->description,
                'type' => $request->type,
                'id_administrator' =>$id_administrators_owner->id_administrator
            ]);
        }

        $audits->save_audits('Add new Statements:'.$statements->id." - ".$request->description);
        return redirect('statements');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request)
    {
        $module = new Modules;
        $iduser = \Auth::id();

        $id_administrators_owners =  UsersAdministrators::where('id_user', '=', $iduser)->get();
        foreach ($id_administrators_owners as $id_administrators_owner) {
            $administrator_id = $id_administrators_owner->id_administrator;
        }

        $statements = Statement::where('id','=',$request->id)->where('id_administrator','=',$administrator_id)->get();
        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        return view('statements.mod', compact('statements','module_menus','module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request)
    {
        $audits = new Audits;
        $iduser = \Auth::id();

        $this->validate($request, [
            'description' => 'required',
            'type' => 'required',
            'id' => 'required'
        ]);

        $id_administrators_owners =  UsersAdministrators::where('id_user', '=', $iduser)->get();
        foreach ($id_administrators_owners as $id_administrators_owner) {
            $administrator_id = $id_administrators_owner->id_administrator;
        }

        $statements = Statement::find($request->id);
        $statements->description = $request->description;
        $statements->type = $request->type;
        $statements->id_administrator = $administrator_id;
        $statements->save();

        $audits->save_audits('Modify Statements:'.$statements->id." - ".$request->description);
        return redirect('statements');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function delete(Request $request)
    {
        $module = new Modules;
        $iduser = \Auth::id();

        $id_administrators_owners =  UsersAdministrators::where('id_user', '=', $iduser)->get();
        foreach ($id_administrators_owners as $id_administrators_owner) {
            $administrator_id = $id_administrators_owner->id_administrator;
        }

        $statements = Statement::where('id','=',$request->id)->where('id_administrator','=',$administrator_id)->get();
        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        return view('statements.del', compact('statements','module_menus','module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request)
    {
        $audits = new Audits;

        $this->validate($request, [
            'description' => 'required',
            'type' => 'required',
            'id' => 'required'
        ]);

        $statements = Statement::find($request->id);
        $statements->delete();
        $audits->save_audits('Deleted Statements:'.$statements->id." - ".$request->description);
        return redirect('statements');
    }
}

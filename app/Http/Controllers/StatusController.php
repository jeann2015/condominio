<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Status;
use App\Modules;
use App\Audits;

class StatusController extends Controller
{
    /**
     * StatusController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $module = new Modules;
        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);
        $status = Status::all();


        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        return view('status.index', compact('status', 'user_access','module_principals','module_menus'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(Request $request)
    {
        $module = new Modules;
        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);
        $status = Status::all();


        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        return view('status.add', compact('status', 'user_access','module_principals','module_menus'));

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function news(Request $request)
    {
        $audits = new Audits;

        $status = Status::create([
            'description'=>$request->description,
            'type'=>$request->types
        ]);


        $audits->save_audits('Add new Status:'.$status->id." - ".$request->description);
        return redirect('status');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request)
    {
        $module = new Modules;
        $iduser = \Auth::id();
        $status = Status::find($request->id);

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        return view('status.mod', compact('status','module_menus','module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request)
    {
        $audits = new Audits;

        $status = Status::find($request->id);
        $status->description = $request->description;
        $status->type = $request->types;
        $status->save();

        $audits->save_audits('Modify Status:'.$status->id." - ".$request->description);
        return redirect('status');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function delete(Request $request)
    {
        $module = new Modules;
        $iduser = \Auth::id();
        $status = Status::find($request->id);
        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        return view('status.del', compact('status','module_menus','module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request)
    {
        $audits = new Audits;

        $status = Status::find($request->id);
        $status->delete();

        $audits->save_audits('Deleted Status:'.$status->id." - ".$request->description);
        return redirect('status');
    }
}


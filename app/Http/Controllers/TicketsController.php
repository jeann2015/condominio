<?php

namespace App\Http\Controllers;


use App\BuildsApartments;
use Illuminate\Http\Request;

use App\Tickets;
use App\Modules;
use App\UsersAdministrators;
use App\Builds;
use App\Owners;
use App\Apartments;
use App\UsersOwners;
use App\Types;
use App\Audits;
use App\Status;
use App\TicketsAnswers;
use App\ViewsOwners;
use App\ApartmentsOwners;

class TicketsController extends Controller
{
    /**
     * OwnersController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $module = new Modules;
        $ticket = new Tickets;
        $ViewsOwners = new ViewsOwners;

        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);
        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);
        $tickets = $ticket->get_tickets_administrators_owners($iduser);

        $ViewsOwners->set_news_owners_administrators($iduser,'tickets');

        return view('tickets.index', compact('tickets', 'user_access','module_principals','module_menus'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(Request $request)
    {
        $module = new Modules;
        $build = new Builds;
        $owners = new Owners;
        $apartment = new Apartments;

        $usersadministrators = new UsersAdministrators;

        $types = Types::all()->pluck('description','id');

        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);
        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        $id_administrators_owners =  UsersOwners::where('id_user','=',$iduser)->get();

        if($id_administrators_owners->count()>0) {
            foreach ($id_administrators_owners as $id_administrators_owner) {
                $apartments = $owners->get_owners_apartments_administrator_builds_id_tickets($id_administrators_owner->id_administrators, $id_administrators_owner->id_owners);
                $buildsAll = $build->get_builds_user_administrator($id_administrators_owner->id_administrators)->pluck('description','id')->put('','Seleccione un Edificio/Residencia');
                $builds = array_sort_recursive($buildsAll->toArray());
            }
        }else{
//            $id_administrators_owners = $usersadministrators->get_administrator($iduser);
//            foreach ($id_administrators_owners as $id_administrators_owner) {
//                $apartments = $owners->get_owners_apartments_administrator($id_administrators_owner->id_administrator);
//                $builds = $build->get_builds_user_administrator($id_administrators_owner->id_administrator);
//            }

            $id_administrators = $usersadministrators->get_administrator($iduser);

            $apartments=array();
            $builds=array();
            foreach ($id_administrators as $id_administrator)
            {
                $apartments = $apartment->get_apartments_administrator_builds_without($id_administrator->id_administrator);
                $buildsAll = $build->get_builds_user_administrator($id_administrator->id_administrator)->pluck('description','id')->put('','Seleccione un Edificio/Residencia');
                $builds = array_sort_recursive($buildsAll->toArray());
            }
        }

        return view('tickets.add', compact( 'types','builds','apartments','user_access','module_principals','module_menus'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function news(Request $request)
    {

        $this->validate($request, [
            'id_apartments' => 'required:numeric',
            'id_types' => 'required:numeric',
            'description'=>'required'
        ]);

        $audits = new Audits;
        $iduser = \Auth::id();
        $user_administrator_builds_apartments =  new Tickets;
        $id_administrators_owners =  UsersOwners::where('id_user', '=', $iduser)->get();

        if($id_administrators_owners->count()==0){
            $ApartmentsOwners = ApartmentsOwners::where('id_apartments','=',$request->id_apartments)->get();
            foreach ($ApartmentsOwners as $ApartmentsOwner){
                $UsersOwners = UsersOwners::where('id_owners','=',$ApartmentsOwner->id_owners)->get();
                foreach ($UsersOwners as $UsersOwner){
                    $iduser = $UsersOwner->id_user;
                }
            }
        }

        $ticket = $user_administrator_builds_apartments->get_administrators_builds_owners($iduser,$request->id_apartments);
        $BuildsApartment = BuildsApartments::where('id_apartments','=',$request->id_apartments)->pluck('id_builds');

        foreach ($ticket as $ticke) {

            $tickets = Tickets::create([
                'id_status' => 4,
                'id_administrator' => $ticke->id_administrators,
                'id_builds' => $BuildsApartment[0],
                'id_apartments' => $request->id_apartments,
                'id_types' => $request->id_types,
                'id_user' => $iduser,
                'id_owners' => $ticke->id_owners,
            ]);
        }

        TicketsAnswers::create([
            'answer'=>$request->description,
            'id_tickets'=>$tickets->id,
            'id_user' => $iduser,
        ]);

        $audits->save_audits('Add new Tickets:'.$tickets->id." - ".$request->descriptions);
        return redirect('tickets');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(Request $request)
    {
        $module = new Modules;
        $build = new Builds;
        $owners = new Owners;
        $usersadministrators = new UsersAdministrators;
        $tickets = Tickets::find($request->id);

        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);
        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);
        $types = Types::all()->where('id','=',$tickets->id_types)->pluck('description','id');
        $id_administrators_owners =  UsersOwners::where('id_user','=',$iduser)->get();
        $tickets_anwers = TicketsAnswers::join('users','users.id','=','tickets_answers.id_user')
            ->select('tickets_answers.*','users.name')
            ->where('id_tickets','=',$request->id)
            ->orderby('tickets_answers.id','desc')
            ->get();

        $apartments=array();
        $builds=array();

       if($id_administrators_owners->count()>0) {

           $status = Status::all()->where('id','=',$tickets->id_status)->pluck('description','id');
           $owners_data = Owners::where('id','=',$tickets->id_owners)->get();


           foreach ($id_administrators_owners as $id_administrators_owner) {
               $apartments = $owners->get_owners_apartments_administrator_builds_id_tickets($id_administrators_owner->id_administrators, $id_administrators_owner->id_owners);
               $builds = $build->get_builds_user_administrator($id_administrators_owner->id_administrators);
           }

           return view('tickets.mod', compact( 'tickets_anwers','owners_data','status','tickets','types','builds','apartments','user_access','module_principals','module_menus'));

       }else{
           $status = Status::all()->where('type','=',2)->pluck('description','id');
           $id_administrators_owners = $usersadministrators->get_administrator($iduser);
           $owners_data = Owners::where('id','=',$tickets->id_owners)->get();

           foreach ($id_administrators_owners as $id_administrators_owner) {
               $apartments = $owners->get_owners_apartments_administrator($id_administrators_owner->id_administrator);
               $builds = $build->get_builds_user_administrator($id_administrators_owner->id_administrator);
           }

           return view('tickets.mod', compact( 'tickets_anwers','owners_data','status','tickets','types','builds','apartments','user_access','module_principals','module_menus'));
       }


    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request)
    {
        $audits = new Audits;
        $iduser = \Auth::id();

        $tickets = Tickets::find($request->id);
        $tickets->id_status = $request->id_status;
        $tickets->save();

        if($request->description_resp<>"") {
            TicketsAnswers::create([
                'answer' => $request->description_resp,
                'id_tickets' => $tickets->id,
                'id_user' => $iduser,
            ]);
        }

        $audits->save_audits('Modify Apartments:'.$request->id." - ".$request->description);
        return redirect('tickets');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function delete(Request $request)
    {
        $module = new Modules;
        $build = new Builds;
        $owners = new Owners;
        $tickets = Tickets::find($request->id);
        $types = Types::all()->where('id','=',$tickets->id_types)->pluck('description','id');
        $status = Status::all()->where('id','=',$tickets->id_status)->pluck('description','id');


        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);
        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        $id_administrators_owners =  UsersOwners::where('id_user','=',$iduser)->get();

        $apartments=array();
        $builds=array();

        foreach ($id_administrators_owners as $id_administrators_owner) {
            $apartments = $owners->get_owners_apartments_administrator_builds_id_tickets($id_administrators_owner->id_administrators, $id_administrators_owner->id_owners);
            $builds = $build->get_builds_user_administrator($id_administrators_owner->id_administrators);
        }

        return view('tickets.del', compact( 'status','tickets','types','builds','apartments','user_access','module_principals','module_menus'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request)
    {
        $audits = new Audits;

        $buildsapartments = BuildsApartments::where('id_apartments','=',$request->id);
        $buildsapartments->delete();

        $apartments = Apartments::find($request->id);
        $apartments->delete();

        $audits->save_audits('Deleted Apartments:'.$apartments->id." - ".$request->description);
        return redirect('apartments');
    }
}

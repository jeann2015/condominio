<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Owners extends Model
{
    protected $table="owners";

    protected $fillable = [
        'fname','lname','email','phonenumber','id_user','status'
    ];

    /**
     * @param $id_administrator
     * @return mixed
     */
    public function get_owners_apartments_administrator_builds($id_administrator)
    {
        $UsersOwners = new UsersOwners;
        $iduser = \Auth::id();

        $builds = $UsersOwners->get_builds_owners_administrator($iduser);

        $owners = \DB::table('owners')
            ->select('apartments.description as description_apartments','owners.*','builds.description as description_builds')
            ->join('apartments_owners','apartments_owners.id_owners','=','owners.id')
            ->join('apartments','apartments_owners.id_apartments','=','apartments.id')
            ->join('builds_apartments','apartments.id','=','builds_apartments.id_apartments')
            ->join('builds','builds_apartments.id_builds','=','builds.id')
            ->join('users_owners','apartments_owners.id_owners','=','users_owners.id_owners')
            ->whereIN('builds.id',$builds->toArray())->get();
        return  $owners;
    }

    /**
     * @param $id_administrator
     * @return mixed
     */
    public function get_owners_apartments_administrator($id_administrator)
    {
        $UsersOwners = new UsersOwners;
        $iduser = \Auth::id();
        $builds = $UsersOwners->get_builds_owners_administrator($iduser);

        $AdministratorsBuilds = \DB::table('apartments')
            ->select(
                'apartments.id',
                'apartments.description as description_apartments',
                'builds.description as description_builds',
                'builds.id as id_builds'
            )
            ->join('builds_apartments', function ($join) use ($builds) {
                    $join->on('builds_apartments.id_builds', '=', 'builds_apartments.id_builds')
                    ->whereIN('builds_apartments.id_builds', $builds);
            })
            ->join('builds','builds.id','=','builds_apartments.id_builds')
//            ->join('builds_apartments','builds_apartments.id_apartments','=','apartments.id')
            ->join('apartments_owners','apartments_owners.id_apartments','=','apartments.id')
//            ->whereIN('builds_apartments.id_builds',$builds->toArray())
            ->get();

        return  $AdministratorsBuilds;
    }

    /**
     * @param $id_administrator
     * @param $id
     * @return mixed
     */
    public function get_owners_apartments_administrator_builds_id($id_administrator,$id)
    {
        $UsersOwners = new UsersOwners;
        $iduser = \Auth::id();

        $builds = $UsersOwners->get_builds_owners_administrator($iduser);

        $owners = \DB::table('owners')
            ->select('apartments.description as description_apartments','owners.*',
                'builds.description as description_builds',
                'apartments_owners.id_apartments','builds_apartments.id_builds')
            ->join('apartments_owners','apartments_owners.id_owners','=','owners.id')
            ->join('apartments','apartments_owners.id_apartments','=','apartments.id')
            ->join('builds_apartments','apartments.id','=','builds_apartments.id_apartments')
            ->join('builds','builds_apartments.id_builds','=','builds.id')
            ->join('users_owners','apartments_owners.id_owners','=','users_owners.id_owners')
            ->where('owners.id','=',$id)
            ->whereIn('builds.id',$builds->toArray())
            ->get();
        return  $owners;
    }

    /**
     * @param $id_administrator
     * @param $id
     * @return mixed
     */
    public function get_owners_apartments_administrator_builds_id_limit($id_administrator,$id)
    {
        $UsersOwners = new UsersOwners;
        $iduser = \Auth::id();

        $builds = $UsersOwners->get_builds_owners_administrator($iduser);

        $owners = \DB::table('owners')
            ->select('apartments.description as description_apartments','owners.*',
                'builds.description as description_builds',
                'apartments_owners.id_apartments','builds_apartments.id_builds','apartments.id','owners.id')
            ->join('apartments_owners','apartments_owners.id_owners','=','owners.id')
            ->join('apartments','apartments_owners.id_apartments','=','apartments.id')
            ->join('builds_apartments','apartments.id','=','builds_apartments.id_apartments')
            ->join('builds','builds_apartments.id_builds','=','builds.id')
            ->join('users_owners','apartments_owners.id_owners','=','users_owners.id_owners')
//            ->limit(1)
            ->where('owners.id','=',$id)
            ->whereIN('builds.id',$builds)->get();
        return  $owners;
    }

    /**
     * @param $id_administrator
     * @param $id
     * @return mixed
     */
    public function get_owners_apartments_administrator_builds_id_limit_id($id)
    {
        $UsersOwners = new UsersOwners;
        $iduser = \Auth::id();

        $builds = $UsersOwners->get_builds_owners_administrator($iduser);

        $owners = \DB::table('owners')
            ->select('apartments.*')
            ->join('apartments_owners','apartments_owners.id_owners','=','owners.id')
            ->join('apartments','apartments_owners.id_apartments','=','apartments.id')
            ->join('builds_apartments','apartments.id','=','builds_apartments.id_apartments')
            ->join('builds','builds_apartments.id_builds','=','builds.id')
            ->join('users_owners','apartments_owners.id_owners','=','users_owners.id_owners')
//            ->limit(1)
            ->where('owners.id','=',$id)
            ->whereIN('builds.id',$builds)->get();
        return  $owners;
    }

    /**
     * @param $id_administrator
     * @param $id
     * @return mixed
     */
    public function get_owners_apartments_administrator_builds_id_tickets($id_administrator,$id)
    {
        $owners = \DB::table('owners')
            ->select('apartments.description as description_apartments','owners.*',
                'builds.description as description_builds',
                'apartments_owners.id_apartments','builds_apartments.id_builds')
            ->join('apartments_owners','apartments_owners.id_owners','=','owners.id')
            ->join('apartments','apartments_owners.id_apartments','=','apartments.id')
            ->join('builds_apartments','apartments.id','=','builds_apartments.id_apartments')
            ->join('builds','builds_apartments.id_builds','=','builds.id')
            ->join('users_owners','apartments_owners.id_owners','=','users_owners.id_owners')
            ->where('owners.id','=',$id)
            ->where('users_owners.id_administrators','=',$id_administrator)->get();
        return  $owners;
    }
}

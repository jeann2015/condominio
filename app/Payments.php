<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payments extends Model
{
    protected $table="payments";

    protected $fillable = [
        'description','id_user','id_status','id_administrator',
        'id_builds','id_apartments','id_owners','amount','type'
    ];

    /**
     * @param $id_user
     * @param $id_builds
     * @param $id_apartments
     * @param $from
     * @param $to
     * @return array
     */
    public function get_payments_administrators_owners_report($id_user,$id_builds,$id_apartments,$from,$to,$id_status)
    {
        $usersadministrators = new UsersAdministrators;
        $id_administrators = $usersadministrators->get_administrator($id_user);
        $payments=collect([]);

        $UsersOwners = new UsersOwners;
        $iduser = \Auth::id();
        $builds = $UsersOwners->get_builds_owners_administrator($iduser);


        if ($id_administrators->count() > 0) {

            foreach ($id_administrators as $id_administrator) {

                $payments = self::join('owners','owners.id','=','payments.id_owners')
                    ->join('builds', function ($join) use ($id_builds) {
                        $join->on('builds.id','=','payments.id_builds')
                        ->where('payments.id_builds','=',$id_builds);
                    })
                    ->join('apartments', function ($join) use ($id_apartments) {
                        if($id_apartments==0) {
                            $join->on('apartments.id', '=', 'payments.id_apartments');
                        }else{
                            $join->on('apartments.id', '=', 'payments.id_apartments')
                            ->where('payments.id_apartments', '=', $id_apartments);
                        }
                    })
                    ->join('status', function ($join) use ($id_status) {
                        if($id_status==0) {
                            $join->on('status.id', '=', 'payments.id_status');
                        }else{
                            $join->on('status.id', '=', 'payments.id_status')
                                ->where('payments.id_status', '=', $id_status);
                        }
                    })
                    ->select('payments.*',
                        \DB::raw('concat(owners.fname," ",owners.lname) as description_owners'),
                        'builds.description as description_builds',
                        'apartments.description as description_apartments',
                        'status.description as description_status')
                    ->whereIN('builds.id',$builds->toArray())
                    ->whereBetween('payments.created_at',[$from." 00:00:00",$to." 23:59:59"])
                    ->orderby('payments.id','desc')
                    ->get();
            }

        }else{
            $id_administrators_owners =  UsersOwners::where('id_user','=',$iduser)->get();

            $id_owners="";
            if($id_administrators_owners->count()>0) {
                foreach ($id_administrators_owners as $id_administrators_owner) {
                    $id_owners = $id_administrators_owner->id_owners;
                }
            }

            $payments = self::join('owners','owners.id','=','payments.id_owners')
                ->join('builds', function ($join) use ($id_builds) {
                    $join->on('builds.id','=','payments.id_builds')
                        ->where('payments.id_builds','=',$id_builds);
                })
                ->join('apartments', function ($join) use ($id_apartments) {
                    if($id_apartments==0) {
                        $join->on('apartments.id', '=', 'payments.id_apartments');
                    }else{
                        $join->on('apartments.id', '=', 'payments.id_apartments')
                            ->where('payments.id_apartments', '=', $id_apartments);
                    }
                })
                ->join('status', function ($join) use ($id_status) {
                    if($id_status==0) {
                        $join->on('status.id', '=', 'payments.id_status');
                    }else{
                        $join->on('status.id', '=', 'payments.id_status')
                            ->where('payments.id_status', '=', $id_status);
                    }
                })
                ->select('payments.*',
                    \DB::raw('concat(owners.fname," ",owners.lname) as description_owners'),
                    'builds.description as description_builds',
                    'apartments.description as description_apartments',
                    'status.description as description_status')
                ->where('owners.id','=',$id_owners)
                ->whereBetween('payments.created_at',[$from." 00:00:00",$to." 23:59:59"])
                ->orderby('payments.id','desc')
                ->get();
        }
        return $payments;
    }

    /**
     * @param $id
     * @param $id_user
     * @return array
     */
    public function get_payments_id($id,$id_user){

        $usersadministrators = new UsersAdministrators;
        $id_administrators = $usersadministrators->get_administrator($id_user);

        $UsersOwners = new UsersOwners;
        $iduser = \Auth::id();

        $builds = $UsersOwners->get_builds_owners_administrator($iduser);

        $payments=array();

        if ($id_administrators->count() > 0) {

            foreach ($id_administrators as $id_administrator) {

                $payments = self::join('owners','owners.id','=','payments.id_owners')
                    ->join('builds','builds.id','=','payments.id_builds')
                    ->join('apartments','apartments.id','=','payments.id_apartments')
                    ->join('status','status.id','=','payments.id_status')
                    ->leftjoin('payments_details','payments_details.id_payments','=','payments.id')
                    ->select('payments.*',
                        \DB::raw('concat(owners.fname," ",owners.lname) as description_owners'),
                        'builds.description as description_builds',
                        'apartments.description as description_apartments',
                        'status.description as description_status',
                        'payments_details.number_transaction','payments_details.bank',
                        'payments_details.description as description_details','payments_details.images')
                    ->whereIn('builds.id',$builds->toArray())
                    ->where('payments.id','=',$id)
                    ->orderby('payments.id','desc')
                    ->get();
            }

        }else{

            $UsersOwners = UsersOwners::where('id_user','=',$id_user)->get();

            foreach ($UsersOwners as $UsersOwner) {

                $payments = self::join('owners', 'owners.id', '=', 'payments.id_owners')
                    ->join('builds', 'builds.id', '=', 'payments.id_builds')
                    ->join('apartments', 'apartments.id', '=', 'payments.id_apartments')
                    ->join('status', 'status.id', '=', 'payments.id_status')
                    ->leftjoin('payments_details','payments_details.id_payments','=','payments.id')
                    ->select('payments.*',
                        \DB::raw('concat(owners.fname," ",owners.lname) as description_owners'),
                        'builds.description as description_builds',
                        'apartments.description as description_apartments',
                        'status.description as description_status',
                        'payments_details.number_transaction','payments_details.bank',
                        'payments_details.description as description_details','payments_details.images')
                    ->where('payments.id_owners', '=', $UsersOwner->id_owners)
                    ->where('payments.id','=',$id)
                    ->orderby('payments.id', 'desc')
                    ->get();

            }
        }
        return $payments;
    }

    /**
     * @param $id_user
     * @return array
     */
    public function get_payments_administrators_owners($id_user)
    {
        $usersadministrators = new UsersAdministrators;
        $iduser = \Auth::id();

        $id_administrators = $usersadministrators->get_administrator($id_user);
        $payments=array();

        $UsersOwners = new UsersOwners;
        $builds = $UsersOwners->get_builds_owners_administrator($iduser);

        if ($id_administrators->count() > 0) {
            foreach ($id_administrators as $id_administrator) {
                $payments = self::join('owners','owners.id','=','payments.id_owners')
                        ->join('builds','builds.id','=','payments.id_builds')
                        ->join('apartments','apartments.id','=','payments.id_apartments')
                        ->join('status','status.id','=','payments.id_status')
                        ->select('payments.*',
                            \DB::raw('concat(owners.fname," ",owners.lname) as description_owners'),
                            'builds.description as description_builds',
                            'apartments.description as description_apartments',
                            'status.description as description_status')
                        ->whereIN('builds.id',$builds->toArray())
                        ->orderby('payments.id','desc')
                        ->get();
            }

        }else{
            $UsersOwners = UsersOwners::where('id_user','=',$id_user)->get();
            foreach ($UsersOwners as $UsersOwner) {
                $payments = self::join('owners', 'owners.id', '=', 'payments.id_owners')
                    ->join('builds', 'builds.id', '=', 'payments.id_builds')
                    ->join('apartments', 'apartments.id', '=', 'payments.id_apartments')
                    ->join('status', 'status.id', '=', 'payments.id_status')
                    ->select('payments.*',
                        \DB::raw('concat(owners.fname," ",owners.lname) as description_owners'),
                        'builds.description as description_builds',
                        'apartments.description as description_apartments',
                        'status.description as description_status')
                    ->where('payments.id_owners', '=', $UsersOwner->id_owners)
                    ->orderby('payments.id', 'desc')
                    ->get();
            }
        }

        return $payments;
    }

    /**
     * @param $id_builds
     * @param $id_apartments
     * @param $id_user
     * @return mixed
     */
    public function get_payments_administrators_owners_builds_apartments($id_builds,$id_apartments,$id_user,$type,$description,$amounts)
    {
        $ApartmentsOwners = ApartmentsOwners::join('users_owners','users_owners.id_owners','=','apartments_owners.id_owners')
            ->join('apartments','apartments_owners.id_apartments','=','apartments.id')
            ->join('builds_apartments','apartments.id','=','builds_apartments.id_apartments')
            ->join('builds','builds_apartments.id_builds','=','builds.id')
            ->where('apartments_owners.id_apartments','=',$id_apartments)
            ->select(
                'users_owners.id_administrators',
                'users_owners.id_owners',
                'apartments.meters','builds.amount_maintenance_meters'
            )
            ->get();

        foreach ($ApartmentsOwners as $ApartmentsOwner) {
            if($type=='automatic') {
                $amount = number_format($ApartmentsOwner->amount_maintenance_meters * $ApartmentsOwner->meters, 2);
            }
            if($type=='manual') {
                $amount=$amounts;
            }

            $payments = Payments::create([
                'id_user' => $id_user,
                'id_administrator' => $ApartmentsOwner->id_administrators,
                'id_builds' => $id_builds,
                'id_apartments' => $id_apartments,
                'id_status' => 2,
                'id_owners' => $ApartmentsOwner->id_owners,
                'amount' => $amount,
                'type' => $type,
                'description' => $description
            ]);

        }
        return $payments;
    }
}

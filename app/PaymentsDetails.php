<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentsDetails extends Model
{
    protected $table="payments_details";

    protected $fillable = [
        'description','id_payments','id_user','images','bank','number_transaction'
    ];
}

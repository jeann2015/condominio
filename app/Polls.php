<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Polls extends Model
{
    protected $table="polls";

    protected $fillable = [
        'description','id_user','id_status','id_administrator','id_builds','from','to','id_status'
    ];

    /**
     * @param $id_user
     * @return array
     */
    public function get_polls_administrators_owners($id_user)
    {
        $usersadministrators = new UsersAdministrators;
        $id_administrators = $usersadministrators->get_administrator($id_user);
        $carbon = new Carbon();
        $to = $carbon->toDateString();

        $UsersOwners = new UsersOwners;
        $iduser = \Auth::id();

        $builds = $UsersOwners->get_builds_owners_administrator($iduser);

        $tickets=array();

        if ($id_administrators->count() > 0) {

            foreach ($id_administrators as $id_administrator) {
                $polls = self::join('builds','builds.id','=','polls.id_builds')
                    ->join('status','status.id','=','polls.id_status')
                    ->select('polls.*',
                        'builds.description as description_builds',
                        'status.description as description_status'
                    )
                    ->whereIN('builds.id',$builds)
                    ->get();
            }

        }else{
            $id_administrators_owners =  UsersOwners::where('id_user','=',$id_user)->get();
            $idOwner="";
            foreach ($id_administrators_owners as $id_administrators_owner) {
                $idOwner = $id_administrators_owner->id_owners;
            }
            $polls = self::join('builds','builds.id','=','polls.id_builds')
                ->join('status','status.id','=','polls.id_status')
                ->join('polls_owners_apartments', function ($join) use($idOwner) {
                    $join->on('polls_owners_apartments.id_polls','=','polls.id')
                        ->where('polls_owners_apartments.id_owners','=',$idOwner);
                })
                ->select('polls.*',
                    'builds.description as description_builds',
                    'status.description as description_status'
                )
                ->whereIn('polls.id_status',[7,8])
                ->where('polls.to','>=',$to)
                ->get();
        }
        return $polls;
    }

    /**
     * @param $id
     * @param $id_user
     * @return array
     */
    public function get_pools_id($id,$id_user){

        $usersadministrators = new UsersAdministrators;
        $id_administrators = $usersadministrators->get_administrator($id_user);

        $payments=array();

        if ($id_administrators->count() > 0) {

            foreach ($id_administrators as $id_administrator) {

                $payments = self::join('owners','owners.id','=','payments.id_owners')
                    ->join('builds','builds.id','=','polls.id_builds')
                    ->join('apartments','apartments.id','=','polls.id_apartments')
                    ->join('status','status.id','=','polls.id_status')
                    ->leftjoin('polls_anwers','polls_anwers.id_polls','=','polls.id')
                    ->select('polls.*',
                        \DB::raw('concat(owners.fname," ",owners.lname) as description_owners'),
                        'builds.description as description_builds',
                        'apartments.description as description_apartments',
                        'status.description as description_status'
                        )
                    ->where('polls.id_administrator','=',$id_administrator->id_administrator)
                    ->where('polls.id','=',$id)
                    ->orderby('polls.id','desc')
                    ->get();
            }

        }else{

            $UsersOwners = UsersOwners::where('id_user','=',$id_user)->get();

            foreach ($UsersOwners as $UsersOwner) {

                $idOwner = $UsersOwner->id_owners;

                $payments = self::join('owners', 'owners.id', '=', 'polls_owners_apartments.id_owners')
                    ->join('builds', 'builds.id', '=', 'polls.id_builds')
                    ->join('apartments', 'apartments.id', '=', 'polls.id_apartments')
                    ->join('status', 'status.id', '=', 'polls.id_status')
                    ->leftjoin('polls_anwers','polls_anwers.id_polls','=','polls.id')
                    ->join('polls_owners_apartments', function ($join) use($idOwner) {
                        $join->on('polls_owners_apartments.id_polls','=','polls.id')
                            ->where('polls_owners_apartments.id_owners','=',$idOwner);
                    })
                    ->select('polls.*',
                        \DB::raw('concat(owners.fname," ",owners.lname) as description_owners'),
                        'builds.description as description_builds',
                        'apartments.description as description_apartments',
                        'status.description as description_status'
                        )
                    ->where('polls.id','=',$id)
                    ->orderby('polls.id', 'desc')
                    ->get();

            }
        }
        return $payments;
    }

    /**
     * @param $id_user
     * @return array
     */
    public function get_pools_administrators_owners($id_user)
    {
        $usersadministrators = new UsersAdministrators;
        $id_administrators = $usersadministrators->get_administrator($id_user);
        $payments=array();
        if ($id_administrators->count() > 0) {
            foreach ($id_administrators as $id_administrator) {
                $payments = self::join('owners','owners.id','=','polls_owners_apartments.id_owners')
                    ->join('builds','builds.id','=','polls.id_builds')
                    ->join('apartments','apartments.id','=','polls.id_apartments')
                    ->join('status','status.id','=','polls.id_status')
                    ->select('polls.*',
                        \DB::raw('concat(owners.fname," ",owners.lname) as description_owners'),
                        'builds.description as description_builds',
                        'apartments.description as description_apartments',
                        'status.description as description_status')
                    ->where('polls.id_administrator','=',$id_administrator->id_administrator)
                    ->orderby('polls.id','desc')
                    ->get();
            }
        }else{
            $UsersOwners = UsersOwners::where('id_user','=',$id_user)->get();
            foreach ($UsersOwners as $UsersOwner) {
                $idOwner = $UsersOwner->id_owners;
                $payments = self::join('owners', 'owners.id', '=', 'polls_owners_apartments.id_owners')
                    ->join('builds', 'builds.id', '=', 'polls.id_builds')
                    ->join('apartments', 'apartments.id', '=', 'polls.id_apartments')
                    ->join('status', 'status.id', '=', 'polls.id_status')
                    ->join('polls_owners_apartments', function ($join) use($idOwner) {
                        $join->on('polls_owners_apartments.id_polls','=','polls.id')
                            ->where('polls_owners_apartments.id_owners','=',$idOwner);
                    })
                    ->select('polls.*',
                        \DB::raw('concat(owners.fname," ",owners.lname) as description_owners'),
                        'builds.description as description_builds',
                        'apartments.description as description_apartments',
                        'status.description as description_status')
                    ->where('polls.id_owners', '=', $UsersOwner->id_owners)
                    ->orderby('polls.id', 'desc')
                    ->get();
            }
        }
        return $payments;
    }

    /**
     * @param $id_builds
     * @param $id_apartments
     * @param $id_user
     * @return mixed
     */
    public function get_polls_administrators_owners_builds_apartments($id_builds,$id_apartments,$id_user)
    {
        $ApartmentsOwners = ApartmentsOwners::join('users_owners','users_owners.id_owners','=','apartments_owners.id_owners')
            ->join('apartments','apartments_owners.id_apartments','=','apartments.id')
            ->join('builds_apartments','apartments.id','=','builds_apartments.id_apartments')
            ->join('builds','builds_apartments.id_builds','=','builds.id')
            ->where('apartments_owners.id_apartments','=',$id_apartments)
            ->select(
                'users_owners.id_administrators',
                'users_owners.id_owners',
                'apartments.meters','builds.amount_maintenance_meters'
            )
            ->get();

        foreach ($ApartmentsOwners as $ApartmentsOwner) {
            $amount=number_format($ApartmentsOwner->amount_maintenance_meters * $ApartmentsOwner->meters,2);
            $payments = Payments::create([
                'id_user' => $id_user,
                'id_administrator' => $ApartmentsOwner->id_administrators,
                'id_builds' => $id_builds,
                'id_apartments' => $id_apartments,
                'id_status' => 2,
                'id_owners' => $ApartmentsOwner->id_owners,
                'amount'=>$amount,
            ]);
        }
        return $payments;
    }

    /**
     * @param $id
     * @return array
     */
    public function resultados($id)
    {
       $PollsAnwersNO = PollsAnwers::join('status','status.id','=','polls_anwers.id_status')
        ->where('id_polls',$id)
            ->where('id_status',11)
            ->get();

        $PollsAnwersSI = PollsAnwers::join('status','status.id','=','polls_anwers.id_status')
            ->where('id_polls',$id)
            ->where('id_status',10)
            ->get();

        $PollsAnwersAll = PollsAnwers::join('status','status.id','=','polls_anwers.id_status')
            ->where('id_polls',$id)
            ->get();

        if($PollsAnwersNO->count()>0 && $PollsAnwersAll->count()>0) {
            $PollsAnwersAllTNo = ($PollsAnwersNO->count()*100) / $PollsAnwersAll->count();
        }else{
            $PollsAnwersAllTNo = 0;
        }

        if($PollsAnwersSI->count()>0 && $PollsAnwersAll->count()>0) {
            $PollsAnwersAllTSi = ($PollsAnwersSI->count()*100) / $PollsAnwersAll->count();
        }else{
            $PollsAnwersAllTSi = 0;
        }

        $resultado=array('si'=>$PollsAnwersAllTSi."%",'no'=>$PollsAnwersAllTNo."%",'all'=>$PollsAnwersAll->count());

        return $resultado;
    }

}

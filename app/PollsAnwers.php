<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PollsAnwers extends Model
{
    protected $table="polls_anwers";

    protected $fillable = [
        'id_user','id_polls','id_status'
    ];

    public function get_polls_anwers($id_user)
    {
        $PollsAnwers = self::where('polls_anwers.id_user','=',$id_user)
            ->select('polls_anwers.*')
            ->get();

        return $PollsAnwers;
    }
}

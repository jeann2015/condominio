<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PollsOwnersApartments extends Model
{
    protected $table="polls_owners_apartments";

    protected $fillable = [
        'id_polls','id_apartments','id_owners'
    ];
}

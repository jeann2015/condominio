<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservations extends Model
{
    protected $table="reservations";

    protected $fillable = [
        'description','date_from','time_from','date_to','time_to','id_user','id_administrator',
        'id_builds','id_apartments','id_owners'
    ];
}

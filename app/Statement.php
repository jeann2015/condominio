<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Statement extends Model
{
    protected $table="statement";

    protected $fillable = [
        'description','type','id_administrator'
    ];
}

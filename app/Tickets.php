<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tickets extends Model
{
    protected $table="tickets";

    protected $fillable = [
        'description','id_user','id_status','id_administrator','id_builds','id_apartments','id_types','id_owners'
    ];

    public function get_tickets_administrators_owners($id_user)
    {
        $usersadministrators = new UsersAdministrators;
        $id_administrators = $usersadministrators->get_administrator($id_user);

        $tickets=array();

        if ($id_administrators->count() > 0) {

            $UsersOwners = new UsersOwners;
            $iduser = \Auth::id();

            $builds = $UsersOwners->get_builds_owners_administrator($iduser);

            foreach ($id_administrators as $id_administrator) {
                $tickets = Tickets::join('builds','builds.id','=','tickets.id_builds')
                    ->join('apartments','apartments.id','=','tickets.id_apartments')
                    ->join('status','status.id','=','tickets.id_status')
                    ->join('types','types.id','=','tickets.id_types')
                    ->join('owners','owners.id','=','tickets.id_owners')
                    ->select('tickets.*',
                        'apartments.description as description_apartments',
                        'builds.description as description_builds',
                        'types.description as description_types',
                        'status.description as description_status',
                        \DB::raw('concat(owners.fname," ",owners.lname) as description_owners')
                    )
                ->whereIN('builds.id',$builds)
                    ->get();
            }

        }else{
            $tickets = Tickets::join('builds','builds.id','=','tickets.id_builds')
                ->join('apartments','apartments.id','=','tickets.id_apartments')
                ->join('status','status.id','=','tickets.id_status')
                ->join('types','types.id','=','tickets.id_types')
                ->join('owners','owners.id','=','tickets.id_owners')
                ->select('tickets.*',
                    'apartments.description as description_apartments',
                    'builds.description as description_builds',
                    'types.description as description_types',
                    'status.description as description_status',
                    'owners.fname as description_owners'
                )
                ->where('tickets.id_user','=',$id_user)
                ->get();
        }
        return $tickets;
    }

    /**
     * @param $id_user
     * @return mixed
     */
    public function get_administrators_builds_owners($id_user,$id_apartments)
    {
        $usersOwners = UsersOwners::where('id_user','=',$id_user)
            ->get();
        return $usersOwners;
    }



}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketsAnswers extends Model
{
    protected $table="tickets_answers";

    protected $fillable = [
        'answer','id_tickets','id_user'
    ];

}

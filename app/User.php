<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Collection as Collection;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @param $name
     * @param $email
     * @param $grupos
     * @param $id_administrator_owners
     * @return bool|string
     */
    public function news_user_administrators_owners($name,$email,$grupos,$id_administrator_owners)
    {
        $users = new User;
        $audits = new Audits;

        $modules = Modules::all();


        $users->name = $name;
        $users->email = $email;

        $link = substr(hash('md5', uniqid('142857', true)),0,8);

        $users->password = bcrypt($link);
        $users->save();

        $GruposUsers = new GruposUsers;
        $GruposUsers->id_user = $users->id;
        $GruposUsers->id_grupos = $grupos;
        $GruposUsers->save();

        foreach ($modules as $module)
        {
            $access = new Access;
            $gruposmodu = new GruposModules;

            $gruposmodule = $gruposmodu->grupos_modulos($grupos,$module->id);

            $gruposmodules = Collection::make($gruposmodule);
            foreach ($gruposmodules as $gruposmodule)
            {
                $view='view'.$module->id;
                $view_value = $gruposmodule->views;
                if($view_value==""){$view_value="0";}

                $save='save'.$module->id;
                $save_value = $gruposmodule->inserts;
                if($save_value==""){$save_value="0";}

                $modify='modify'.$module->id;
                $modify_value = $gruposmodule->modifys;
                if($modify_value==""){$modify_value="0";}

                $delete='delete'.$module->id;
                $delete_value = $gruposmodule->deletes;
                if($delete_value==""){$delete_value="0";}

                if($view_value<>"" || $save_value<>"" || $modify_value<>"" || $delete_value<>"")
                {
                    $access->id_user=$users->id;
                    $access->id_module=$module->id;
                    $access->views=$view_value;
                    $access->inserts=$save_value;
                    $access->modifys=$modify_value;
                    $access->deletes=$delete_value;
                    $access->status=0;
                    $access->save();
                }
            }
        }

        $audits->save_audits('Add new User:'.$users->id." - ".$name);

        if($grupos==2 || $grupos==4 || $grupos==5)
        {
            $usersadministrators = new UsersAdministrators;
            $usersadministrators->id_user = $users->id;
            $usersadministrators->id_administrator = $id_administrator_owners;
            $usersadministrators->save();
            $audits->save_audits('Add new User Type Administrator:'.$id_administrator_owners." - ".$name);
        }
        if($grupos==3)
        {
            $usersadministrators = new UsersAdministrators;
            $iduser = \Auth::id();
            $id_administrators = $usersadministrators->get_administrator($iduser);
            foreach ($id_administrators as $id_administrator)
            {
                $id_administrator = $id_administrator->id_administrator;
            }
            $usersowners = new UsersOwners;
            $usersowners->id_user = $users->id;
            $usersowners->id_owners = $id_administrator_owners;
            $usersowners->id_administrators = $id_administrator;
            $usersowners->save();
            $audits->save_audits('Add new User Type Owners:'.$id_administrator_owners." - ".$name);
        }



        return  $link;
    }

    /**
     * @param $user_id
     * @return int
     */
    public function get_type_user($user_id)
    {
        $GruposUsers = GruposUsers::where('id_user',$user_id)->where('id_grupos',1)->get();

        if ($GruposUsers->count()>0){
            return 1;
        }
        else{
            return 0;
        }
    }

}

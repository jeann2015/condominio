<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersAdministrators extends Model
{
    protected $table="users_administrators";

    protected $fillable = [
        'id_user','id_administrator'
    ];

    public function get_administrator($id_user)
    {
        $users_administrators = \DB::table('users_administrators')
            ->select(
                'users_administrators.id_administrator'
            )
            ->where('users_administrators.id_user','=',$id_user)
            ->get();

        return  $users_administrators;
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersOwners extends Model
{
    protected $table="users_owners";

    protected $fillable = [
        'id_user','id_owners','id_administrators'
    ];

    public function get_owners($id_user)
    {
        $users_owners = \DB::table('users_owners')
            ->select(
                'users_owners.id_owners'
            )
            ->where('id_user.id_user', '=', $id_user)
            ->get();

        return  $users_owners;
    }

    /**
     * @param $user_id
     * @return mixed
     */
    public function get_builds_owners_administrator($user_id)
    {
        $UsersOwners = self::where('id_user', '=', $user_id)->get();
        $id_administrator="";
        $builds = collect([]);

        if ($UsersOwners->count()>0) {
            $builds = self::join('apartments_owners', 'apartments_owners.id_owners', '=', 'users_owners.id_owners')
            ->join('builds_apartments', 'builds_apartments.id_apartments', '=', 'apartments_owners.id_apartments')
                ->select('builds_apartments.id_builds')
                ->where('id_user', $user_id)
            ->get();
        } else {
            $UsersAdministrators = UsersAdministrators::where('id_user', '=', $user_id)->get();

            if ($UsersAdministrators->count()>0) {
                foreach ($UsersAdministrators as $UsersAdministrator) {
                    $id_administrator = $UsersAdministrator->id_administrator;
                }
                if ($id_administrator <> "") {
                    $builds = BuildsAdministrators::where('id_administrator', $id_administrator)
                        ->select('administrators_builds.id_builds')
                        ->get();
                }
            }
        }

        return $builds;
    }
}

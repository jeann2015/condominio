<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ViewsAdministrators extends Model
{
    protected $table="views_administrators";

    protected $fillable = [
        'value','type','id_administrator'
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ViewsOwners extends Model
{
    protected $table="views_owners";

    protected $fillable = [
        'value','type','id_owners'
    ];

    /**
     * @param $user_id
     * @param $type_news
     * @return int
     */
    public function set_news_owners_administrators($user_id,$type_news)
    {
        $id_administrators_owners =  UsersOwners::where('id_user', '=', $user_id)->get();

        if ($id_administrators_owners->count()>0) {

            foreach ($id_administrators_owners as $id_administrators_owner) {
                if($type_news=='tickets'){
                    $tickets = Tickets::where('id_owners', '=', $id_administrators_owner->id_owners)->get();

                    self::where('type','tickets')
                        ->where('id_owners',$id_administrators_owner->id_owners)
                        ->update(['value' => $tickets->count()]);

                }
                if($type_news=='cuentas'){
                    $payments = Payments::where('id_owners', '=', $id_administrators_owner->id_owners)->get();

                    self::where('type','cuentas')
                        ->where('id_owners',$id_administrators_owner->id_owners)
                        ->update(['value' => $payments->count()]);

                }
                if($type_news=='encuesta'){
                    $polls = PollsOwnersApartments::where('id_owners', '=', $id_administrators_owner->id_owners)->get();

                    self::where('type','encuesta')
                        ->where('id_owners',$id_administrators_owner->id_owners)
                        ->update(['value' => $polls->count()]);

                }
            }

        }else{

            $id_administrators_owners =  UsersAdministrators::where('id_user', '=', $user_id)->get();

            if ($id_administrators_owners->count()>0) {
                foreach ($id_administrators_owners as $id_administrators_owner) {
                    if($type_news=='tickets'){
                        $tickets = Tickets::where('id_administrator', '=', $id_administrators_owner->id_administrator)->get();

                        ViewsAdministrators::where('type','tickets')
                            ->where('id_administrator',$id_administrators_owner->id_administrator)
                            ->update(['value' => $tickets->count()]);

                    }
                    if($type_news=='cuentas'){
                        $payments = Payments::where('id_administrator', '=', $id_administrators_owner->id_administrator)->get();

                        ViewsAdministrators::where('type','cuentas')
                            ->where('id_administrator',$id_administrators_owner->id_administrator)
                            ->update(['value' => $payments->count()]);

                    }
                    if($type_news=='encuesta'){
                        $polls = Polls::where('id_administrator', '=', $id_administrators_owner->id_administrator)->get();

                        ViewsAdministrators::where('type','encuesta')
                            ->where('id_administrator',$id_administrators_owner->id_administrator)
                            ->update(['value' => $polls->count()]);

                    }
                }

            }
        }
        return 1;
    }
    /**
     * @param $user_id
     * @return array
     */
    public function search_news_owners_administrators($user_id)
    {
        $id_administrators_owners =  UsersOwners::where('id_user', '=', $user_id)->get();
        if ($id_administrators_owners->count()>0) {
            $vTickets=0;
            $vPolls=0;
            $vPayments=0;
            foreach ($id_administrators_owners as $id_administrators_owner) {
                $viewOwners = self::where('id_owners', '=', $id_administrators_owner->id_owners)->get();
                $tickets = Tickets::where('id_owners', '=', $id_administrators_owner->id_owners)->get();
                $polls = PollsOwnersApartments::where('id_owners', '=', $id_administrators_owner->id_owners)->get();
                $payments = Payments::where('id_owners', '=', $id_administrators_owner->id_owners)->get();

                $vTickets=0;
                $vPayments=0;
                $vPolls=0;
                if ($viewOwners->count()==0) {
                    self::create([
                        'id_owners' => $id_administrators_owner->id_owners,
                        'type' => 'tickets',
                        'value' => 0
                    ]);
                    self::create([
                        'id_owners' => $id_administrators_owner->id_owners,
                        'type' => 'cuentas',
                        'value' => 0
                    ]);
                    self::create([
                        'id_owners' => $id_administrators_owner->id_owners,
                        'type' => 'encuesta',
                        'value' => 0
                    ]);

                    $tickets = Tickets::where('id_owners', '=', $id_administrators_owner->id_owners)->get();
                    $polls = PollsOwnersApartments::where('id_owners', '=', $id_administrators_owner->id_owners)->get();
                    $payments = Payments::where('id_owners', '=', $id_administrators_owner->id_owners)->get();
                    $viewOwners = self::where('id_owners', '=', $id_administrators_owner->id_owners)->get();

                    foreach ($viewOwners as $viewOwner) {
                        if ($viewOwner->type == 'tickets') {
                            $vTickets = $tickets->count() - $viewOwner->value;
                        }

                        if ($viewOwner->type == 'encuesta') {
                            $vPolls = $polls->count() - $viewOwner->value;
                        }

                        if ($viewOwner->type == 'cuentas') {
                            $vPayments = $payments->count() - $viewOwner->value;
                        }
                    }


                } else {
                    foreach ($viewOwners as $viewOwner) {
                        $tickets = Tickets::where('id_owners', '=', $id_administrators_owner->id_owners)->get();
                        $polls = PollsOwnersApartments::where('id_owners', '=', $id_administrators_owner->id_owners)->get();
                        $payments = Payments::where('id_owners', '=', $id_administrators_owner->id_owners)->get();


                        if ($viewOwner->type=='tickets') {
                            $vTickets = $tickets->count() - $viewOwner->value;
                        }

                        if ($viewOwner->type=='encuesta') {
                            $vPolls = $polls->count() - $viewOwner->value;
                        }

                        if ($viewOwner->type=='cuentas') {
                            $vPayments = $payments->count() - $viewOwner->value;
                        }
                    }
                }
            }
            $values_views[] = array(
                'tickets'=>$vTickets,
                'TicketsTotal'=>$tickets->count(),
                'cuentas'=>$vPayments,
                'CuentasTotal'=>$payments->count(),
                'encuesta'=>$vPolls,
                'PollsTotal'=>$polls->count(),
            );
        } else {
            $id_administrators_owners =  UsersAdministrators::where('id_user', '=', $user_id)->get();
            $vTickets=0;
            $vPolls=0;
            $vPayments=0;
            foreach ($id_administrators_owners as $id_administrators_owner) {
                $viewAdministrators = ViewsAdministrators::where('id_administrator', '=', $id_administrators_owner->id_administrator)->get();

                $tickets = Tickets::where('id_administrator', '=', $id_administrators_owner->id_administrator)->get();
                $polls = Polls::where('id_administrator', '=', $id_administrators_owner->id_administrator)->get();
                $payments = Payments::where('id_administrator', '=', $id_administrators_owner->id_administrator)->get();

                if ($viewAdministrators->count()==0) {

                    ViewsAdministrators::create([
                        'id_administrator' => $id_administrators_owner->id_administrator,
                        'type' => 'tickets',
                        'value' => 0
                    ]);
                    ViewsAdministrators::create([
                        'id_administrator' => $id_administrators_owner->id_administrator,
                        'type' => 'cuentas',
                        'value' => 0
                    ]);
                    ViewsAdministrators::create([
                        'id_administrator' => $id_administrators_owner->id_administrator,
                        'type' => 'encuesta',
                        'value' => 0
                    ]);
                } else {

                    foreach ($viewAdministrators as $viewAdministrator) {

                        $tickets = Tickets::where('id_administrator', '=', $id_administrators_owner->id_administrator)->get();
                        $polls = Polls::where('id_administrator', '=', $id_administrators_owner->id_administrator)->get();
                        $payments = Payments::where('id_administrator', '=', $id_administrators_owner->id_administrator)->get();


                        if ($viewAdministrator->type=='tickets') {
                            $vTickets = $tickets->count() - $viewAdministrator->value;
                        }

                        if ($viewAdministrator->type=='encuesta') {
                            $vPolls = $polls->count() - $viewAdministrator->value;
                        }

                        if ($viewAdministrator->type=='cuentas') {
                            $vPayments = $payments->count() - $viewAdministrator->value;
                        }
                    }
                }
            }

            if(isset($tickets) && isset($payments) && isset($polls) ) {
                $values_views[] = array(
                    'tickets' => $vTickets,
                    'TicketsTotal' => $tickets->count(),
                    'cuentas' => $vPayments,
                    'CuentasTotal' => $payments->count(),
                    'encuesta' => $vPolls,
                    'PollsTotal' => $polls->count(),
                );
            }else{
                $values_views[] = array(
                    'tickets' => $vTickets,
                    'TicketsTotal' => 0,
                    'cuentas' => $vPayments,
                    'CuentasTotal' => 0,
                    'encuesta' => $vPolls,
                    'PollsTotal' => 0,
                );
            }
        }

        return $values_views;
    }
}

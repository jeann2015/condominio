<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableApartments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apartments', function (Blueprint $table)
        {
            $table->increments('id');
            $table->text('description');
            $table->string('identifications',35);
            $table->string('meters',35);
            $table->string('floor',35);
            $table->string('parking',35);
            $table->string('notes',255);
            $table->integer('id_user')->unsigned();

            $table->foreign('id_user')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apartments');
    }
}

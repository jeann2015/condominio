<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOwners extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('owners', function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('fname',35);
            $table->string('lname',35);
            $table->string('email',35);
            $table->string('phonenumber',60);
            $table->integer('id_user')->unsigned();
            $table->integer('status');
            $table->foreign('id_user')->references('id')->on('users');
            $table->timestamps();


        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('owners');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableApartmentsOwners extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apartments_owners', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('id_apartments')->unsigned();
            $table->integer('id_owners')->unsigned();
            $table->foreign('id_apartments')->references('id')->on('apartments');
            $table->foreign('id_owners')->references('id')->on('owners');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apartments_owners');
    }
}

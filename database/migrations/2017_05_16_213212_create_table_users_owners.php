<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUsersOwners extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_owners', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('id_user')->unsigned();
            $table->integer('id_owners')->unsigned();
            $table->integer('id_administrators')->unsigned();
            $table->foreign('id_user')->references('id')->on('users');
            $table->foreign('id_owners')->references('id')->on('owners');
            $table->foreign('id_administrators')->references('id')->on('administrators');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_owners');
    }
}

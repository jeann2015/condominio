<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBuildsApartments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('builds_apartments', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('id_builds')->unsigned();
            $table->integer('id_apartments')->unsigned();
            $table->timestamps();

            $table->foreign('id_builds')->references('id')->on('builds');
            $table->foreign('id_apartments')->references('id')->on('apartments');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('builds_apartments');
    }
}

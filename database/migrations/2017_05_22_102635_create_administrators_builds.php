<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdministratorsBuilds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('administrators_builds', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('id_builds')->unsigned();
            $table->integer('id_administrator')->unsigned();
            $table->timestamps();

            $table->foreign('id_builds')->references('id')->on('builds');
            $table->foreign('id_administrator')->references('id')->on('administrators');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('administrators_builds');
    }
}

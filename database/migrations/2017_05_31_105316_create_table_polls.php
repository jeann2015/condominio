<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePolls extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('polls', function (Blueprint $table)
        {
            $table->increments('id');
            $table->text('description');
            $table->integer('id_user')->unsigned();
            $table->integer('id_administrator')->unsigned();
            $table->integer('id_builds')->unsigned();
            $table->integer('id_status')->unsigned();
            $table->date('from');
            $table->date('to');
            $table->timestamps();
            $table->foreign('id_user')->references('id')->on('users');
            $table->foreign('id_administrator')->references('id')->on('administrators');
            $table->foreign('id_builds')->references('id')->on('builds');
            $table->foreign('id_status')->references('id')->on('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('polls');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description',255);
            $table->integer('id_user')->unsigned();
            $table->integer('id_administrator')->unsigned();
            $table->integer('id_builds')->unsigned();
            $table->integer('id_apartments')->unsigned();
            $table->integer('id_status')->unsigned();
            $table->integer('id_owners')->unsigned();
            $table->decimal('amount',12,2);
            $table->enum('type', ['manual', 'automatic']);
            $table->timestamps();

            $table->foreign('id_user')->references('id')->on('users');
            $table->foreign('id_administrator')->references('id')->on('administrators');
            $table->foreign('id_builds')->references('id')->on('builds');
            $table->foreign('id_apartments')->references('id')->on('apartments');
            $table->foreign('id_status')->references('id')->on('status');
            $table->foreign('id_owners')->references('id')->on('owners');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}

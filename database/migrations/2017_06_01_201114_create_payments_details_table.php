<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments_details', function (Blueprint $table) {
            $table->increments('id');
            $table->text('description');
            $table->integer('id_payments')->unsigned();
            $table->integer('id_user')->unsigned();
            $table->string('bank',60);
            $table->string('number_transaction',100);
            $table->timestamps();

            $table->foreign('id_payments')->references('id')->on('payments');
            $table->foreign('id_user')->references('id')->on('users');

        });

        \DB::statement("ALTER TABLE payments_details ADD COLUMN `images`  LONGBLOB AFTER `number_transaction`");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments_details');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableReservations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table)
        {
            $table->increments('id');
            $table->text('description');
            $table->date('date_from');
            $table->time('time_from');
            $table->date('date_to');
            $table->time('time_to');
            $table->integer('id_user')->unsigned();
            $table->integer('id_administrator')->unsigned();
            $table->integer('id_builds')->unsigned();
            $table->integer('id_apartments')->unsigned();
            $table->integer('id_owners')->unsigned();
            $table->timestamps();
            $table->foreign('id_user')->references('id')->on('users');
            $table->foreign('id_administrator')->references('id')->on('administrators');
            $table->foreign('id_builds')->references('id')->on('builds');
            $table->foreign('id_apartments')->references('id')->on('apartments');
            $table->foreign('id_owners')->references('id')->on('owners');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewsAdministratorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('views_administrators', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_administrator')->unsigned();
            $table->enum('type', ['tickets', 'cuentas','encuesta']);
            $table->integer('value');
            $table->timestamps();

            $table->foreign('id_administrator')->references('id')->on('administrators');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('views_administrators');
    }
}

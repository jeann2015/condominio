<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewsOwnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('views_owners', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_owners')->unsigned();
            $table->enum('type', ['tickets', 'cuentas','encuesta']);
            $table->integer('value');
            $table->timestamps();

            $table->foreign('id_owners')->references('id')->on('owners');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('views_owners');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expense', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user')->unsigned();
            $table->integer('id_administrator')->unsigned();
            $table->integer('id_builds')->unsigned();
            $table->integer('id_statment')->unsigned();
            $table->decimal('amount',12,2);
            $table->timestamps();

            $table->foreign('id_user')->references('id')->on('users');
            $table->foreign('id_administrator')->references('id')->on('administrators');
            $table->foreign('id_builds')->references('id')->on('builds');
            $table->foreign('id_statment')->references('id')->on('statement');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expense');
    }
}

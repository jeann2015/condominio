<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePollsOwnersApartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('polls_owners_apartments', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('id_polls')->unsigned();
            $table->integer('id_apartments')->unsigned();
            $table->integer('id_owners')->unsigned();
            $table->timestamps();

            $table->foreign('id_polls')->references('id')->on('polls');
            $table->foreign('id_apartments')->references('id')->on('apartments');
            $table->foreign('id_owners')->references('id')->on('owners');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('polls_owners_apartments');
    }
}

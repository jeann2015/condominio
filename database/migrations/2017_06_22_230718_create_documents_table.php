<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description', 255);
            $table->integer('id_builds')->unsigned();
            $table->string('type_file', 20);
            $table->integer('size');
            $table->timestamps();
            $table->foreign('id_builds')->references('id')->on('builds');
        });
        \DB::statement("ALTER TABLE documents ADD COLUMN `files_general`  LONGBLOB AFTER `type_file`");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}

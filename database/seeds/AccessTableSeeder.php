<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class AccessTableSeeder extends Seeder
{
   /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        for ($con = 1; $con <= 24; $con++) {
            if($con==5 || $con==8 || $con==9 || $con==10 || $con==11 || $con==12 || $con==19 || $con==20 || $con==21 || $con==22 || $con==23 || $con==24) {
                DB::table('access')->insert([
                    'id_user' => '1',
                    'id_module' => $con,
                    'views' => '0',
                    'inserts' => '0',
                    'modifys' => '0',
                    'deletes' => '0',
                    'status' => '1',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);
            }else{
                DB::table('access')->insert([
                    'id_user' => '1',
                    'id_module' => $con,
                    'views' => '1',
                    'inserts' => '1',
                    'modifys' => '1',
                    'deletes' => '1',
                    'status' => '1',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);

            }
        }

    }
}

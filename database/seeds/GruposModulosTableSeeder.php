<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class GruposModulosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($con = 1; $con <= 24; $con++) {


            DB::table('grupos_modules')->insert([
                'id_modules' => $con,
                'id_grupos' => '1',
                'views' => '1',
                'inserts' => '1',
                'modifys' => '1',
                'deletes' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);

            if ($con==1 || $con==5 || $con==6 || $con==8 || $con==9 || $con==10 || $con==11 || $con==12 || $con==13 || $con==15 || $con==16 || $con==18 || $con==19 || $con==20 || $con==21 || $con==22 || $con==23 || $con==24) {
                DB::table('grupos_modules')->insert([
                    'id_modules' => $con,
                    'id_grupos' => '2',
                    'views' => '1',
                    'inserts' => '1',
                    'modifys' => '1',
                    'deletes' => '1',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);


            } else {
                DB::table('grupos_modules')->insert([
                    'id_modules' => $con,
                    'id_grupos' => '2',
                    'views' => '0',
                    'inserts' => '0',
                    'modifys' => '0',
                    'deletes' => '0',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);


            }

            if ($con==1 || $con==5 || $con==6 || $con==8 || $con==9 || $con==10 || $con==11 || $con==12  || $con==15 || $con==16 || $con==18 || $con==19 || $con==20 || $con==21 || $con==22 || $con==23 || $con==24) {


                DB::table('grupos_modules')->insert([
                    'id_modules' => $con,
                    'id_grupos' => '4',
                    'views' => '1',
                    'inserts' => '1',
                    'modifys' => '1',
                    'deletes' => '1',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);
            } else {


                DB::table('grupos_modules')->insert([
                    'id_modules' => $con,
                    'id_grupos' => '4',
                    'views' => '0',
                    'inserts' => '0',
                    'modifys' => '0',
                    'deletes' => '0',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);
            }

            if ($con==1 || $con==5 || $con==6 || $con==11 || $con==12 || $con==15 || $con==16 || $con==18 || $con==19 || $con==20 || $con==24) {
                DB::table('grupos_modules')->insert([
                    'id_modules' => $con,
                    'id_grupos' => '3',
                    'views' => '1',
                    'inserts' => '1',
                    'modifys' => '1',
                    'deletes' => '1',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);

            } else {
                DB::table('grupos_modules')->insert([
                    'id_modules' => $con,
                    'id_grupos' => '3',
                    'views' => '0',
                    'inserts' => '0',
                    'modifys' => '0',
                    'deletes' => '0',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);

            }

            if ($con==1 || $con==11 || $con==15 || $con==16 || $con==18) {
                DB::table('grupos_modules')->insert([
                    'id_modules' => $con,
                    'id_grupos' => '5',
                    'views' => '1',
                    'inserts' => '1',
                    'modifys' => '1',
                    'deletes' => '1',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);

            } else {
                DB::table('grupos_modules')->insert([
                    'id_modules' => $con,
                    'id_grupos' => '5',
                    'views' => '0',
                    'inserts' => '0',
                    'modifys' => '0',
                    'deletes' => '0',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);

            }
        }
    }
}

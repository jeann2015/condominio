<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class GruposTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('grupos')->insert([
          'description' => 'Administradores Sistema',
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now()
      ]);

        DB::table('grupos')->insert([
            'description' => 'Clientes Administradores',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('grupos')->insert([
            'description' => 'Propietarios',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('grupos')->insert([
            'description' => 'Sub-Administrador',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('grupos')->insert([
            'description' => 'Jefe Operaciones',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }
}

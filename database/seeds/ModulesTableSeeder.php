<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ModulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('modules')->insert([
            'description' => 'Principal',
            'order' => '1',
            'id_father' =>'0',
            'url' =>'#',
            'messages' => 'Principal',
            'status' =>'1',
            'visible' =>'1',
            'clase' =>'pages',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('modules')->insert([
            'description' => 'Users',
            'order' => '2',
            'id_father' =>'1',
            'url' =>'users',
            'messages' => 'Users',
            'status' =>'1',
            'visible' =>'1',
            'clase' =>'pages',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('modules')->insert([
            'description' => 'Audit',
            'order' => '3',
            'id_father' =>'1',
            'url' =>'audit',
            'messages' => 'Audit',
            'status' =>'1',
            'visible' =>'1',
            'clase' =>'pages',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('modules')->insert([
            'description' => 'Modules',
            'order' => '4',
            'id_father' =>'1',
            'url' =>'modules',
            'messages' => 'Modules',
            'status' =>'1',
            'visible' =>'1',
            'clase' =>'pages',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('modules')->insert([
            'description' => 'Reports',
            'order' => '1',
            'id_father' =>'0',
            'url' =>'#',
            'messages' => 'Reports',
            'status' =>'1',
            'visible' =>'1',
            'clase' =>'pages',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('modules')->insert([
            'description' => 'Estado de Cuenta',
            'order' => '1',
            'id_father' =>'5',
            'url' =>'reportsaccount',
            'messages' => 'Estado de Cuenta',
            'status' =>'1',
            'visible' =>'1',
            'clase' =>'pages',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        
         DB::table('modules')->insert([
            'description' => 'Grupos',
            'order' => '7',
            'id_father' =>'1',
            'url' =>'grupos',
            'messages' => 'Grupos',
            'status' =>'1',
            'visible' =>'1',
            'clase' =>'pages',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('modules')->insert([
            'description' => 'Edificios/Residencias',
            'order' => '8',
            'id_father' =>'1',
            'url' =>'builds',
            'messages' => 'Edificios/Residencias',
            'status' =>'1',
            'visible' =>'1',
            'clase' =>'pages',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('modules')->insert([
            'description' => 'Apartamentos/Casas',
            'order' => '9',
            'id_father' =>'1',
            'url' =>'apartments',
            'messages' => 'Apartamentos/Casas',
            'status' =>'1',
            'visible' =>'1',
            'clase' =>'pages',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('modules')->insert([
            'description' => 'Propietarios',
            'order' => '10',
            'id_father' =>'1',
            'url' =>'owners',
            'messages' => 'Propietarios',
            'status' =>'1',
            'visible' =>'1',
            'clase' =>'pages',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('modules')->insert([
            'description' => 'Tickets',
            'order' => '11',
            'id_father' =>'1',
            'url' =>'tickets',
            'messages' => 'Tickets',
            'status' =>'1',
            'visible' =>'1',
            'clase' =>'pages',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('modules')->insert([
            'description' => 'Reservacion',
            'order' => '12',
            'id_father' =>'1',
            'url' =>'reservations',
            'messages' => 'Reservacion',
            'status' =>'1',
            'visible' =>'1',
            'clase' =>'pages',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('modules')->insert([
            'description' => 'Administradores',
            'order' => '13',
            'id_father' =>'1',
            'url' =>'administrators',
            'messages' => 'Administradores',
            'status' =>'1',
            'visible' =>'1',
            'clase' =>'pages',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('modules')->insert([
            'description' => 'Estados',
            'order' => '14',
            'id_father' =>'1',
            'url' =>'status',
            'messages' => 'Estados',
            'status' =>'1',
            'visible' =>'1',
            'clase' =>'pages',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('modules')->insert([
            'description' => 'Salir',
            'order' => '1',
            'id_father' =>'0',
            'url' =>'#',
            'messages' => 'Salir',
            'status' =>'1',
            'visible' =>'1',
            'clase' =>'pages',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('modules')->insert([
            'description' => 'Salir de la Aplicacion',
            'order' => '1',
            'id_father' =>'15',
            'url' =>'logout',
            'messages' => 'Salir de la Aplicacion',
            'status' =>'1',
            'visible' =>'1',
            'clase' =>'pages',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('modules')->insert([
            'description' => 'Tipos de Tickets',
            'order' => '17',
            'id_father' =>'1',
            'url' =>'types',
            'messages' => 'Tipos de Tickets',
            'status' =>'1',
            'visible' =>'1',
            'clase' =>'pages',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('modules')->insert([
            'description' => 'Cambio de Clave',
            'order' => '18',
            'id_father' =>'1',
            'url' =>'changes',
            'messages' => 'Cambio de Clave',
            'status' =>'1',
            'visible' =>'1',
            'clase' =>'pages',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('modules')->insert([
            'description' => 'Encuesta',
            'order' => '19',
            'id_father' =>'1',
            'url' =>'polls',
            'messages' => 'Encuestas',
            'status' =>'1',
            'visible' =>'1',
            'clase' =>'pages',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('modules')->insert([
            'description' => 'Estado de Cuenta',
            'order' => '20',
            'id_father' =>'1',
            'url' =>'payments',
            'messages' => 'Estados de Cuenta',
            'status' =>'1',
            'visible' =>'1',
            'clase' =>'pages',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('modules')->insert([
            'description' => 'Items de Estados Financiero',
            'order' => '21',
            'id_father' =>'1',
            'url' =>'statements',
            'messages' => 'Items de Estados Financiero',
            'status' =>'1',
            'visible' =>'1',
            'clase' =>'pages',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('modules')->insert([
            'description' => 'Estados Financiero',
            'order' => '22',
            'id_father' =>'1',
            'url' =>'expenses',
            'messages' => 'Estados Financiero',
            'status' =>'1',
            'visible' =>'1',
            'clase' =>'pages',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('modules')->insert([
            'description' => 'Estado Financiero',
            'order' => '2',
            'id_father' =>'5',
            'url' =>'reportsstates',
            'messages' => 'Estado Financiero',
            'status' =>'1',
            'visible' =>'1',
            'clase' =>'pages',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('modules')->insert([
            'description' => 'Documentos',
            'order' => '23',
            'id_father' =>'1',
            'url' =>'documents',
            'messages' => 'Documentos',
            'status' =>'1',
            'visible' =>'1',
            'clase' =>'pages',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

    }
}

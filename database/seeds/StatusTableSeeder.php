<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pagos_list = array(
            array(
                'description'=>"Pago Realizado",
                'type'=>'1'
            ),
            array(
                'description'=>"Pago Pendiente",
                'type'=>'1'
            ),
            array(
                'description'=>"Recibo de Pago Recibido",
                'type'=>'1'
            )
        );

        foreach ($pagos_list as $pagos)
        {
            DB::table('status')->insert([
                'description' => $pagos['description'],
                'type' => $pagos['type'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }

        $tickets_list = array(
            array(
                'description'=>"Ticket Abierto",
                'type'=>'2'
            ),
            array(
                'description'=>"Ticket Pendiente",
                'type'=>'2'
            ),
            array(
                'description'=>"Ticket Cerrado",
                'type'=>'2'
            )
        );

        foreach ($tickets_list as $tickets)
        {
            DB::table('status')->insert([
                'description' => $tickets['description'],
                'type' => $tickets['type'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }

        $tickets_tipo = array(
            array(
                'description'=>"Reclamo",
                'type'=>'3'
            ),
            array(
                'description'=>"Solicitud",
                'type'=>'3'
            ),
            array(
                'description'=>"LLamado de Atencion",
                'type'=>'3'
            )
        );

        foreach ($tickets_tipo as $tickets)
        {
            DB::table('types')->insert([
                'description' => $tickets['description'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }

        $encuesta_tipo = array(
            array(
                'description'=>"Abierta",
                'type'=>'3'
            ),
            array(
                'description'=>"Cerrada",
                'type'=>'3'
            ),
             array(
                 'description'=>"Suspendida",
                 'type'=>'3'
             )
        );

        foreach ($encuesta_tipo as $tickets)
        {
            DB::table('status')->insert([
                'description' => $tickets['description'],
                'type' => $tickets['type'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }

        $encuesta_answer = array(
                array(
                    'description'=>"Aprobado",
                    'type'=>'4'
                ),
                array(
                    'description'=>"No Aprobado",
                    'type'=>'4'
                )
            );

            foreach ($encuesta_answer as $encuesta)
            {
                DB::table('status')->insert([
                    'description' => $encuesta['description'],
                    'type' => $encuesta['type'],
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);
            }


    }
}

@extends('layouts.header')
@section('content')
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-header">
                        <h4>Administradores</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if(isset($notifys))

                            <div class="alert alert-danger" role="alert">
                                <button class="close" aria-label="Close" data-dismiss="alert" type="button">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <strong>No hay Edificios/Residencia</strong>
                            </div>

                        @endif
                        <div class="card-header">
                            <div class="toolbar">
                                @foreach ($user_access as $user_acces)
                                    @if($user_acces->inserts == 1)
                                        <a href="{{ url('administrators/add') }}" class="btn btn-default" role="button">Add </a>
                                    @else
                                        <a href="#" class="btn btn-default" role="button">No Add  </a>
                                    @endif
                                @endforeach

                                <a href="{{ url('home') }}" class="btn btn-default" role="button">Back </a>
                            </div>
                        </div>
                        <div class="card-block">
                            <table id="General" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Nombre</th>
                                    <th>Correo</th>
                                    <th>Fecha/Hora</th>
                                    <th>Tipo Administrador</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($administrators as $administrador)
                                    <tr>
                                        <td>{{ $administrador->id }}</td>
                                        <td>{{ $administrador->fname." ".$administrador->lname }}</td>
                                        <td>{{ $administrador->email }}</td>
                                        <td>{{ Carbon\Carbon::parse($administrador->created_at)->format('l jS \\of F Y h:i:s A') }}</td>
                                        <td>{{ $administrador->description_grupos }}</td>
                                        <td>
                                            <div class="tabledit-toolbar btn-toolbar">
                                                <div class="btn-group btn-group-sm">
                                                    @foreach ($user_access as $user_acces)
                                                        @if($user_acces->modifys == 1)
                                                            <a href="administrators/edit/{{ $administrador->id }}" class="tabledit-edit-button btn btn-primary waves-effect waves-light" role="button">
                                                                <span class="icofont icofont-ui-edit"></span>
                                                            </a>
                                                        @else
                                                            <a href="#" class="btn btn-default" role="button">No Modify</a>
                                                        @endif
                                                        @if($user_acces->deletes==1)
                                                            <a href="administrators/delete/{{ $administrador->id }}" class="tabledit-delete-button btn btn-danger waves-effect waves-light" role="button">
                                                                <span class="icofont icofont-ui-delete"></span>
                                                            </a>
                                                        @else
                                                            <a href="#" class="btn btn-default" role="button">No Delete</a>
                                                        @endif
                                                    @endforeach
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
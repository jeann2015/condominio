@extends('layouts.header')

@section('content')
<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="main-header">
					<!-- Tutulo del Formulario -->
					<h4>Modificar Administrador</h4>
				</div>
			</div>
		</div>	
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
					<div class="card-header">
						<a href="{{ url('administrators') }}" class="btn btn-default" role="button">Back </a>
					</div>
					<div class="card-block">
						<!-- Formulario -->
						{!! Form::open(array('url' => 'administrators/update','enctype'=>'multipart/form-data')) !!}
						<div class="form-group">
							<label>Nombre</label>
							{!! Form::text('fname',$administrators->fname,array('class' => 'form-control','id'=>'fname','required')) !!}
						</div>
						<div class="form-group">
							<label>Apellido</label>
							{!! Form::text('lname',$administrators->lname,array('class' => 'form-control','id'=>'lname','required')) !!}
						</div>
						<div class="form-group">
							<label>Correo</label>
							{!! Form::email('email',$administrators->email,array('class' => 'form-control','id'=>'email','required')) !!}
						</div>
						<div class="form-group">
							<label>Imagen:</label>
							<br>
                            <img src="{{ url(''.$imagen[0] ) }}" height="150" width="150">
							{!! Form::file('images', ['id'=>'images','class'=>'form-control','submit']) !!}
						</div>
						{!! Form::submit('Update!',array('class' => 'btn btn-primary','id'=>'update')); !!}
						{!! Form::hidden('id',$administrators->id,array('id'=>'id')) !!}
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
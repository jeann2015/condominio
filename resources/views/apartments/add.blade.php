@extends('layouts.header')

@section('content')

	<div class="content-wrapper">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<div class="main-header">
						<!-- Tutulo del Formulario -->
						<h4>Nuevo Apartamento/Casa</h4>
					</div>
				</div>
			</div>
			@if (count($errors) > 0)
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
			<div class="row">
				<div class="col-lg-12">
					<div class="card">
						<div class="card-header">
							<a href="{{ url('apartments') }}" class="btn btn-default" role="button">Back </a>
						</div>
						<div class="card-block">
							<!-- Formulario -->
							{!! Form::open(array('url' => 'apartments/new')) !!}
							<div class="form-group">
								<label>Edificios/Redidencias</label>
								{!! Form::select('id_builds', $builds, null, ['id'=>'id_builds','class'=>'form-control','required']) !!}
							</div>
							<div class="form-group">
								<label>Identificacion</label>
								{!! Form::text('identification','',array('class' => 'form-control','id'=>'identification','required')) !!}
							</div>
							<div class="form-group">
								<label>Descripcion</label>
								{!! Form::text('description','',array('class' => 'form-control','id'=>'description','required')) !!}
							</div>
							<div class="form-group">
								<label>Metraje</label>
								{!! Form::number('meters','',array('class' => 'form-control','id'=>'meters','required','step'=>'any')) !!}
							</div>
							<div class="form-group">
								<label>Piso</label>
								{!! Form::text('floor','',array('class' => 'form-control','id'=>'floor','required')) !!}
							</div>
							<div class="form-group">
								<label>Estacionamiento</label>
								{!! Form::text('parking','',array('class' => 'form-control','id'=>'parking','required')) !!}
							</div>
							<div class="form-group">
								<label>Notas</label>
								{!! Form::textarea('notes','',array('class' => 'form-control','id'=>'notes')) !!}
							</div>
							{!! Form::submit('Save!',array('class' => 'btn btn-primary','id'=>'save')); !!}
							{!! Form::close() !!}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection
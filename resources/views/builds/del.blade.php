@extends('layouts.header')

@section('content')

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="main-header">
					<!-- Tutulo del Formulario -->
					<h4>Eliminar Edificio/Residencia</h4>
				</div>
			</div>
		</div>	
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
					<div class="card-header">
						<a href="{{ url('builds') }}" class="btn btn-default" role="button">Back </a>
					</div>
					<div class="card-block">
						<!-- Formulario -->
						{!! Form::open(array('url' => 'builds/destroy')) !!}
						<div class="form-group">
							<label>Descripción</label>
							{!! Form::text('description',$builds->description,array('class' => 'form-control','id'=>'description','required')) !!}
						</div>

						<div class="form-group">
							<label>Identificacion</label>
							{!! Form::text('identifications',$builds->identifications,array('class' => 'form-control','id'=>'identifications','required')) !!}
						</div>
						<div class="form-group">
							<label>Dirección</label>
							{!! Form::textarea('address',$builds->address,array('class' => 'form-control','id'=>'address','required')) !!}
						</div>
						<div class="form-group">
							<label>Monto Costo por Metro Cuadrado</label>
							{!! Form::number('amount_maintenance_meters',$builds->amount_maintenance_meters,array('class' => 'form-control','id'=>'amount_maintenance_meters','required','step'=>"any")) !!}
						</div>

						{!! Form::submit('Delete!',array('class' => 'btn btn-primary','id'=>'delete')) !!}
						{!! Form::hidden('id',$builds->id,array('id'=>'id')) !!}
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
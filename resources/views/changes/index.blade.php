@extends('layouts.header')
@section('content')

    <!-- @if(isset($notifys))

        <div class="alert alert-success" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
          <span aria-hidden="true">×</span>
          </button>
@foreach ($notifys as $noti => $valor)
            @if($valor == 1)
                <strong>Well done!</strong> You successfully insert your Data.
@endif
            @if($valor == 2)
                <strong>Well done!</strong> You successfully modified your Data.
@endif
            @if($valor == 3)
                <strong>Well done!</strong> You successfully removed your Data,
@endif
        @endforeach

                </div>

@endif -->

    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-header">
                        <h4>Cambio de Clave</h4>
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-lg-12">

                    <div class="card">
                        <div class="card-header">
                            <div class="toolbar">
                                <a href="{{ url('home') }}" class="btn btn-default" role="button">Back </a>
                            </div>
                        </div>

                        <div class="container" role="alert">
                            @include('flash::message')
                        </div>
                        <div class="card-header">

                                <div class="toolbar">
                                    {!! Form::open(array('url' => 'changes/update')) !!}
                                    <div class="form-group">
                                        <label>Clave Nueva:</label>
                                        <input id="password" type="password" class="md-form-control" name="password" required>
                                    </div>
                                    {!! Form::submit('Cambiar!',array('class' => 'btn btn-primary','id'=>'cambiar')) !!}
                                    {!! Form::close() !!}
                                </div>

                        </div>
                    </div>
                </div>

        </div>
    </div>

@endsection
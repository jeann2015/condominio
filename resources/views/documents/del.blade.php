@extends('layouts.header')

@section('content')

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="main-header">
					<h4>Eliminar Documento</h4>
				</div>
			</div>
		</div>	
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

					<div class="card-header">
						<a href="{{ url('documents') }}" class="btn btn-default" role="button">Back </a>
					</div>
					<div class="card-block">
                        {!! Form::open(array('url' => 'documents/destroy','enctype'=>'multipart/form-data')) !!}
						<div class="form-group">
							<label>Edificios/Residencia</label>
							{!! Form::select('id_builds', $builds, $documents->id_builds, ['id'=>'id_builds','class'=>'form-control','required']) !!}
						</div>
						<div class="form-group">
							<label>Descripción</label>
							{!! Form::text('description',$documents->description,array('class' => 'form-control','id'=>'description','required')) !!}
						</div>
						<div class="form-group">
							<label>Archivo</label>
							{!! Form::file('images', ['id'=>'images','class'=>'form-control','submit']) !!}
						</div>


						{!! Form::submit('Delete!',array('class' => 'btn btn-primary','id'=>'delete')) !!}
						{!! Form::hidden('id',$documents->id,array('id'=>'id')) !!}
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
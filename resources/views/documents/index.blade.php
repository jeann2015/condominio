@extends('layouts.header')
@section('content')

    <!-- @if(isset($notifys))

        <div class="alert alert-success" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
          <span aria-hidden="true">×</span>
          </button>
@foreach ($notifys as $noti => $valor)
            @if($valor == 1)
                <strong>Well done!</strong> You successfully insert your Data.
@endif
            @if($valor == 2)
                <strong>Well done!</strong> You successfully modified your Data.
@endif
            @if($valor == 3)
                <strong>Well done!</strong> You successfully removed your Data,
@endif
        @endforeach

                </div>

@endif -->

    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-header">
                        <h4>Documentos</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="toolbar">
                                @foreach ($user_access as $user_acces)
                                    @if($user_acces->inserts == 1)
                                        <a href="{{ url('documents/add') }}" class="btn btn-default" role="button">Add </a>
                                    @else
                                        <a href="#" class="btn btn-default" role="button">No Add  </a>
                                    @endif
                                @endforeach

                                <a href="{{ url('home') }}" class="btn btn-default" role="button">Back </a>
                            </div>
                        </div>
                        <div class="card-block">

                            <table id="General" class="table table-striped table-bordered">

                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Edificio/Residencia</th>
                                    <th>Descripcion</th>
                                    <th>Fecha/Hora</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($documents as $document)
                                    <tr>
                                        <td>{{ $document->id }}</td>
                                        <td>{{ $document->description_builds }}</td>
                                        <td>{{ $document->description }}</td>
                                        <td>{{ Carbon\Carbon::parse($document->updated_at)->format('l jS \\of F Y h:i:s A') }}</td>
                                        <td>
                                            <div class="tabledit-toolbar btn-toolbar">
                                                <div class="btn-group btn-group-sm">
                                                    @foreach ($user_access as $user_acces)
                                                        @if($user_acces->modifys == 1)

                                                            <a title="Bajar Documento" href="documents/download/{{ $document->id }}" class="tabledit-edit-button btn btn-success waves-effect waves-light" role="button">
                                                                <span class="fa fa-cloud-download"></span>
                                                            </a>
                                                            @if($idAdministrator<>"")
                                                                <a href="documents/edit/{{ $document->id }}" class="tabledit-edit-button btn btn-primary waves-effect waves-light" role="button">
                                                                    <span class="icofont icofont-ui-edit"></span>
                                                                </a>
                                                            @endif
                                                        @else
                                                            <a href="#" class="btn btn-default" role="button">No Modify</a>
                                                        @endif
                                                        @if($user_acces->deletes==1)
                                                            @if($idAdministrator<>"")
                                                                <a href="documents/delete/{{ $document->id }}" class="tabledit-delete-button btn btn-danger waves-effect waves-light" role="button">
                                                                    <span class="icofont icofont-ui-delete"></span>
                                                                </a>
                                                            @endif
                                                        @else
                                                            <a href="#" class="btn btn-default" role="button">No Delete</a>
                                                        @endif
                                                    @endforeach
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
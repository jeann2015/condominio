@extends('layouts.header')

@section('content')

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="main-header">
					<!-- Tutulo del Formulario -->
					<h4>Nuevo Gasto</h4>
				</div>
			</div>
		</div>	
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
					<div class="card-header">
						<a href="{{ url('expenses') }}" class="btn btn-default" role="button">Back </a>
					</div>
					<div class="card-block">
						<!-- Formulario -->
						{!! Form::open(array('url' => 'expenses/new')) !!}
						<div class="form-group">
							<label>Edificio/Residencia</label>
							{!! Form::select('id_builds', $builds, null, ['id'=>'id_builds','class'=>'form-control','required']) !!}
						</div>
						<div class="form-group">
							<label>Gasto/Ingreso</label>
							{!! Form::select('id_statment', $statements, null, ['id'=>'id_statment','class'=>'form-control','required']) !!}
						</div>
						<div class="form-group">
							<label>Monto</label>
							{!! Form::number('amount','',array('class' => 'form-control','id'=>'amount','required','step'=>"any")) !!}
						</div>
						{!! Form::submit('Save!',array('class' => 'btn btn-primary','id'=>'save')); !!}
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
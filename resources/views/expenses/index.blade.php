@extends('layouts.header')
@section('content')

    <!-- @if(isset($notifys))

        <div class="alert alert-success" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
          <span aria-hidden="true">×</span>
          </button>
@foreach ($notifys as $noti => $valor)
            @if($valor == 1)
                <strong>Well done!</strong> You successfully insert your Data.
@endif
            @if($valor == 2)
                <strong>Well done!</strong> You successfully modified your Data.
@endif
            @if($valor == 3)
                <strong>Well done!</strong> You successfully removed your Data,
@endif
        @endforeach

                </div>

@endif -->

    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-header">
                        <h4>Informe de Gatos</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="card-header">
                            <div class="toolbar">
                                @foreach ($user_access as $user_acces)
                                    @if($user_acces->inserts == 1)
                                        <a href="{{ url('expenses/add') }}" class="btn btn-default" role="button">Add </a>
                                    @else
                                        <a href="#" class="btn btn-default" role="button">No Add  </a>
                                    @endif
                                @endforeach

                                <a href="{{ url('home') }}" class="btn btn-default" role="button">Back </a>
                            </div>
                        </div>
                        <div class="card-block">
                            <table id="General" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Edificio/Residencia</th>
                                    <th>Monto</th>
                                    <th>Tipo</th>
                                    <th>Fecha/Hora</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($expenses as $expense)
                                    <tr>
                                        <td>{{ $expense->id }}</td>
                                        <td>{{ $expense->description_builds }}</td>
                                        <td>{{ $expense->amount }}</td>
                                        @if($expense->type=='ingreso')
                                            <td><span class="label label-success">{{ $expense->type }}</span></td>
                                        @elseif($expense->type=='gasto')
                                            <td><span class="label label-danger">{{ $expense->type }}</span></td>
                                        @endif
                                        <td>{{ Carbon\Carbon::parse($expense->created_at)->format('l jS \\of F Y h:i:s A') }}</td>
                                        <td>
                                            <div class="tabledit-toolbar btn-toolbar">
                                                <div class="btn-group btn-group-sm">
                                                    <a title="Ver PDF" href="expenses/pdf/{{ $expense->id }}" class="tabledit-edit-button btn btn-info waves-effect waves-light" role="button">
                                                        <span class="fa fa-file-pdf-o"></span>
                                                    </a>
                                                    @foreach ($user_access as $user_acces)
                                                        @if($user_acces->modifys == 1)
                                                            <a href="expenses/edit/{{ $expense->id }}" class="tabledit-edit-button btn btn-primary waves-effect waves-light" role="button">
                                                                <span class="icofont icofont-ui-edit"></span>
                                                            </a>
                                                        @else
                                                            <a href="#" class="btn btn-default" role="button">No Modify</a>
                                                        @endif
                                                        @if($user_acces->deletes==1)
                                                            <a href="expenses/delete/{{ $expense->id }}" class="tabledit-delete-button btn btn-danger waves-effect waves-light" role="button">
                                                                <span class="icofont icofont-ui-delete"></span>
                                                            </a>
                                                        @else
                                                            <a href="#" class="btn btn-default" role="button">No Delete</a>
                                                        @endif
                                                    @endforeach
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                            <div class="card-header">
                                @if($expenses->count()>0)
                                    <div class="toolbar">
                                        <span class="label label-danger">Total: {{$expenses->sum('amount')}}</span>
                                    </div>
                                @endif
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
<html>
<meta charset="utf-8">
<meta name="description" content="">
<meta name="author" content="Jean Carlos Nunez">
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<link href="assets/css/ablePro.css" rel="stylesheet">
<head>
    <title>{{ config('app.name', 'Laravel') }}</title>
</head>
<body>
<div class="content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="main-header">
                    <h4>Estados Financieros</h4>
                </div>
            </div>
        </div>
        <table class="table table-striped">

            <tr>
                <td>Id</td>
                <td>Edificio/Residencias</td>
                <td>Description</td>
                <td>Tipo</td>
                <td>Monto</td>
                <td>Fecha/Hora</td>
            </tr>

            @foreach($expenses as $expense)
                    <tr>
                        <td>{{ $expense->id }}</td>
                        <td>{{ $expense->description_builds }}</td>
                        <td>{{ $expense->description_statement }}</td>
                        <td>{{ $expense->type }}</td>
                        <td>{{ $expense->amount }}</td>
                        <td>{{ Carbon\Carbon::parse($expense->created_at)->format('l jS \\of F Y h:i:s A') }}</td>
                    </tr>
            @endforeach

            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td>Total Ingreso:</td>
                <td>{{ number_format($expenses->sum('amount'),2) }}</td>
                <td></td>
            </tr>


        </table>
    </div>
</div>

<script src="assets/js/jquery-3.1.1.min.js" ></script>
<script src="assets/js/bootstrap.min.js" ></script>
</body>
</html>
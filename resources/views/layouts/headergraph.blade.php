<html>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="Jean Carlos Nunez">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css">
    <!-- {{ Html::style('../resources/assets/css/dashboard.css') }} -->
    <!-- {{ Html::style('../resources/assets/css/signin.css') }} -->

    <head>
        <title>{{ config('app.name', 'Laravel') }}</title>
    </head>
    <body >
        <div class="container">
            @yield('content')

        </div>

    </body>

    <script src="http://code.jquery.com/jquery-1.12.0.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/vue/1.0.28/vue.min.js"></script>
    <script src="http://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>

    <!-- tether.js -->
    <script src="assets/js/tether.min.js"></script>

    <!-- waves effects.js -->
    <script src="assets/plugins/waves/js/waves.min.js"></script>

    <!-- Custom js -->
    <!-- <script type="text/javascript" src="assets/pages/elements.js"></script> -->

    <!-- Scrollbar JS-->
    <script src="assets/plugins/slimscroll/js/jquery.slimscroll.js"></script>
    <script src="assets/plugins/slimscroll/js/jquery.nicescroll.min.js"></script>

    <!--classic JS-->
    <!-- <script src="assets/plugins/search/js/classie.js"></script> -->

    <!-- table edit js  -->
    <!-- {{--<script src="assets/plugins/edit-table/js/jquery.tabledit.js"></script>--}} -->

    <!-- custom js -->
    {{--<script type="text/javascript" src="assets/js/main.js"></script>--}}
    <!-- {{--<script src="assets/pages/editable.js"></script>--}} -->
    <script type="text/javascript" src="assets/pages/elements.js"></script>
    <!-- <script src="assets/js/menu-horizontal.js"></script> -->

    <script>

    var ctxAsis = document.getElementById("myChartAsis");
    var ctxRD = document.getElementById("myChartRD");
    var id = document.getElementById("id").value;
    var can_pre = document.getElementById("cantidad_preguntas_cerradas").value;
    var url="http://52.18.22.122/";
  //  var url="http://franklin.local/";


        $.ajax({
            type: "get",
            url: url+"api/graph/evento/general/reports/" + id,
            dataType: "json",
            data: {},
            success: function (data) {
                var myChart = new Chart(ctxRD, {
                type: 'doughnut',
                data: data,
                options: {
                    segmentShowStroke: true,
                    segmentStrokeColor: "#fff",
                    segmentStrokeWidth: 2,
                    percentageInnerCutout: 20,
                    animationSteps: 100,
                    animationEasing: "easeOutBounce",
                    animateRotate: true,
                    animateScale: false,
                    responsive: true,
                    maintainAspectRatio: true,
                    showScale: true
                }
                });
            }
        });

        $.ajax({
            type: "get",
            url: url+"api/graph/evento/reports/general/norespondio/" + id,
            dataType: "json",
            data: {},
            success: function (data) {
                var myChart = new Chart(ctxAsis, {
                    type: 'doughnut',
                    data: data,
                    options: {
                        segmentShowStroke: true,
                        segmentStrokeColor: "#fff",
                        segmentStrokeWidth: 2,
                        percentageInnerCutout: 20,
                        animationSteps: 100,
                        animationEasing: "easeOutBounce",
                        animateRotate: true,
                        animateScale: false,
                        responsive: true,
                        maintainAspectRatio: true,
                        showScale: true
                    }
                });
            }
        });

        $.ajax({
            type: "get",
            url: url+"api/graph/evento/reports/general/promedio/" + id,
            dataType: "json",
            data: {},
            success: function (data) {

                for(var i in data) {
                    if(i=='promedios_neto') {
                        $("#promedio_neto").text("Promotores Neto:" + data[i]+" %");
                    }
                    else {
                        $("#promedio").text("Promedio:" + data[i]);
                    }
                }
            }
        });



        $.ajax({
            type: "get",
            url: url+"api/graph/evento/reports/preguntas/" + id,
            dataType: "json",
            data: {},
            success: function (data) {

                for(var i in data) {

                    var ctxDP = document.getElementById("myChartDP"+i);
                    var ctxBP = document.getElementById("myChartBP"+i);


                    var myChart = new Chart(ctxDP, {
                        type: 'doughnut',
                        data: data[i]['donut'],
                        options: {
                            segmentShowStroke: true,
                            segmentStrokeColor: "#fff",
                            segmentStrokeWidth: 2,
                            percentageInnerCutout: 20,
                            animationSteps: 100,
                            animationEasing: "easeOutBounce",
                            animateRotate: true,
                            animateScale: false,
                            responsive: true,
                            maintainAspectRatio: true,
                            showScale: true
                        }
                    });

                    var myChart = new Chart(ctxBP, {
                        type: 'doughnut',
                        data: data[i]['barra'],
                        options: {
                            segmentShowStroke: true,
                            segmentStrokeColor: "#fff",
                            segmentStrokeWidth: 2,
                            percentageInnerCutout: 20,
                            animationSteps: 100,
                            animationEasing: "easeOutBounce",
                            animateRotate: true,
                            animateScale: false,
                            responsive: true,
                            maintainAspectRatio: true,
                            showScale: true
                        }
                    });
                }

            }
        });









    </script>
</html>

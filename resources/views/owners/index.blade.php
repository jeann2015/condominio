@extends('layouts.header')
@section('content')

    <!-- @if(isset($notifys))

        <div class="alert alert-success" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
          <span aria-hidden="true">×</span>
          </button>
@foreach ($notifys as $noti => $valor)
            @if($valor == 1)
                <strong>Well done!</strong> You successfully insert your Data.
@endif
            @if($valor == 2)
                <strong>Well done!</strong> You successfully modified your Data.
@endif
            @if($valor == 3)
                <strong>Well done!</strong> You successfully removed your Data,
@endif
        @endforeach

                </div>

@endif -->

    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-header">
                        <h4>Propietarios</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="toolbar">
                                @foreach ($user_access as $user_acces)
                                    @if($user_acces->inserts == 1)
                                        <a href="{{ url('owners/add') }}" class="btn btn-default" role="button">Add </a>
                                    @else
                                        <a href="#" class="btn btn-default" role="button">No Add  </a>
                                    @endif
                                @endforeach

                                <a href="{{ url('home') }}" class="btn btn-default" role="button">Back </a>
                            </div>
                        </div>
                        <div class="card-block">
                            <table id="General" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Nombre</th>
                                    <th>Correo</th>
                                    <th>Telefono</th>
                                    <th>Edificio/Residencias</th>
                                    <th>Apartamento/Casa</th>
                                    <th>Estado</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($owners as $owner)
                                    <tr>
                                        <td>{{ $owner->id }}</td>
                                        <td>{{ $owner->fname." ".$owner->lname }}</td>
                                        <td>{{ $owner->email }}</td>
                                        <td>{{ $owner->phonenumber }}</td>
                                        <td>{{ $owner->description_builds }}</td>
                                        <td>{{ $owner->description_apartments }}</td>
                                        <td>
                                            @if($owner->status==1)
                                                {{ 'Activo' }}
                                            @else
                                                {{ 'No Activo' }}
                                             @endif
                                        </td>
                                        <td>
                                            <div class="tabledit-toolbar btn-toolbar">
                                                <div class="btn-group btn-group-sm">
                                                    @foreach ($user_access as $user_acces)
                                                        @if($user_acces->modifys == 1)
                                                            <a href="owners/edit/{{ $owner->id }}" class="tabledit-edit-button btn btn-primary waves-effect waves-light" role="button">
                                                                <span class="icofont icofont-ui-edit"></span>
                                                            </a>
                                                        @else
                                                            <a href="#" class="btn btn-default" role="button">No Modify</a>
                                                        @endif
                                                        @if($user_acces->deletes==1)
                                                            <a href="owners/delete/{{ $owner->id }}" class="tabledit-delete-button btn btn-danger waves-effect waves-light" role="button">
                                                                <span class="icofont icofont-ui-delete"></span>
                                                            </a>
                                                        @else
                                                            <a href="#" class="btn btn-default" role="button">No Delete</a>
                                                        @endif
                                                    @endforeach
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
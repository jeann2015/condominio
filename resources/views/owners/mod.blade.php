@extends('layouts.header')

@section('content')

	<div class="content-wrapper">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<div class="main-header">
						<!-- Tutulo del Formulario -->
						<h4>Modificar Propietario</h4>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="card">
						@if (count($errors) > 0)
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif
						<div class="card-header">
							<a href="{{ url('owners') }}" class="btn btn-default" role="button">Back </a>
						</div>
						<div class="card-block">
							{!! Form::open(array('url' => 'owners/update')) !!}
							{{--@foreach($owners as $owner)--}}
                                <div class="form-group">
                                    <label>Edificios/Residencias</label>
                                    {!! Form::select('id_builds', $builds, $owners->pluck('id_builds')->toArray(), ['id'=>'id_builds','class'=>'form-control','onchange'=>'search_apartments_name_witout_owners(this.value)']) !!}
                                </div>
                                <div class="form-group">
                                    <label>Apartments/Casa:</label>
                                    <br>
                                    {!! Form::select('id_apartments[]', $apartment_builds, $owners->pluck('id_apartments')->toArray(), ['id'=>'id_apartments','class'=>'form-control multiple-select','multiple'=>'true']) !!}
                                </div>
                                <div class="form-group">
                                    <label>Nombre</label>
                                    {!! Form::text('fname',$owners->pluck('fname')[0],array('class' => 'form-control','id'=>'fname','required')) !!}
                                </div>
                                <div class="form-group">
                                    <label>Apellido</label>
                                    {!! Form::text('lname',$owners->pluck('lname')[0],array('class' => 'form-control','id'=>'lname','required')) !!}
                                </div>
                                <div class="form-group">
                                    <label>Correo</label>
                                    {!! Form::email('email',$owners->pluck('email')[0],array('class' => 'form-control','id'=>'email','required')) !!}
                                </div>
                                <div class="form-group">
                                    <label>Telefono</label>
                                    {!! Form::text('phonenumber',$owners->pluck('phonenumber')[0],array('class' => 'form-control','id'=>'phonenumber','required')) !!}
                                </div>
                                <div class="form-group">
                                    <label>Estado</label>
                                    {!! Form::select('status', ['1'=>'Activo','0'=>'No Activo'], $owners->pluck('status')[0], ['class'=>'form-control','required']) !!}
                                </div>
							{{--@endforeach--}}
                            {!! Form::submit('Update!',array('class' => 'btn btn-primary','id'=>'update')) !!}
                            {!! Form::hidden('id',$owners->pluck('id')[0],array('id'=>'id')) !!}
							{!! Form::close() !!}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection
@extends('layouts.header')
@section('content')

    <!-- @if(isset($notifys))

        <div class="alert alert-success" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
          <span aria-hidden="true">×</span>
          </button>
@foreach ($notifys as $noti => $valor)
            @if($valor == 1)
                <strong>Well done!</strong> You successfully insert your Data.
@endif
            @if($valor == 2)
                <strong>Well done!</strong> You successfully modified your Data.
@endif
            @if($valor == 3)
                <strong>Well done!</strong> You successfully removed your Data,
@endif
        @endforeach

                </div>

@endif -->

    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-header">
                        <h4>Pagos</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="toolbar">
                                @if($administrators['id_administrator'] > 0)
                                    @foreach ($user_access as $user_acces)
                                        @if($user_acces->inserts == 1)
                                            <a href="{{ url('payments/add/a') }}" class="btn btn-default" role="button">Add </a>
                                            <a href="{{ url('payments/add/m') }}" class="btn btn-default" role="button">Add Manual </a>
                                        @else
                                            <a href="#" class="btn btn-default" role="button">No Add  </a>
                                        @endif
                                    @endforeach
                                @endif
                                <a href="{{ url('home') }}" class="btn btn-default" role="button">Back </a>
                            </div>
                        </div>
                        <div class="card-block">
                            <table id="General" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Propietario</th>
                                    <th>Edificio/Residencia</th>
                                    <th>Apartamento/Casa</th>
                                    <th>Monto</th>
                                    <th>Estado</th>
                                    <th>Tipo</th>
                                    <th>Fecha/Hora</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($payments as $payment)
                                    <tr>
                                        <td>{{ $payment->id }}</td>
                                        <td>{{ $payment->description_owners }}</td>
                                        <td>{{ $payment->description_builds }}</td>
                                        <td>{{ $payment->description_apartments }}</td>
                                        <td>{{ $payment->amount }}</td>
                                        @if($payment->id_status==2)
                                            <td><span class="label label-danger">{{ $payment->description_status }}</span></td>
                                        @elseif($payment->id_status==3)
                                            <td><span class="label label-warning">{{ $payment->description_status }}</span></td>
                                        @elseif($payment->id_status==1)
                                            <td><span class="label label-success">{{ $payment->description_status }}</span></td>
                                        @endif
                                        <td>{{ strtoupper($payment->type) }}</td>
                                        <td>{{ Carbon\Carbon::parse($payment->created_at)->format('l jS \\of F Y h:i:s A') }}</td>
                                        <td>
                                            <div class="tabledit-toolbar btn-toolbar">
                                                <div class="btn-group btn-group-sm">
                                                    @foreach ($user_access as $user_acces)
                                                        @if($user_acces->modifys == 1)
                                                            <a title="Ver PDF" href="payments/pdf/{{ $payment->id }}" class="tabledit-edit-button btn btn-info waves-effect waves-light" role="button">
                                                                <span class="fa fa-file-pdf-o"></span>
                                                            </a>
                                                            <a href="payments/edit/{{ $payment->id }}" class="tabledit-edit-button btn btn-primary waves-effect waves-light" role="button">
                                                                <span title="Ver Estado de Cuenta" class="fa fa-money"></span>
                                                            </a>
                                                        @else
                                                            <a href="#" class="btn btn-default" role="button">No Modify</a>
                                                        @endif

                                                    @endforeach
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                        @if($payments)
                            <div class="card-header">
                                <div class="toolbar">
                                    <span class="label label-danger">Pago Pendiente: {{$payments->where('id_status',2)->sum('amount')}}</span>
                                    <span class="label label-warning">Pago Recibido: {{$payments->where('id_status',3)->sum('amount')}}</span>
                                    <span class="label label-success">Pago Realizado: {{$payments->where('id_status',1)->sum('amount')}}</span>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection
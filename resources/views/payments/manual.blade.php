@extends('layouts.header')
@section('content')
<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="main-header">
					<!-- Tutulo del Formulario -->
					<h4>Nuevos Pagos Manual</h4>
				</div>
			</div>
		</div>	
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
					<div class="card-header">
						<a href="{{ url('payments') }}" class="btn btn-default" role="button">Back </a>
					</div>
					<div class="card-block">
						{!! Form::open(array('url' => 'payments/new/m')) !!}
						<div class="form-group">
							<label>Edificios/Residencias</label>
							{!! Form::select('id_builds', $builds, null, ['id'=>'id_builds','class'=>'form-control','onchange'=>'search_apartments_name_owners(this.value)']) !!}
						</div>
						<div class="form-group">
							<label>Apartments/Casas/Propietarios:</label>
							<br>
							{!! Form::select('id_apartments', [], null, ['id'=>'id_apartments','class'=>'form-control','required']) !!}
						</div>
						<div class="form-group">
							<label>Descripcion</label>
							{!! Form::text('description','',array('class' => 'form-control','id'=>'description','required')) !!}
						</div>
						<div class="form-group">
							<label>Monto</label>
							{!! Form::number('amount','',array('class' => 'form-control','id'=>'amount','required','step'=>'any')) !!}
						</div>
						{!! Form::submit('Generar Cuenta!',array('class' => 'btn btn-primary','id'=>'pagos')) !!}
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
@extends('layouts.header')
@section('content')
<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="main-header">
					<!-- Tutulo del Formulario -->
					<h4>Modificar Pagos</h4>
				</div>
			</div>
		</div>	
		<div class="row">
			<div class="col-lg-12">
				<div class="card">

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

					<div class="card-header">
						<a href="{{ url('payments') }}" class="btn btn-default" role="button">Back </a>
					</div>
					<div class="card-block">
						{!! Form::open(array('url' => 'payments/update','enctype'=>'multipart/form-data')) !!}
                        @foreach($payments as $payment)
						<div class="form-group">
							<label>Propietarios</label>
							{!! Form::text('owners', $payment->description_owners,array('class' => 'form-control','id'=>'owners','readonly')) !!}
						</div>

						<div class="form-group">
							<label>Edificios/Redidencias</label>
							{!! Form::text('builds',$payment->description_builds ,array('class' => 'form-control','id'=>'builds','readonly')) !!}
						</div>
						<div class="form-group">
							<label>Monto</label>
							{!! Form::text('amount',$payment->amount ,array('class' => 'form-control','id'=>'amount','readonly')) !!}
						</div>

                        @if($administrators['id_administrator'] > 0)
                            <div class="form-group">
                                <label>Descripcion</label>
                                {!! Form::text('description', $payment->description_details ,array('class' => 'form-control','id'=>'description','readonly')) !!}
                            </div>

                            <div class="form-group">
                                <label>Banco</label>
                                {!! Form::text('bank', $payment->bank ,array('class' => 'form-control','id'=>'bank','readonly')) !!}
                            </div>
                            <div class="form-group">
                                <label>Numero de Transaccion</label>
                                {!! Form::text('number_transaction',$payment->number_transaction ,array('class' => 'form-control','id'=>'number_transaction','readonly')) !!}
                            </div>
                            <div class="form-group">
                                <label>Banco</label>
                                {!! Form::text('banco',$payment->bank ,array('class' => 'form-control','id'=>'banco','readonly')) !!}
                            </div>
                            <div class="form-group">
                                <label>Numero de Transaccion</label>
                                {!! Form::text('number_transaction',$payment->number_transaction ,array('class' => 'form-control','id'=>'number_transaction','readonly')) !!}
                            </div>
                            @if($payment->type=='manual')
                                <div class="form-group">
                                    <label>Descripcion de Cuenta Manual</label>
                                    {!! Form::text('descriptionM', $payment->description ,array('class' => 'form-control','id'=>'descriptionM','readonly')) !!}
                                </div>
                            @endif
                            <div class="form-group">
                                <label>Estado</label>
                                {!! Form::select('id_status', $status, $payment->id_status, ['id'=>'id_status','class'=>'form-control','required']) !!}
                            </div>

                            <div class="form-group">
                                <label>Imagen:</label>
                                <br>

                                {<img src="{{ url( $pathToFile ) }}" height="350" width="350">
                                <br>
                                {!! Form::submit('Modificar!',array('class' => 'btn btn-primary','id'=>'modificar')) !!}
                                {!! Form::hidden('id',$payment->id,array('id'=>'id')) !!}
                            </div>

                        @else
                            <div class="form-group">
                                <label>Descripcion</label>
                                {!! Form::text('description', $payment->description_details ,array('class' => 'form-control','id'=>'description','required')) !!}
                            </div>

                            <div class="form-group">
                                <label>Banco</label>
                                {!! Form::text('bank', $payment->bank ,array('class' => 'form-control','id'=>'bank','required')) !!}
                            </div>
                            <div class="form-group">
                                <label>Numero de Transaccion</label>
                                {!! Form::text('number_transaction',$payment->number_transaction ,array('class' => 'form-control','id'=>'number_transaction','required')) !!}
                            </div>
                            <div class="form-group">
                                <label>Estado</label>
                                {!! Form::text('id_status',$payment->description_status ,array('class' => 'form-control','id'=>'id_status','readonly')) !!}
                            </div>
                            @if($payment->type=='manual')
                                <div class="form-group">
                                    <label>Descripcion de Cuenta Manual</label>
                                    {!! Form::text('descriptionM', $payment->description ,array('class' => 'form-control','id'=>'descriptionM','readonly')) !!}
                                </div>
                            @endif
                            @if($payment->id_status==3 || $payment->id_status==2)
                                <div class="form-group">
                                    <label>Imagen:</label>
                                    <br>
                                    @if($payment->id_status==3)
                                        <img src="{{ url( $pathToFile ) }}" height="350" width="350">
                                    @endif
                                    <br>
                                    {!! Form::file('images', ['id'=>'images','class'=>'form-control','submit']) !!}
                                    {!! Form::submit('Modificar!',array('class' => 'btn btn-primary','id'=>'modificar')) !!}
                                    {!! Form::hidden('id',$payment->id,array('id'=>'id')) !!}
                                </div>
                                @elseif($payment->id_status==1)
                                    <div class="form-group">
                                        <label>Imagen:</label>
                                        <br>
                                        <img src="{{ url(''.$payment->id.".jpg" ) }}" height="350" width="350">
                                    </div>
                            @endif
                        @endif
						{!! Form::close() !!}
                        @endforeach
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
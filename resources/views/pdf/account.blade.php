<html>
<meta charset="utf-8">
<meta name="description" content="">
<meta name="author" content="Jean Carlos Nunez">
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<link href="assets/css/ablePro.css" rel="stylesheet">
<head>
    <title>{{ config('app.name', 'Laravel') }}</title>
</head>
<body>
<div class="content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="main-header">
                    <h4>Reporte Estados de Cuenta</h4>
                </div>
            </div>
        </div>
        <table class="table table-striped">
            <tr>
                <td>Id</td>
                <td>Description</td>
                <td>Edificio/Residencia</td>
                <td>Apartamento/Casa</td>
                <td>Estado</td>
                <td>Tipo</td>
                <td>Monto</td>
                <td>Fecha/Hora</td>
            </tr>


            @foreach($payments as $payment)
                <tr>
                    <td>{{ $payment->id }}</td>
                    <td>{{ $payment->description }}</td>
                    <td>{{ $payment->description_builds }}</td>
                    <td>{{ $payment->description_apartments." ".$payment->description_owners }}</td>
                    <td>{{ $payment->description_status }}</td>
                    <td>{{ strtoupper($payment->type) }}</td>
                    <td>{{ $payment->amount }}</td>
                    <td>{{ $payment->created_at }}</td>
                </tr>
            @endforeach

            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>Total:</td>
                @if(isset($payments))
                    <td>{{ number_format($payments->sum('amount'),2) }}</td>
                @else
                    <td>{{ '0.00' }}</td>
                @endif
                <td></td>
            </tr>
        </table>
    </div>
</div>

<script src="assets/js/jquery-3.1.1.min.js" ></script>
<script src="assets/js/bootstrap.min.js" ></script>
</body>
</html>
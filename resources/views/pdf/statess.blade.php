<html>
<meta charset="utf-8">
<meta name="description" content="">
<meta name="author" content="Jean Carlos Nunez">
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<link href="assets/css/ablePro.css" rel="stylesheet">
<head>
    <title>{{ config('app.name', 'Laravel') }}</title>
</head>
<body>
<div class="content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="main-header">
                    <h4>Reporte Estados Financieros</h4>
                </div>
            </div>
        </div>
        <table class="table table-striped">

            <tr>
                <td>Id</td>
                <td>Edificio/Residencias</td>
                <td>Description</td>
                <td>Tipo</td>
                <td>Monto</td>
                <td>Fecha/Hora</td>
            </tr>

            <tr>
                <td colspan="6" class="success">
                    <h4>
                        Ingresos
                    </h4>
                </td>
            </tr>

            @foreach($expenses as $expense)
                @if($expense->type=='ingreso')
                    <tr>
                        <td>{{ $expense->id }}</td>
                        <td>{{ $expense->description_builds }}</td>
                        <td>{{ $expense->description_statement }}</td>
                        <td>{{ $expense->type }}</td>
                        <td>{{ $expense->amount }}</td>
                        <td>{{ $expense->created_at }}</td>
                    </tr>
                @endif
            @endforeach

            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td>Total Ingreso:</td>
                <td>{{ number_format($expenses->where('type','ingreso')->sum('amount'),2) }}</td>
                <td></td>
            </tr>
            <tr>
                <td colspan="6" class="danger">
                    <h4>
                    Gastos
                    </h4>
                </td>
            </tr>
            @foreach($expenses as $expense)
                @if($expense->type=='gasto')
                <tr>
                    <td>{{ $expense->id }}</td>
                    <td>{{ $expense->description_builds }}</td>
                    <td>{{ $expense->description_statement }}</td>
                    <td>{{ $expense->type }}</td>
                    <td>{{ $expense->amount }}</td>
                    <td>{{ $expense->created_at }}</td>
                </tr>
                @endif
            @endforeach

            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td>Total Gastos:</td>
                <td>{{ number_format($expenses->where('type','gasto')->sum('amount'),2) }}</td>
                <td></td>
            </tr>

            <tr>
                <td colspan="6" class="danger">
                    <h3>
                        Total General: {{ number_format(number_format($expenses->where('type','ingreso')->sum('amount'),2) - number_format($expenses->where('type','gasto')->sum('amount'),2),2) }}
                    </h3>
                </td>
            </tr>

        </table>
    </div>
</div>

<script src="assets/js/jquery-3.1.1.min.js" ></script>
<script src="assets/js/bootstrap.min.js" ></script>
</body>
</html>
@extends('layouts.header')

@section('content')

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="main-header">
					<!-- Tutulo del Formulario -->
					<h4>Nueva Encuesta</h4>
				</div>
			</div>
		</div>	
		<div class="row">
			<div class="col-lg-12">
				<div class="card">

					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<div class="card-header">
						<a href="{{ url('polls') }}" class="btn btn-default" role="button">Back </a>
					</div>
					<div class="card-block">
						<!-- Formulario -->
						{!! Form::open(array('url' => 'polls/new')) !!}

						<div class="form-group">
							<label>Edificios/Redidencias</label>
							{!! Form::select('id_builds', $builds, null, ['id'=>'id_builds','class'=>'form-control','onchange'=>'search_apartments_name_owners(this.value)']) !!}
						</div>
						<div class="form-group">
							<label>Apartments/Casas/Propietarios:</label>
							<br>
							{!! Form::select('id_apartments[]', [], null, ['id'=>'id_apartments','class'=>'form-control multiple-select','multiple' => true]) !!}
						</div>
                        <div class="form-group">
                            <label>Pregunta:</label>
                            {!! Form::textarea('description','',array('class' => 'form-control','id'=>'description','required')) !!}
                        </div>
						<div class="form-group">
							<table class="table">
								<tr>
									<td>Desde</td>
									<td>{!! Form::text('desde', '',array('class' => 'form-control','id'=>'desde','required')) !!}</td>
									<td>Hasta</td>
									<td>{!! Form::text('hasta','',array('class' => 'form-control','id'=>'hasta','required')) !!}</td>
								</tr>
							</table>
						</div>

						{!! Form::submit('Generar Encuesta!',array('class' => 'btn btn-primary','id'=>'pagos')) !!}
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
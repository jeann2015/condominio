@extends('layouts.header')
@section('content')

    <!-- @if(isset($notifys))

        <div class="alert alert-success" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
          <span aria-hidden="true">×</span>
          </button>
@foreach ($notifys as $noti => $valor)
            @if($valor == 1)
                <strong>Well done!</strong> You successfully insert your Data.
@endif
            @if($valor == 2)
                <strong>Well done!</strong> You successfully modified your Data.
@endif
            @if($valor == 3)
                <strong>Well done!</strong> You successfully removed your Data,
@endif
        @endforeach

                </div>

@endif -->

    <div class="modal fade modal-flex" id="Modal-overflowV" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">

            <div class="modal-content">
                <div class="modal-body model-container">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h5 class="font-header">Encuesta Realizada</h5>

                    <div class="form-group">
                        <label>Pregunta/Descripcion?</label>
                        {!! Form::textarea('descriptionV','',array('class' => 'form-control','id'=>'descriptionV','required','readonly')) !!}
                    </div>
                    <div class="form-group">
                        <label>Estado</label>
                        <br>
                        {!! Form::text('statusV','',array('class' => 'form-control','id'=>'statusV','required','readonly')) !!}

                    </div>


                </div>

            </div>
        </div>
    </div>

    <div class="modal fade modal-flex" id="Modal-overflow" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">

            <div class="modal-content">
                <div class="modal-body model-container">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h5 class="font-header">Encuesta</h5>

                    {!! Form::open(array('url' => 'polls/anwers')) !!}

                    <div class="form-group">
                        <label>Pregunta/Descripcion?</label>
                        {!! Form::textarea('description','',array('class' => 'form-control','id'=>'description','required','readonly')) !!}
                    </div>
                    <div class="form-group">
                        <label>Estado</label>
                        <br>
                        {!! Form::select('id_status', $status->pluck('description','id'), '', ['id'=>'id_status','class'=>'form-control','required']) !!}

                    </div>
                        {!! Form::submit('Votar!',array('class' => 'btn btn-primary','id'=>'vote')) !!}
                        @foreach($polls as $poll)
                            {!! Form::hidden('id',$poll->id,array('id'=>'id')) !!}
                        @endforeach
                    {!! Form::close() !!}
                </div>
                <div class="p-15">
                </div>
            </div>
        </div>
    </div>
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-header">
                        <h4>Encuestas</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="card-header">
                            <div class="toolbar">
                                @foreach ($user_access as $user_acces)
                                    @if($user_acces->inserts == 1   && $idAdministrator<>"" )
                                        <a href="{{ url('polls/add') }}" class="btn btn-default" role="button">Add </a>
                                    @endif
                                @endforeach

                                <a href="{{ url('home') }}" class="btn btn-default" role="button">Back </a>
                            </div>
                        </div>
                        <div class="card-block">
                            <table id="General" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Descripcion</th>
                                    <th>Edificio/Residencia</th>
                                    <th>Estado de Encuesta</th>
                                    @if($idAdministrator=="")
                                        <th>Respondida?</th>
                                    @endif
                                    <th>Fecha/Hora</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($polls as $poll)
                                    <tr>
                                        <td>{{ $poll->id }}</td>
                                        <td>{{ $poll->description }}</td>
                                        <td>{{ $poll->description_builds }}</td>


                                        @if($poll->id_status==7)
                                            <td><span class="label label-success">{{ $poll->description_status }}</span></td>
                                        @elseif($poll->id_status==8)
                                            <td><span class="label label-danger">{{ $poll->description_status }}</span></td>
                                        @elseif($poll->id_status==9)
                                            <td><span class="label label-warning">{{ $poll->description_status }}</span></td>
                                        @endif
                                        @if($idAdministrator=="")
                                            @if($PollsAnwer->where('id_polls',$poll->id)->count()>0)
                                                <td><span class="label label-success">{{ 'Si' }}</span></td>
                                            @else
                                                <td><span class="label label-danger">{{ 'No' }}</span></td>
                                            @endif
                                        @endif
                                        <td>{{ Carbon\Carbon::parse($poll->created_at)->format('l jS \\of F Y h:i:s A') }}</td>
                                        <td>
                                            <div class="tabledit-toolbar btn-toolbar">
                                                <div class="btn-group btn-group-sm">
                                                    @foreach ($user_access as $user_acces)
                                                        @if($user_acces->views == 1 && $idAdministrator=="")
                                                            @if($PollsAnwer->where('id_polls',$poll->id)->count()==0)
                                                                <a onclick="search_polls_vote({{ $poll->id }})" data-toggle="modal" data-target="#Modal-overflow" class="tabledit-edit-button btn btn-success waves-effect waves-light" role="button">
                                                                    <span title="Votar" class="fa fa-check-circle"></span>
                                                                </a>
                                                            @else
                                                                <a onclick="search_polls_vote_done({{ $poll->id }})" data-toggle="modal" data-target="#Modal-overflowV" class="tabledit-edit-button btn btn-danger waves-effect waves-light" role="button">
                                                                    <span title="Ver Encuesta" class="fa fa-check-circle"></span>
                                                                </a>
                                                                @if($poll->id_status==8)
                                                                    <a class="tabledit-edit-button btn btn-success waves-effect waves-light" href="polls/view/{{ $poll->id }}" data-toggle="modal" role="button">
                                                                        <span title="Ver Encuesta" class="icofont icofont-search-alt-2"></span>
                                                                    </a>
                                                                @endif
                                                            @endif
                                                        @endif

                                                        @if($user_acces->modifys == 1 && $idAdministrator<>"")
                                                            <a class="tabledit-edit-button btn btn-success waves-effect waves-light" href="polls/view/{{ $poll->id }}" data-toggle="modal" role="button">
                                                                <span title="Ver Encuesta" class="icofont icofont-search-alt-2"></span>
                                                            </a>
                                                            <a href="polls/edit/{{ $poll->id }}" class="tabledit-edit-button btn btn-primary waves-effect waves-light" role="button">
                                                                <span class="icofont icofont-ui-edit"></span>
                                                            </a>
                                                        @else
                                                        @endif
                                                        @if($user_acces->deletes==1 && $idAdministrator<>"")
                                                            <a href="polls/delete/{{ $poll->id }}" class="tabledit-delete-button btn btn-danger waves-effect waves-light" role="button">
                                                                <span class="icofont icofont-ui-delete"></span>
                                                            </a>
                                                        @else
                                                        @endif

                                                    @endforeach
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
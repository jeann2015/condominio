@extends('layouts.header')

@section('content')

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="main-header">
					<!-- Tutulo del Formulario -->
					<h4>Ver Resultado Encuesta</h4>
				</div>
			</div>
		</div>	
		<div class="row">
			<div class="col-lg-12">
				<div class="card">

					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<div class="card-header">
						<a href="{{ url('polls') }}" class="btn btn-default" role="button">Back </a>
					</div>
					<div class="card-block">

						{!! Form::open(array('url' => 'polls')) !!}
                        <div class="form-group">
                            <label>Resultados:</label>
                            <br>
                            <div class="container" align="center">
                            <span class="badge badge-success header-badger">Aprobado:{{ $resultado['si'] }}</span>

                            <span class="badge badge-danger header-badger">No Aprobado:{{ $resultado['no'] }}</span>

                            <span class="badge badge-primary header-badger">Total Votantes:{{ $resultado['all'] }}</span>
                            </div>

                        </div>
						<div class="form-group">
							<label>Edificios/Redidencias</label>
							{!! Form::text('id_builds',$builds->where('id',$polls->id_builds)->pluck('description')[0],array('class' => 'form-control','id'=>'id_builds','readonly')) !!}

						</div>

                        <div class="form-group">
                            <label>Pregunta:</label>
                            {!! Form::textarea('description',$polls->description,array('class' => 'form-control','id'=>'description','readonly')) !!}
                        </div>
						<div class="form-group">
							<table class="table">
								<tr>
									<td>Desde</td>
									<td>{!! Form::text('desdev', $polls->from,array('class' => 'form-control','id'=>'desdev','required','readonly')) !!}</td>
									<td>Hasta</td>
									<td>{!! Form::text('hastav',$polls->to,array('class' => 'form-control','id'=>'hastav','required','readonly')) !!}</td>
								</tr>
							</table>
						</div>
						<div class="form-group">
							<label>Estado</label>
							<br>
{{--							{!! Form::select('id_status', $status->pluck('description','id'), $polls->id_status, ['id'=>'id_status','class'=>'form-control','required']) !!}--}}
                            {!! Form::text('id_status',$status->where('id',$polls->id_status)->pluck('description')[0],array('class' => 'form-control','id'=>'id_status','readonly')) !!}

						</div>
{{--						{!! Form::submit('Update!',array('class' => 'btn btn-primary','id'=>'update')) !!}--}}
                        {!! Form::hidden('id',$polls->id,array('id'=>'id')) !!}
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
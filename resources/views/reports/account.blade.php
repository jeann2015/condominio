@extends('layouts.header')


@section('content')

    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-header">
                        <h4>Reporte Estados de Cuenta</h4>
                    </div>
                </div>
            </div>
          

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <a href="{{ url('home') }}" class="btn btn-default" role="button">Back </a>
                    </div>
                    <div class="card-block">
                        <!-- Formulario -->
                        {!! Form::open(array('url' => 'reportsaccount/account/search')) !!}
                        <div class="form-group">
                            <label>Edificios/Residencias</label>
                            {!! Form::select('id_builds', $builds, null, ['id'=>'id_builds','class'=>'form-control','onchange'=>'search_apartments_name_owners_all(this.value)','required']) !!}
                        </div>
                        <div class="form-group">
                            <label>Apartments/Casas/Propietarios:</label>
                            <br>
                            {!! Form::select('id_apartments', [], null, ['id'=>'id_apartments','class'=>'form-control','required']) !!}
                        </div>
                        <div class="form-group">
                            <label>Estado de Cuenta:</label>
                            <br>
                            {!! Form::select('id_status', $status, null, ['id'=>'id_status','class'=>'form-control','required']) !!}
                        </div>
                        <div class="form-group">
                            <table class="table">
                                <tr>
                                    <td>Desde</td>
                                    <td>{!! Form::text('desde', $values['desde'] ,array('class' => 'form-control','id'=>'desde','required')) !!}</td>
                                    <td>Hasta</td>
                                    <td>{!! Form::text('hasta',$values['hasta'],array('class' => 'form-control','id'=>'hasta','required')) !!}</td>
                                </tr>
                            </table>
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Buscar!',array('class' => 'btn btn-primary','id'=>'buscar')); !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>


@endsection




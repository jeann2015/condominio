@extends('layouts.headerreports')
@section('content')

    <table class="table table-striped">
        <tr>
            <td colspan="5">
                <a href="{{ url('home') }}" class="btn btn-default" role="button">Back </a>
            </td>
        </tr>
        <tr>
            <td colspan="5">
                Reporte Consolidado
            </td>
        </tr>

        <tr>
            <td>Pais:</td>
            <td colspan="4">
                {!! Form::select('pais',$pais, '', ['placeholder' => 'Todos','class'=>'form-control','required']) !!}
            </td>
        </tr>
        <tr>
            <td>Tipo:</td>
            <td colspan="4">
                {!! Form::select('tipoevento', $tipoeventos, '', ['placeholder' => 'Todos','class'=>'form-control','required']) !!}
            </td>
        </tr>
        <tr>
            <td>Solución:</td>
            <td colspan="4">
                {!! Form::select('solucion',$soluciones, '', ['placeholder' => 'Todos','class'=>'form-control','required']) !!}
            </td>
        </tr>

        <tr>
            <td>Industrias:</td>
            <td colspan="4">
                {!! Form::select('industria',$industrias, '', ['placeholder' => 'Todos','class'=>'form-control','required']) !!}
            </td>
        </tr>

        <tr>
            <td>Facilitador:</td>
            <td colspan="4">
                {!! Form::select('facilitador',$facilitador,'', ['placeholder' => 'Todos','class'=>'form-control','required']) !!}
            </td>
        </tr>
        <tr>
            <td>Desde:</td>
            <td>
                {!! Form::text('desde', Carbon\Carbon::now()->toDateString() ,array('class' => 'form-control','id'=>'desde','required')) !!}
            </td>
            <td>Hasta:</td>
            <td>
                {!! Form::text('hasta',Carbon\Carbon::now()->toDateString(),array('class' => 'form-control','id'=>'hasta','required')) !!}
            </td>
        </tr>
        <tr>
            <td colspan="5">
                {!! Form::button('Buscar!',array('class' => 'btn btn-primary','id'=>'buscar')); !!}
            </td>
        </tr>

        <tr class="success">
            <td>Fecha</td>
            <td>Solución</td>
            <td>Consultores</td>
            <td>Pais</td>
            <td>Organizacion</td>
        </tr>


    </table>

@endsection
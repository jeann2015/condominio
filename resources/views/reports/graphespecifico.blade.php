@extends('layouts.headergraph')
@section('content')


    {!! Form::open(array('url' => 'respecifico/ver/report/graph')) !!}
    <table class="table ">
        @foreach ($eventos as $evento)
        <tr>
            <td colspan="2">
                <a href="{{ url('respecifico/ver/report/'.$evento->id) }}" class="btn btn-default" role="button">Back </a>
                {!! Form::submit('Refrescar!',array('class' => 'btn btn-primary','id'=>'refrescar')) !!}
                {!! Form::hidden('id',$evento->id,array('class' => 'form-control','id'=>'id','required')) !!}
                {!! Form::hidden('cantidad_preguntas_cerradas',$preguntas_cerrada->count(),array('class' => 'form-control','id'=>'cantidad_preguntas_cerradas','required')) !!}
            </td>
        </tr>
        <tr class="info">
            <td colspan="2">
                Datos del Evento
            </td>
        </tr>

        <tr>
            <td>{{ $evento->tipo }}:</td>
            <td colspan="2">
                {{ $evento->tipo }}
            </td>
        </tr>
        <tr>
            <td>Facilitador:</td>
            <td colspan="2">
                {{ $evento->facilitador }}
            </td>
        </tr>
        <tr>
            <td>Fecha:</td>
            <td colspan="2">
                {{ $evento->fecha_evento }}
            </td>
        </tr>
        <tr>
            <td>Organización:</td>
            <td colspan="2">
                {{ $evento->organizacion }}
            </td>
        </tr>
        <tr>
            <td>Lugar:</td>
            <td colspan="2">
                {{ $evento->lugar }}
            </td>
        </tr>

        <tr>
            <td>Asistentes:</td>
            <td colspan="2">
                {{ $evento->asistentes }}
            </td>
        </tr>


        <tr>
            <td>Participantes:</td>
            <td colspan="2">
                {{ $evento->participantes }}
            </td>
        </tr>
        @endforeach

            <tr class="info">
                <td colspan="2">
                    Resumen de Evaluación
                </td>
            </tr>
        <tr>
            <td>
                {{--{{ Form::label('promedio', 'Promedio', array('class' => 'form-control','id'=>'promedio')) }}--}}

                <h1><span id="promedio" class="badge badge-default">Promedio:</span></h1>

                <h1><span id="promedio_neto" class="badge badge-default">Promedio Neto:</span></h1>

            </td>
            <td>
                    <div style="float:right" >
                        <canvas id="myChartRD" ></canvas>
                    </div>
                    <div style="float:right" >
                        <canvas id="myChartAsis" ></canvas>
                    </div>
            </td>
        </tr>
            @php
                $con=1;
            @endphp

        @foreach($preguntas_cerrada as $preguntas_cerrad)

                <tr class="alert-danger">
                    <td colspan="2">Pregunta{{ $con }}: {{ $preguntas_cerrad->description  }}</td>
                </tr>
            <tr>

                <td>
                    <br>
                    <div style="float:right" >
                        <canvas id="myChartBP{{ $preguntas_cerrad->id }}" ></canvas>
                    </div>
                </td>
                <td>
                    <br>
                    <div style="float:right" >
                        <canvas id="myChartDP{{ $preguntas_cerrad->id }}" ></canvas>
                    </div>
                </td>
            </tr>
                @php
                    $con=$con+1;
                @endphp
        @endforeach
            <tr class="info">
                <td colspan="2">Comentarios</td>

            </tr>
        @foreach($preguntas_abiertas as $preguntas_abierta)

            <tr class="alert-danger">
                <td colspan="2">{{ $preguntas_abierta->description  }}</td>
            </tr>
                @foreach($respuestas_abiertas as $respuestas_abierta)
                    @if($respuestas_abierta->id_preguntas == $preguntas_abierta->id)
                        <tr>
                            <td colspan="2">{{$respuestas_abierta->respuesta}}</td>
                        </tr>
                    @endif
                @endforeach
        @endforeach

    </table>

    {!! Form::close() !!}

@endsection

@extends('layouts.header')
@section('content')

    <!-- @if(isset($notifys))

        <div class="alert alert-success" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
          <span aria-hidden="true">×</span>
          </button>
@foreach ($notifys as $noti => $valor)
            @if($valor == 1)
                <strong>Well done!</strong> You successfully insert your Data.
@endif
            @if($valor == 2)
                <strong>Well done!</strong> You successfully modified your Data.
@endif
            @if($valor == 3)
                <strong>Well done!</strong> You successfully removed your Data,
@endif
        @endforeach

                </div>

@endif -->

    <div class="modal fade modal-flex" id="Modal-overflow" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">

            <div class="modal-content">
                <div class="modal-body model-container">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h5 class="font-header">Nueva Reservacion</h5>

                    {!! Form::open(array('url' => 'reservations/new')) !!}
                    <table class="table">
                        <thead>
                        <div class="form-group">
                            <label>Descripción</label>
                            {!! Form::textarea('description','',array('class' => 'form-control','id'=>'description','required')) !!}
                        </div>
                        <div class="form-group">
                            <label>Edificios/Residencias</label>
                            {!! Form::select('id_builds', $builds, null, ['id'=>'id_builds','class'=>'form-control','onchange'=>'search_apartments(this.value)']) !!}
                        </div>
                        <div class="form-group">
                            <label>Apartments/Casas/Propietarios:</label>
                            <br>
                            {!! Form::select('id_apartments', [], null, ['id'=>'id_apartments','class'=>'form-control ']) !!}
                        </div>

                        <tr>
                            <td>
                                Fecha desde:
                            </td>
                            <td>
                            {!! Form::text('date_from', '' ,array('class' => 'form-control','id'=>'date_from','required')) !!}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Hora desde:
                            </td>
                            <td>
                                {!! Form::text('time_from','' ,array('class' => 'form-control','id'=>'time_from','required')) !!}
                            </td>
                        </tr>

                        <tr>
                            <td>
                                Fecha Hasta:
                            </td>
                            <td>
                                {!! Form::text('date_to', '' ,array('class' => 'form-control','id'=>'date_to','required')) !!}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Hora Hasta:
                            </td>
                            <td>
                                {!! Form::text('time_to','' ,array('class' => 'form-control','id'=>'time_to','required')) !!}
                            </td>
                        </tr>

                        </thead>
                    </table>
                    {!! Form::submit('Guardar Reservacion!',array('class' => 'btn btn-primary','id'=>'save')) !!}
                    {!! Form::hidden('id_administrator',$idAdministrator,array('id'=>'id_administrator')) !!}
                    {!! Form::close() !!}
                </div>
                <div class="p-15">
                </div>
            </div>
        </div>
    </div>

    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-header">
                        <h4>Reservaciones</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="card-header">
                            <div class="toolbar">
                                @if($idAdministratorOwner<>"")
                                @foreach ($user_access as $user_acces)
                                    @if($user_acces->inserts == 1)
                                        <a data-toggle="modal" data-target="#Modal-overflow" class="btn btn-default" role="button">Add </a>
                                    @else
                                        <a href="#" class="btn btn-default" role="button">No Add  </a>
                                    @endif
                                @endforeach
                                @endif
                                <a href="{{ url('home') }}" class="btn btn-default" role="button">Back </a>
                            </div>
                        </div>
                        <div class="card-block">
                            <div class="toolbar" id='calendar'></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
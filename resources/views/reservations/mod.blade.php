@extends('layouts.header')

@section('content')
	<div class="content-wrapper">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<div class="main-header">
						<h4>Modificar/Eliminar Reservacion</h4>
					</div>
				</div>
			</div>
			@if (count($errors) > 0)
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
			<div class="row">
				<div class="col-lg-12">
					<div class="card">
						<div class="card-header">
							<a href="{{ url('reservations') }}" class="btn btn-default" role="button">Back </a>
						</div>
						<div class="card-block">
                            @foreach($reservations as $reservation)

							{!! Form::open(array('url' => 'reservations/update')) !!}
							<table class="table">
								<thead>
								<div class="form-group">
									<label>Descripción</label>
									{!! Form::textarea('description',$reservation->description,array('class' => 'form-control','id'=>'description','required')) !!}
								</div>
								<div class="form-group">
									<label>Edificios/Residencias</label>
                                    {!! Form::text('builds', $reservation->description_builds ,array('class' => 'form-control','id'=>'builds','required','readonly')) !!}
								</div>
								<div class="form-group">
									<label>Apartments/Casas/Propietarios:</label>
									<br>
                                    {!! Form::text('apartments', $reservation->description_apartments ,array('class' => 'form-control','id'=>'apartments','required','readonly')) !!}
								</div>

								<tr>
									<td>
										Fecha desde:
									</td>
									<td>
										{!! Form::text('date_from', $reservation->date_from ,array('class' => 'form-control','id'=>'date_from','required')) !!}
									</td>
								</tr>
								<tr>
									<td>
										Hora desde:
									</td>
									<td>
										{!! Form::text('time_from',$reservation->time_from ,array('class' => 'form-control','id'=>'time_from','required')) !!}
									</td>
								</tr>

								<tr>
									<td>
										Fecha Hasta:
									</td>
									<td>
										{!! Form::text('date_to', $reservation->date_to ,array('class' => 'form-control','id'=>'date_to','required')) !!}
									</td>
								</tr>
								<tr>
									<td>
										Hora Hasta:
									</td>
									<td>
										{!! Form::text('time_to',$reservation->time_to ,array('class' => 'form-control','id'=>'time_to','required')) !!}
									</td>
								</tr>
								</thead>
							</table>
                            @if($idAdministratorOwner<>"")
							    {!! Form::submit('Update!',array('class' => 'btn btn-primary','id'=>'update')) !!}
							    {!! Form::hidden('id',$reservation->id,array('id'=>'id')) !!}
                            @endif
							{!! Form::close() !!}
                            @if($idAdministratorOwner<>"")
                                {!! Form::open(array('url' => 'reservations/destroy')) !!}
                                {!! Form::hidden('idD',$reservation->id,array('id'=>'idD')) !!}
                                {!! Form::submit('Delete!',array('class' => 'btn btn-danger','id'=>'delete')) !!}
                                {!! Form::close() !!}
                            @endif
                            @endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
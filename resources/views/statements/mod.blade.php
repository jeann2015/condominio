@extends('layouts.header')

@section('content')

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="main-header">
					<!-- Tutulo del Formulario -->
					<h4>Modificar Item</h4>
				</div>
			</div>
		</div>	
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
					<div class="card-header">
						<a href="{{ url('statements') }}" class="btn btn-default" role="button">Back </a>
					</div>
					<div class="card-block">
						<!-- Formulario -->
						@foreach($statements as $statement)
						{!! Form::open(array('url' => 'statements/update')) !!}
							<div class="form-group">
								<label>Descripción</label>
								{!! Form::text('description',$statement->description,array('class' => 'form-control','id'=>'description','required')) !!}
							</div>
							<div class="form-group">
								<label>Tipo</label>

								{!! Form::select('type', array('gasto' => 'Gasto', 'ingreso' => 'Ingreso'), $statement->type, ['class'=>'form-control','required']) !!}
							</div>
							{!! Form::submit('Update!',array('class' => 'btn btn-primary','id'=>'update')) !!}
							{!! Form::hidden('id',$statement->id,array('id'=>'id')) !!}
						@endforeach
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
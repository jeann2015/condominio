@extends('layouts.header')

@section('content')

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="main-header">
					<!-- Tutulo del Formulario -->
					<h4>Modificar Status</h4>
				</div>
			</div>
		</div>	
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header">
						<a href="{{ url('status') }}" class="btn btn-default" role="button">Back </a>
					</div>
					<div class="card-block">
						<!-- Formulario -->
						{!! Form::open(array('url' => 'status/update')) !!}
						<div class="form-group">
							<label>Descripción</label>
							{!! Form::text('description',$status->description,array('class' => 'form-control','id'=>'description','required')) !!}
						</div>
						<div class="form-group">
							<label>Tipo</label>
							{!! Form::select('types', array('1' => 'Pagos', '2' => 'Tickets','3'=>'Encuesta','4'=>'Respuesta de Encuesta'), $status->type, ['class'=>'form-control','required']) !!}
						</div>
						{!! Form::submit('Update!',array('class' => 'btn btn-primary','id'=>'update')) !!}
						{!! Form::hidden('id',$status->id,array('id'=>'id')) !!}
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
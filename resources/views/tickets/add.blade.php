@extends('layouts.header')

@section('content')

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="main-header">
					<!-- Tutulo del Formulario -->
					<h4>Nuevo Ticket</h4>
				</div>
			</div>
		</div>	
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header">
						<a href="{{ url('tickets') }}" class="btn btn-default" role="button">Back </a>
					</div>
					<div class="card-block">
						<!-- Formulario -->
						{!! Form::open(array('url' => 'tickets/new')) !!}

						<div class="form-group">
							<label>Edificios/Residencias</label>
							{!! Form::select('id_builds', $builds, null, ['id'=>'id_builds','class'=>'form-control','onchange'=>'search_apartments_name_owners(this.value)']) !!}
						</div>
						<div class="form-group">
							<label>Apartments/Casas/Propietarios:</label>
							<br>
							{!! Form::select('id_apartments', [], null, ['id'=>'id_apartments','class'=>'form-control']) !!}
						</div>
						<div class="form-group">
							<label>Tipo</label>
							{!! Form::select('id_types', $types, null, ['class'=>'form-control','required']) !!}
						</div>
						<div class="form-group">
							<label>Descripción</label>
							{!! Form::textarea('description','',array('class' => 'form-control','id'=>'description','required')) !!}
						</div>

						{!! Form::submit('Save!',array('class' => 'btn btn-primary','id'=>'save')); !!}
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
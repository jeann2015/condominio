@extends('layouts.header')

@section('content')

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="main-header">
					<h4>Eliminar Ticket</h4>
				</div>
			</div>
		</div>	
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header">
						<a href="{{ url('tickets') }}" class="btn btn-default" role="button">Back </a>
					</div>
					<div class="card-block">
						<!-- Formulario -->
						{!! Form::open(array('url' => 'tickets/delete')) !!}
						<label>Edificios/Redidencias -> Apartamentos/Casa</label>
						<select id="id_apartments" name="id_apartments" class="form-control" tabindex="-1" aria-hidden="true">
							@foreach($builds as $build)
								<optgroup label="{{ $build->description }}">
									@foreach($apartments as $apartment)
										@if($build->id == $apartment->id_builds)
											<option value="{{$apartment->id}}">{{ $apartment->description_apartments }}</option>
										@endif
									@endforeach
								</optgroup>
							@endforeach
						</select>
						<div class="form-group">
							<label>Tipo</label>
							{!! Form::select('types', $types, $tickets->id_types, ['class'=>'form-control','required']) !!}
						</div>
						<div class="form-group">
							<label>Estado</label>
							{!! Form::select('types', $status, $tickets->id_status, ['class'=>'form-control','required']) !!}
						</div>

						<div class="form-group">
							<label>Descripción</label>
							{!! Form::textarea('description',$tickets->description,array('class' => 'form-control','id'=>'description','required')) !!}
						</div>

						{!! Form::submit('Delete!',array('class' => 'btn btn-primary','id'=>'delete')) !!}
						{!! Form::hidden('id',$tickets->id,array('id'=>'id')) !!}
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
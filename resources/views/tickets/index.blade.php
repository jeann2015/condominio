@extends('layouts.header')
@section('content')

    <!-- @if(isset($notifys))

        <div class="alert alert-success" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
          <span aria-hidden="true">×</span>
          </button>
@foreach ($notifys as $noti => $valor)
            @if($valor == 1)
                <strong>Well done!</strong> You successfully insert your Data.
@endif
            @if($valor == 2)
                <strong>Well done!</strong> You successfully modified your Data.
@endif
            @if($valor == 3)
                <strong>Well done!</strong> You successfully removed your Data,
@endif
        @endforeach

                </div>

@endif -->

    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-header">
                        <h4>Tickets</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="toolbar">
                                @foreach ($user_access as $user_acces)
                                    @if($user_acces->inserts == 1)
                                        <a href="{{ url('tickets/add') }}" class="btn btn-default" role="button">Add </a>
                                    @else
                                        <a href="#" class="btn btn-default" role="button">No Add  </a>
                                    @endif
                                @endforeach

                                <a href="{{ url('home') }}" class="btn btn-default" role="button">Back </a>
                            </div>
                        </div>
                        <div class="card-block">

                            <table id="General" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Edificio/Residencia</th>
                                    <th>Apartamento/Casa</th>
                                    <th>Propietario</th>
                                    <th>Tipo</th>
                                    <th>Estado</th>
                                    <th>Fecha/Hora</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($tickets as $ticket)
                                    <tr>
                                        <td>{{ $ticket->id }}</td>
                                        <td>{{ $ticket->description_builds }}</td>
                                        <td>{{ $ticket->description_apartments }}</td>
                                        <td>{{ $ticket->description_owners }}</td>
                                        <td>{{ $ticket->description_types }}</td>
                                        @if($ticket->id_status==4)
                                            <td><span class="label label-danger">{{ $ticket->description_status }}</span></td>
                                        @elseif($ticket->id_status==5)
                                            <td><span class="label label-warning">{{ $ticket->description_status }}</span></td>
                                        @elseif($ticket->id_status==6)
                                            <td><span class="label label-success">{{ $ticket->description_status }}</span></td>
                                        @endif
                                        <td>{{  Carbon\Carbon::parse($ticket->created_at)->format('l jS \\of F Y h:i:s A')  }}</td>
                                        <td>
                                            <div class="tabledit-toolbar btn-toolbar">
                                                <div class="btn-group btn-group-sm">
                                                    @foreach ($user_access as $user_acces)
                                                        @if($user_acces->modifys == 1)
                                                            <a href="tickets/view/{{ $ticket->id }}" class="tabledit-edit-button btn btn-primary waves-effect waves-light" role="button">
                                                                <span class="icofont icofont-search-alt-2"></span>
                                                            </a>
                                                        @else
                                                            <a href="#" class="btn btn-default" role="button">No Modify</a>
                                                        @endif

                                                    @endforeach
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
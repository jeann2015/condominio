@extends('layouts.header')

@section('content')

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="main-header">
					<h4>Ver Ticket</h4>
				</div>
			</div>
		</div>
		<div class="card-block button-list">
            {!! Form::open(array('url' => 'tickets/update')) !!}

            <div class="modal fade modal-flex" id="Modal-overflow" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body model-container">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <h5 class="font-header">Conversacion</h5>
                            @foreach($tickets_anwers as $tickets_anwer)
                                <div class="form-group">
                                    <label>Mensaje de: {{ $tickets_anwer->name.", Fecha: ".Carbon\Carbon::parse($tickets_anwer->created_at)->format('l jS \\of F Y h:i:s A') }}</label>
                                    {!! Form::textarea('details',$tickets_anwer->answer,array('class' => 'form-control','id'=>'details','readonly')) !!}
                                </div>
                            @endforeach
                            @if($tickets->id_status<>"6")
                                <div class="form-group">
                                    <label>Respuesta</label>
                                    {!! Form::textarea('description_resp',null,array('class' => 'form-control','id'=>'description_resp')) !!}
                                </div>
                                {!! Form::submit('Guardar Respuesta!',array('class' => 'btn btn-primary','id'=>'Guardar')); !!}
                            @endif
                        </div>
                        <div class="p-15">
                        </div>
                    </div>
                </div>
            </div>

		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header">
						<a href="{{ url('tickets') }}" class="btn btn-default" role="button">Back </a>
						<a data-toggle="modal" data-target="#Modal-overflow" class="btn btn-success" role="button">Ver Conversacion </a>
					</div>

					<div class="card-block">
						<!-- Formulario -->

						<label>Edificios/Redidencias -> Apartamentos/Casa</label>
						<select id="id_apartments" name="id_apartments" class="form-control" tabindex="-1" aria-hidden="true">
							@foreach($builds as $build)
								<optgroup label="{{ $build->description }}">
									@foreach($apartments as $apartment)
										@if($build->id == $apartment->id_builds)
											<option value="{{$apartment->id}}">{{ $apartment->description_apartments }}</option>
										@endif
									@endforeach
								</optgroup>
							@endforeach
						</select>
						@foreach($owners_data as $owner)
							<div class="form-group">
								<label>Propietario</label>
								{!! Form::text('owners',$owner->fname,array('class' => 'form-control','id'=>'owners','required','readonly')) !!}
							</div>
							<div class="form-group">
								<label>Telefono de Propietario</label>
								{!! Form::text('phone_owners',$owner->phonenumber,array('class' => 'form-control','id'=>'phone_owners','required','readonly')) !!}
							</div>
						@endforeach
						<div class="form-group">
							<label>Tipo</label>
							{!! Form::select('types', $types, $tickets->id_types, ['class'=>'form-control','required']) !!}
						</div>
						<div class="form-group">
							<label>Estado</label>
							{!! Form::select('id_status', $status, $tickets->id_status, ['class'=>'form-control','required']) !!}
						</div>
                        @if($tickets->id_status<>"6")
                            {!! Form::submit('Guardar!',array('class' => 'btn btn-primary','id'=>'Guardar')); !!}
                        @endif
						{!! Form::hidden('id',$tickets->id,array('id'=>'id')) !!}
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
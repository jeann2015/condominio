@extends('layouts.header')

@section('content')

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="main-header">
					<!-- Tutulo del Formulario -->
					<h4>Modificar Tipo de Tickets</h4>
				</div>
			</div>
		</div>	
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header">
						<a href="{{ url('types') }}" class="btn btn-default" role="button">Back </a>
					</div>
					<div class="card-block">
						<!-- Formulario -->
						{!! Form::open(array('url' => 'types/update')) !!}
						<div class="form-group">
							<label>Descripción</label>
							{!! Form::text('description',$types->description,array('class' => 'form-control','id'=>'description','required')) !!}
						</div>

						{!! Form::submit('Update!',array('class' => 'btn btn-primary','id'=>'update')) !!}
						{!! Form::hidden('id',$types->id,array('id'=>'id')) !!}
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
@extends('layouts.header')

@section('content')

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="main-header">
					<!-- Tutulo del Formulario -->
					<h4>Nuevo Usuario</h4>
				</div>
			</div>
		</div>	
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header">
						<a href="{{ url('users') }}" class="btn btn-default" role="button">Back </a>
					</div>
					<div class="card-block">
						<!-- Formulario -->
						{!! Form::open(array('url' => 'users/new')) !!}
						<div class="form-group">
							<label>Grupos</label>
							{!! Form::select('grupos', $grupos, '', ['placeholder' => 'Select','class'=>'form-control','required']); !!}
						</div>
						<div class="form-group">
							<label>Nombre</label>
							{!! Form::text('name','',array('class' => 'form-control','id'=>'name','required')) !!}
						</div>
						<div class="form-group">
							<label>Correo</label>
							{!! Form::text('email','',array('class' => 'form-control','id'=>'email','required')) !!}
						</div>
							
						{!! Form::submit('Save!',array('class' => 'btn btn-success waves-effect waves-light m-r-30','id'=>'save')); !!}
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

<script>

    function changeValue(valor,checkbox){

        if(valor==0){
            $('#'+checkbox).val(1);
        }else{
            $('#'+checkbox).val(0);
        }
    }

    function all_view(valor){
        var count=0;
        count=$('#count_mod').val();

        if(valor==0){
            for(con=1;con<=count;con++){
                $('#view'+con).prop('checked', true);
                $('#view'+con).val(1);
            }
            $('#view_all').val(1);
        }
        if(valor==1){
            for(con=1;con<=count;con++){
                $('#view'+con).prop('checked',false);
                $('#view'+con).val(0);
            }
            $('#view_all').val(0);
        }

    }

    function all_save(valor){
        var count=0;
        count=$('#count_mod').val();

        if(valor==0){
            for(con=1;con<=count;con++){
                $('#save'+con).prop('checked', true);
                $('#save'+con).val(1);
            }
            $('#save_all').val(1);
        }
        if(valor==1){
            for(con=1;con<=count;con++){
                $('#save'+con).prop('checked',false);
                $('#save'+con).val(0);
            }
            $('#save_all').val(0);
        }

    }

    function all_modify(valor){
        var count=0;
        count=$('#count_mod').val();

        if(valor==0){
            for(con=1;con<=count;con++){
                $('#modify'+con).prop('checked', true);
                $('#modify'+con).val(1);
            }
            $('#modify_all').val(1);
        }
        if(valor==1){
            for(con=1;con<=count;con++){
                $('#modify'+con).prop('checked',false);
                $('#modify'+con).val(0);
            }
            $('#modify_all').val(0);
        }

    }

    function all_delete(valor){
        var count=0;
        count=$('#count_mod').val();

        if(valor==0){
            for(con=1;con<=count;con++){
                $('#delete'+con).prop('checked', true);
                $('#delete'+con).val(1);
            }
            $('#delete_all').val(1);
        }
        if(valor==1){
            for(con=1;con<=count;con++){
                $('#delete'+con).prop('checked',false);
                $('#delete'+con).val(0);
            }
            $('#delete_all').val(0);
        }

    }

</script>

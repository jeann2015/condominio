<?php

use App\Apartments;
use App\Polls;
use App\Reservations;
use App\UsersOwners;
use Carbon\Carbon;
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::get('/search/polls/{id_polls}', function (Request $request) {
    $polls = Polls::find($request->id_polls)->toArray();
    return response()->json($polls);
});

Route::get('/search/polls/done/{id_polls}', function (Request $request) {
    $polls = Polls::join('polls_anwers','polls_anwers.id_polls','=','polls.id')
        ->join('status','polls_anwers.id_status','=','status.id')
        ->select('polls.description','status.description as description_status')
        ->where('polls.id',$request->id_polls)
        ->get()->toArray();
    return response()->json($polls);
});

Route::get('/search/apartments/{id_builds}', function (Request $request) {

    $apartments = \DB::table('apartments')
        ->join('builds_apartments','builds_apartments.id_apartments','=','apartments.id')
        ->join('builds','builds_apartments.id_builds','=','builds.id')
        ->join('apartments_owners','apartments_owners.id_apartments','=','apartments.id')
        ->join('owners','apartments_owners.id_owners','=','owners.id')
	->where('builds_apartments.id_builds','=',$request->id_builds)
        ->select(
            'apartments.id',
            \DB::raw('concat(apartments.description,
            " (",apartments.meters,"m2x", builds.amount_maintenance_meters,")=",convert(apartments.meters*builds.amount_maintenance_meters, DECIMAL(12,2))," ",owners.fname," ",owners.lname) as description'),
            'owners.status',
            'builds.amount_maintenance_meters'
            )
        ->pluck('description', 'apartments.id');
    return response()->json($apartments);
});

Route::get('/search/report/{id_builds}', function (Request $request) {

    $apartments = \DB::table('apartments')
        ->join('builds_apartments','builds_apartments.id_apartments','=','apartments.id')
        ->join('builds','builds_apartments.id_builds','=','builds.id')
        ->join('apartments_owners','apartments_owners.id_apartments','=','apartments.id')
        ->join('owners','apartments_owners.id_owners','=','owners.id')
        ->where('builds_apartments.id_builds','=',$request->id_builds)
        ->select(
            'apartments.id',
            \DB::raw('concat(apartments.description," (",owners.fname," ",owners.lname,")") as description'),
            'owners.status',
            'builds.amount_maintenance_meters'
        )
        ->pluck('description', 'apartments.id')
        ->put('0','Todos');

    return response()->json($apartments);
});

Route::get('/search/encuesta/{id_builds}', function (Request $request) {

    $apartments = \DB::table('apartments')
        ->join('builds_apartments','builds_apartments.id_apartments','=','apartments.id')
        ->join('builds','builds_apartments.id_builds','=','builds.id')
        ->join('apartments_owners','apartments_owners.id_apartments','=','apartments.id')
        ->join('owners','apartments_owners.id_owners','=','owners.id')
        ->where('builds_apartments.id_builds','=',$request->id_builds)
        ->select(
            'apartments.id',
            \DB::raw('concat(apartments.description," (",owners.fname," ",owners.lname,")") as description'),
            'owners.status',
            'builds.amount_maintenance_meters'
        )
        ->pluck('description', 'apartments.id');
    return response()->json($apartments);
});

Route::get('/search/reservations/{id_user}', function (Request $request) {

    $dt = Carbon::now();
    $dateFrom =$dt->subMonth()->toDateString();
    $dateTo = $dt->addMonth(2)->toDateString();

    $UsersOwners = new UsersOwners;
    $builds = $UsersOwners->get_builds_owners_administrator($request->id_user);

    $reservations = Reservations::where('date_from','>=',$dateFrom)
    ->where('date_from','<=',$dateTo)
    ->whereIN('id_builds',$builds->toArray())
        ->select('reservations.description as title',
            \DB::raw("CONCAT(reservations.date_from,'T',reservations.time_from) as start"),
            \DB::raw("CONCAT(reservations.date_to,'T',reservations.time_to) as end"),
            \DB::raw("CONCAT('reservations/edit/',reservations.id) as url")
        )
        ->get();
    return response()->json($reservations);
});


Route::get('/search/apatments/along/{id_builds}', function (Request $request) {

    $apartments_owners = Apartments::join('builds_apartments','builds_apartments.id_apartments','=','apartments.id')
        ->join('apartments_owners','apartments_owners.id_apartments','=','apartments.id')
        ->select('apartments.id')
        ->where('builds_apartments.id_builds','=',$request->id_builds)
        ->get();

    $apartments = \DB::table('apartments')
        ->join('builds_apartments','builds_apartments.id_apartments','=','apartments.id')
        ->join('builds','builds_apartments.id_builds','=','builds.id')
        ->select(
            'apartments.id',
            \DB::raw('apartments.description as description')
        )
        ->whereNotIn('builds_apartments.id_apartments',$apartments_owners->toArray())
        ->where('builds_apartments.id_builds','=',$request->id_builds)
        ->pluck('description', 'apartments.id');

        return response()->json($apartments);
});









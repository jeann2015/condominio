<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Documents;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/modules', 'ModulesController@index');
Route::get('/modules/add', 'ModulesController@add');
Route::post('/modules/new', 'ModulesController@news');
Route::get('/modules/edit/{id}', 'ModulesController@edit');
Route::post('/modules/update', 'ModulesController@update');
Route::get('/modules/delete/{id}', 'ModulesController@delete');
Route::post('/modules/destroy', 'ModulesController@destroy');

Route::get('/audit', 'AuditsController@index');

Route::get('/home', 'HomeController@index');
Route::get('/users', 'UsersController@index');
Route::get('/users/add', 'UsersController@add');
Route::post('/users/new', 'UsersController@news');
Route::get('/users/edit/{id}', 'UsersController@edit');
Route::post('/users/update', 'UsersController@update');
Route::get('/users/delete/{id}', 'UsersController@delete');
Route::post('/users/destroy', 'UsersController@destroy');

Route::get('/grupos', 'GruposController@index');
Route::get('/grupos/add', 'GruposController@add');
Route::post('/grupos/new', 'GruposController@news');
Route::get('/grupos/edit/{id}', 'GruposController@edit');
Route::post('/grupos/update', 'GruposController@update');
Route::get('/grupos/delete/{id}', 'GruposController@delete');
Route::post('/grupos/destroy', 'GruposController@destroy');

Route::get('/status', 'StatusController@index');
Route::get('/status/add', 'StatusController@add');
Route::post('/status/new', 'StatusController@news');
Route::get('/status/edit/{id}', 'StatusController@edit');
Route::post('/status/update', 'StatusController@update');
Route::get('/status/delete/{id}', 'StatusController@delete');
Route::post('/status/destroy', 'StatusController@destroy');

Route::get('/builds', 'BuildsController@index');
Route::get('/builds/add', 'BuildsController@add');
Route::post('/builds/new', 'BuildsController@news');
Route::get('/builds/edit/{id}', 'BuildsController@edit');
Route::post('/builds/update', 'BuildsController@update');
Route::get('/builds/delete/{id}', 'BuildsController@delete');
Route::post('/builds/destroy', 'BuildsController@destroy');


Route::get('/administrators', 'AdministratorsController@index');
Route::get('/administrators/add', 'AdministratorsController@add');
Route::post('/administrators/new', 'AdministratorsController@news');
Route::get('/administrators/edit/{id}', 'AdministratorsController@edit');
Route::post('/administrators/update', 'AdministratorsController@update');
Route::get('/administrators/delete/{id}', 'AdministratorsController@delete');
Route::post('/administrators/destroy', 'AdministratorsController@destroy');

Route::get('/owners', 'OwnersController@index');
Route::get('/owners/add', 'OwnersController@add');
Route::post('/owners/new', 'OwnersController@news');
Route::get('/owners/edit/{id}', 'OwnersController@edit');
Route::post('/owners/update', 'OwnersController@update');
Route::get('/owners/delete/{id}', 'OwnersController@delete');
Route::post('/owners/destroy', 'OwnersController@destroy');

Route::get('/apartments', 'ApartmentsController@index');
Route::get('/apartments/add', 'ApartmentsController@add');
Route::post('/apartments/new', 'ApartmentsController@news');
Route::get('/apartments/edit/{id}', 'ApartmentsController@edit');
Route::post('/apartments/update', 'ApartmentsController@update');
Route::get('/apartments/delete/{id}', 'ApartmentsController@delete');
Route::post('/apartments/destroy', 'ApartmentsController@destroy');


Route::get('/tickets', 'TicketsController@index');
Route::get('/tickets/add', 'TicketsController@add');
Route::post('/tickets/new', 'TicketsController@news');
Route::get('/tickets/view/{id}', 'TicketsController@view');
Route::post('/tickets/update', 'TicketsController@update');
Route::get('/tickets/delete/{id}', 'TicketsController@delete');
Route::post('/tickets/destroy', 'TicketsController@destroy');


Route::get('/types', 'TypesController@index');
Route::get('/types/add', 'TypesController@add');
Route::post('/types/new', 'TypesController@news');
Route::get('/types/edit/{id}', 'TypesController@edit');
Route::post('/types/update', 'TypesController@update');
Route::get('/types/delete/{id}', 'TypesController@delete');
Route::post('/types/destroy', 'TypesController@destroy');


Route::get('/polls', 'PollsController@index');
Route::get('/polls/add', 'PollsController@add');
Route::post('/polls/new', 'PollsController@news');
Route::get('/polls/edit/{id}', 'PollsController@edit');
Route::post('/polls/update', 'PollsController@update');
Route::get('/polls/delete/{id}', 'PollsController@delete');
Route::post('/polls/destroy', 'PollsController@destroy');
Route::post('/polls/anwers', 'PollsController@anwers');
Route::get('/polls/view/{id}', 'PollsController@view');

Route::get('/reservations', 'ReservationsController@index');
Route::get('/reservations/add', 'ReservationsController@add');
Route::post('/reservations/new', 'ReservationsController@news');
Route::get('/reservations/edit/{id}', 'ReservationsController@edit');
Route::post('/reservations/update', 'ReservationsController@update');
Route::get('/reservations/delete/{id}', 'ReservationsController@delete');
Route::post('/reservations/destroy', 'ReservationsController@destroy');

Route::get('/payments', 'PaymentsController@index');
Route::get('/payments/add/{typePay}', 'PaymentsController@add');
Route::post('/payments/new/{typePay}', 'PaymentsController@news');
Route::get('/payments/edit/{id}', 'PaymentsController@edit');
Route::post('/payments/update', 'PaymentsController@update');
Route::get('/payments/delete/{id}', 'PaymentsController@delete');
Route::post('/payments/destroy', 'PaymentsController@destroy');
Route::get('/payments/pdf/{id}', 'PaymentsController@pdf');

Route::get('/statements', 'StatementController@index');
Route::get('/statements/add', 'StatementController@add');
Route::post('/statements/new', 'StatementController@news');
Route::get('/statements/edit/{id}', 'StatementController@edit');
Route::post('/statements/update', 'StatementController@update');
Route::get('/statements/delete/{id}', 'StatementController@delete');
Route::post('/statements/destroy', 'StatementController@destroy');

Route::get('/expenses', 'ExpenseController@index');
Route::get('/expenses/add', 'ExpenseController@add');
Route::post('/expenses/new', 'ExpenseController@news');
Route::get('/expenses/edit/{id}', 'ExpenseController@edit');
Route::post('/expenses/update', 'ExpenseController@update');
Route::get('/expenses/delete/{id}', 'ExpenseController@delete');
Route::post('/expenses/destroy', 'ExpenseController@destroy');
Route::get('/expenses/pdf/{id}', 'ExpenseController@pdf');

Route::get('/documents', 'DocumentsController@index');
Route::get('/documents/add', 'DocumentsController@add');
Route::post('/documents/new', 'DocumentsController@news');
Route::get('/documents/edit/{id}', 'DocumentsController@edit');
Route::post('/documents/update', 'DocumentsController@update');
Route::get('/documents/delete/{id}', 'DocumentsController@delete');
Route::post('/documents/destroy', 'DocumentsController@destroy');

Route::get('/documents/download/{id}', function($id)
{
    $documents = Documents::find($id);
    $data = base64_decode($documents->files_general);


    return response($data)
        ->header('Cache-Control', 'no-cache private')
        ->header('Content-Description', 'File Transfer')
        ->header('Content-Type', $documents->type_file)
        ->header('Content-length', strlen($data))
        ->header('Content-Disposition', 'attachment; filename=' . "pdf.pdf")
        ->header('Content-Transfer-Encoding', 'binary');
});

Route::get('/reportsaccount', 'ReportsController@reportsaccount');
Route::post('/reportsaccount/account/search', 'ReportsController@account');

Route::get('/reportsstates', 'ReportsController@reportsstates');
Route::post('/reportsstatess/states/search', 'ReportsController@states');


Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/changes', 'HomeController@changes');
Route::post('/changes/update', 'HomeController@update');